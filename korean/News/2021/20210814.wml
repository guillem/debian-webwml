#use wml::debian::translation-check translation="7ce619292bb7825470d6cb611887d68f405623ff" maintainer="Sebul"
<define-tag pagetitle>데비안 11 <q>bullseye</q> 릴리스</define-tag>
<define-tag release_date>2021-08-14</define-tag>
#use wml::debian::news


<p>2년 1개월 9일 동안 개발 끝에 데비안 프로젝트는 <a href="https://security-team.debian.org/">데비안 보안팀</a> 
및 <a href="https://wiki.debian.org/LTS">데비안 장기 지원</a>팀의 합동 작업 덕분에 향후 5년 동안 지원될 새로운 안정 버전 11
(코드명 <q>bullseye</q>)을 발표하게 된 것을 자랑스럽게 생각합니다.
</p>

<p>데비안 11 <q>bullseye</q>는 여러 데스크톱 응용 프로그램 및 환경과 함께 제공됩니다. 
무엇보다도 이제 데스크톱 환경을 포함합니다.
</p>
<ul>
<li>Gnome 3.38,</li>
<li>KDE Plasma 5.20,</li>
<li>LXDE 11,</li>
<li>LXQt 0.16,</li>
<li>MATE 1.24,</li>
<li>Xfce 4.16.</li>
</ul>


<p>이 릴리스는 11,294개 이상의 새 패키지 포함 총 59,551개의 패키지를 포함하며, 
<q>obsolete</q>로 표시되어 제거된 9,519개 이상의 패키지가 줄었습니다. 
42,821개의 패키지가 업데이트되었고 5,434개의 패키지는 바꾸지 않은 상태로 남았습니다.
</p>

<p><q>bullseye</q>는 exFAT 파일 시스템을 지원하는 리눅스 커널을 제공하는 첫 번째 릴리스가 되었으며 
기본적으로 exFAT 파일 시스템 마운트에 사용합니다. 
결과적으로 더 이상 exfat-fuse 패키지를 통해 제공되는 filesystem-in-userspace 구현을 사용할 필요가 없습니다. 
exFAT 파일 시스템을 만들고 확인하기 위한 도구는 exfatprogs 패키지에 제공됩니다.
</p>

<p>대부분의 최신 프린터는 공급업체별(종종 무료가 아닌) 드라이버 없이도 
드라이버 없는 인쇄 및 스캔을 사용할 수 있습니다. 
<q>bullseye</q>는 많은 최신 프린터에서 지원하는 공급업체 중립적인 IPP-over-USB 프로토콜을 사용하는 새로운 패키지인 ipp-usb를 제공합니다. 
이를 통해 USB 장치를 네트워크 장치로 취급할 수 있습니다. 
공식 SANE 드라이버 없는 백엔드는 eSCL 프로토콜을 사용하는 libsane1의 sane-escl에서 제공합니다.
</p>

<p><q>bullseye</q>의 systemd는 기본적으로 휘발성 저장소에 대한 암시적 폴백과 함께 영구 저널 기능을 활성화합니다. 
이를 통해 특수 기능에 의존하지 않는 사용자가 기존 로깅 데몬을 제거하고 시스템 저널만 사용하도록 전환할 수 있습니다.
</p>

<p>Debian Med 팀은 시퀀스 수준에서 바이러스를 연구하고 전염병에 사용되는 도구를 사용하여 
전염병과 싸우기 위한 소프트웨어를 패키징하여 코로나19와의 싸움에 참여하고 있습니다. 
이 작업은 두 분야 모두를 위한 기계 학습 도구에 초점을 맞춰 계속될 것입니다. 
품질 보증 및 지속적인 통합에 대한 팀의 작업은 과학에서 요구되는 일관되고 재현 가능한 결과에 매우 중요합니다. 
Debian Med Blend에는 이제 SIMD Everywhere의 혜택을 받는 다양한 성능 중요 응용 프로그램이 있습니다. 
Debian Med 팀에서 유지 관리하는 패키지를 설치하려면 버전 3.6.x에 있는 med-* 메타패키지를 설치하십시오. 
</p>

<p>
한국, 중국, 일본 및 다른 여러 나라에는 새로운 Fcitx 5 입력 방법이 있습니다. 
이는 <q>buster</q>에서 인기 있는 Fcitx4의 후속 제품입니다. 
이 새 버전은 훨씬 더 나은 Wayland(기본 디스플레이 관리자) 애드온을 지원합니다.
</p>

<p>데비안 11 <q>bullseye</q>에는 다음과 같은 수많은 업데이트된 소프트웨어 패키지가
(이전 릴리스의 모든 패키지의 72% 이상) 들어 있습니다.
</p>
<ul>
<li>Apache 2.4.48</li>
<li>BIND DNS Server 9.16</li>
<li>Calligra 3.2</li>
<li>Cryptsetup 2.3</li>
<li>Emacs 27.1</li>
<li>GIMP 2.10.22</li>
<li>GNU Compiler Collection 10.2</li>
<li>GnuPG 2.2.20</li>
<li>Inkscape 1.0.2</li>
<li>LibreOffice 7.0</li>
<li>Linux kernel 5.10 series</li>
<li>MariaDB 10.5</li>
<li>OpenSSH 8.4p1</li>
<li>Perl 5.32</li>
<li>PHP 7.4</li>
<li>PostgreSQL 13</li>
<li>Python 3, 3.9.1</li>
<li>Rustc 1.48</li>
<li>Samba 4.13</li>
<li>Vim 8.2</li>
<li>30,000개 넘는 소스 패키지에서 빌드한 59,000개 넘는 즉시 사용 가능한 소프트웨어 패키지.</li>
</ul>

<p>이러한 광범위한 패키지 선택과 기존의 광범위한 아키텍처 지원을 통해 데비안은 다시 한번 <q>범용 운영체제</q> 라는 목표에 충실합니다. 
데스크톱 시스템에서 넷북에 이르기까지 다양한 사용 사례에 적합합니다. 
개발 서버에서 클러스터 시스템으로; 데이터베이스, 웹 및 스토리지 서버용. 
동시에 데비안 아카이브의 모든 패키지에 대한 자동 설치 및 업그레이드 테스트와 같은 추가적인 품질 보증 노력은 
<q>bullseye</q>가 안정 데비안 릴리스에 대한 사용자의 높은 기대치를 충족할 것을 보장합니다.
</p>

<p>
9개 아키텍처 지원:
64-bit PC / Intel EM64T / x86-64 (<code>amd64</code>),
32-bit PC / Intel IA-32 (<code>i386</code>),
64-bit little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64-bit IBM S/390 (<code>s390x</code>),
for ARM, <code>armel</code>
and <code>armhf</code> for older and more recent 32-bit hardware,
plus <code>arm64</code> for the 64-bit <q>AArch64</q> architecture,
and for MIPS, <code>mipsel</code> (little-endian) architectures for 32-bit hardware
and <code>mips64el</code> architecture for 64-bit little-endian hardware.
</p>

<h3>시도해 보고 싶은가요?</h3>
<p>
데비안 11 <q>bullseye</q> 를 설치하지 않고 단순히 사용해 보고 싶다면 
컴퓨터 메모리를 통해 읽기 전용 상태로 전체 운영 체제를 로드하고 실행하는 사용 가능한 <a href="$(HOME)/CD/live/">라이브 이미지</a> 중 하나를 써 볼 수 있습니다.
</p>

<p>이러한 라이브 이미지는 <code>amd64</code> 및 <code>i386</code> 아키텍처에 제공되며 
DVD, USB 스틱 및 netboot 설정에 사용할 수 있습니다. 
사용자는 GNOME, KDE Plasma, LXDE, LXQt, MATE 및 Xfce와 같은 다양한 데스크탑 환경 중에서 선택할 수 있습니다. 
데비안 라이브 <q>bullseye</q>에는 표준 라이브 이미지가 있으므로 그래픽 사용자 인터페이스 없이 기본 데비안 시스템을 사용해 볼 수도 있습니다.
</p>

<p>운영 체제를 사용하려면 라이브 이미지에서 컴퓨터의 하드 디스크로 설치할 수 있습니다. 
라이브 이미지는 Calamares 독립 설치 프로그램과 표준 데비안 설치 프로그램을 포함합니다. 
자세한 정보는 데비안 웹사이트의 <a href="$(HOME)/releases/bullseye/releasenotes">release notes</a> 및  
<a href="$(HOME)/CD/live/">live install images</a> 섹션에서 볼 수 있습니다.
</p>

<p>데비안 11 <q>bullseye</q>를 컴퓨터의 하드 디스크에 직접 설치하려면 
Blu-ray 디스크, DVD, CD, USB 스틱 또는 네트워크 연결과 같은 다양한 설치 미디어 중에서 선택할 수 있습니다. 
Cinnamon, GNOME, KDE Plasma Desktop and Applications, LXDE, LXQt, MATE 및 Xfce와 같은 
여러 데스크탑 환경은 이러한 이미지를 통해 설치할 수 있습니다. 
또한 단일 디스크에서 선택한 아키텍처에서 설치를 지원하는 <q>multi-architecture</q> CD를 사용할 수 있습니다. 
또는 항상 부팅 가능한 USB 설치 미디어를 만들 수 있습니다
(자세한 내용은 <a href="$(HOME)/releases/bullseye/installmanual">Installation Guide</a> 참조).
</p>

# Translators: some text taken from /devel/debian-installer/News/2021/20210802

<p>데비안 설치 프로그램이 많이 개발되어 하드웨어 지원 및 기타 새로운 기능이 향상되었습니다.
</p>
<p>때에 따라 성공적인 설치에도 설치된 시스템으로 재부팅할 때 디스플레이 문제가 계속 발생할 수 있습니다. 
이러한 경우 로그인에 도움이 될 수 있는 <a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">몇 가지 해결 방법</a>이 있습니다. 
사용자가 자동화된 방식으로 시스템에서 누락된 펌웨어를 감지하고 수정할 수 있는 
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">isenkram 기반 절차</a>도 있습니다. 
물론, 무료가 아닌 패키지를 설치해야 할 가능성이 매우 높기 때문에 해당 도구를 사용할 때의 장단점을 저울질해야 합니다.</p>

<p>이 외에도 <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">펌웨어 패키지가 포함된 비자유 설치 프로그램 이미지</a>가 
개선되어 설치된 시스템에 펌웨어가 필요한지 예상할 수 있습니다
(예: AMD 또는 Nvidia 그래픽 카드용 펌웨어 또는 최신 세대의 Intel 오디오 하드웨어).
</p>

<p>클라우드 사용자를 위해 데비안은 가장 잘 알려진 많은 클라우드 플랫폼을 직접 지원합니다. 
공식 데비안 이미지는 각 이미지 마켓플레이스를 통해 쉽게 선택할 수 있습니다. 
데비안은 또한 로컬 클라우드 설정에서 다운로드하여 사용할 수 있도록 <a
href="https://cloud.debian.org/images/openstack/current/">사전 빌드된 OpenStack 이미지</a>(amd64 및 arm64아키텍처)를 게시 합니다.
</p>

<p>데비안은 이제 76개 언어로 설치할 수 있으며 대부분 텍스트 기반 및 그래픽 사용자 인터페이스에서 사용할 수 있습니다.
</p>

<p>설치 이미지는 <a href="$(HOME)/CD/torrent-cd/">bittorrent</a>(권장하는 방법), 
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> 또는 
<a href="$(HOME)/CD/http-ftp/">HTTP</a>를 통해 지금 다운로드할 수 있습니다. 
자세한 내용은 <a href="$(HOME)/CD/">Debian on CDs</a>을 참조하십시오. 
<q>bullseye</q>는 곧 여러 <a href="$(HOME)/CD/vendors">vendors</a>의 실제 DVD, CD-ROM 및 Blu-ray 디스크로도 제공될 예정입니다.
</p>


<h3>데비안 업그레이드</h3>
<p>이전 릴리스 데비안 10(코드 이름 <q>buster</q>)에서 데비안 11로 업그레이드는 
대부분의 구성에서 APT 패키지 관리 도구에 의해 자동으로 처리됩니다.
</p>

<p>Bullseye에서, 보안 제품군은 이제 bullseye-security로 이름이 지정되었으며 사용자는 업그레이드할 때 그에 따라 APT 소스 목록 파일을 조정해야 합니다. 
APT 구성에 또는 고정도 포함하여 <code>APT::Default-Release</code>되어 있으면 조정이 필요할 수 있습니다. 
자세한 내용은 릴리스 정보의 <a href="https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information#security-archive">Changed 
security archive layout</a> 섹션을 참조하십시오.
</p>

<p>원격으로 업그레이드 하려면 
<a href="$(HOME)/releases/bullseye/amd64/release-notes/ch-information#ssh-not-available">No new SSH connections possible 
during upgrade</a> 섹션을 유의해세요. 
</p>

<p>늘 그렇듯이 데비안 시스템은 강제 다운타임 없이 문제 없이 제자리에서 업그레이드할 수 있지만 
<a href="$(HOME)/releases/bullseye/releasenotes">release notes</a> 및 
<a href="$(HOME)/releases/bullseye/installmanual">installation guide</a>에서 
가능한 문제와 설치 및 업그레이드에 대한 자세한 지침을 읽는 것이 좋습니다. 
릴리스 정보는 릴리스 후 몇 주 내에 더욱 개선되고 추가 언어로 번역될 것입니다.
</p>


<h2>데비안 정보</h2>

<p>데비안은 인터넷을 통해 협력하는 전 세계 수천 명의 자원 봉사자가 개발한 자유 운영체제입니다. 
데비안 프로젝트의 주요 장점은 자원 봉사 기반, 데비안 사회 계약 및 자유 소프트웨어에 대한 헌신, 가능한 최고의 운영 체제를 제공하겠다는 약속입니다. 
이 새로운 릴리스는 그 방향에서 또 다른 중요한 단계입니다.
</p>


<h2>연락 정보</h2>

<p>더 자세한 정보를 보려면 데비안 웹 페이지 
<a href="$(HOME)/">https://www.debian.org/</a>를 방문하거나 메일 
&lt;press@debian.org&gt; 보내세요.
</p>
