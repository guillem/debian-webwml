# Debian Web Site. Italian translation.
# Sebastiano Pistore <SebastianoPistore.info@protonmail.ch>, 2019
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Sebastiano Pistore <SebastianoPistore.info@protonmail.ch>\n"
"Language-Team: \n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.3\n"

#: ../../english/blends/blend.defs:15
msgid "Metapackages"
msgstr "Metapacchetti"

#: ../../english/blends/blend.defs:18
msgid "Downloads"
msgstr "Scarica"

#: ../../english/blends/blend.defs:21
msgid "Derivatives"
msgstr "Derivate"

#: ../../english/blends/released.data:15
msgid ""
"The goal of Debian Astro is to develop a Debian based operating system that "
"fits the requirements of both professional and hobby astronomers. It "
"integrates a large number of software packages covering telescope control, "
"data reduction, presentation and other fields."
msgstr ""
"L'obiettivo di Debian Astro è quello di sviluppare un sistema operativo "
"basato su Debian pensato per le esigenze di astronomi e astrofili. Integra "
"un gran numero di pacchetti che vanno dal controllo del telescopio, data "
"reduction, alle presentazioni e molte altre possibili necessità."

#: ../../english/blends/released.data:23
msgid ""
"The goal of DebiChem is to make Debian a good platform for chemists in their "
"day-to-day work."
msgstr ""
"DebiChem è realizzato con lo scopo di rendere Debian una buona piattaforma "
"per supportare le attività più comuni nel campo della chimica."

#: ../../english/blends/released.data:31
msgid ""
"The goal of Debian Games is to provide games in Debian from arcade and "
"adventure to simulation and strategy."
msgstr ""
"Debian Games è un sistema Debian orientato all'intrattenimento: arcade, "
"giochi d'avventura, simulazione e strategia."

#: ../../english/blends/released.data:39
msgid ""
"The goal of Debian Edu is to provide a Debian OS system suitable for "
"educational use and in schools."
msgstr ""
"L'obiettivo di Debian Edu è quello di realizzare un sistema Debian adatto "
"per le scuole e le attività formative."

#: ../../english/blends/released.data:47
msgid ""
"The goal of Debian GIS is to develop Debian into the best distribution for "
"Geographical Information System applications and users."
msgstr ""
"L'obiettivo di Debian GIS è quello di sfruttare Debian come base per "
"realizzare una distribuzione orientata agli utilizzatori di sistemi GIS "
"(Geographical Information System)."

#: ../../english/blends/released.data:57
msgid ""
"The goal of Debian Junior is to make Debian an OS that children will enjoy "
"using."
msgstr ""
"Debian Junior è pensato per rendere Debian un sistema operativo dedicato "
"alle necessità dell'infanzia."

#: ../../english/blends/released.data:65
msgid ""
"The goal of Debian Med is a complete free and open system for all tasks in "
"medical care and research. To achieve this goal Debian Med integrates "
"related free and open source software for medical imaging, bioinformatics, "
"clinic IT infrastructure, and others within the Debian OS."
msgstr ""
"Debian Med è un sistema aperto pensato per svolgere tutte le attività nel "
"settore clinico e della ricerca biomedica. Per raggiungere questo obiettivo, "
"Debian Med integra software Liberi e gratuiti per l'imaging medico, la "
"bioinformatica, l'infrastruttura IT e altro all'interno del sistema "
"operativo Debian."

#: ../../english/blends/released.data:73
msgid ""
"The goal of Debian Multimedia is to make Debian a good platform for audio "
"and multimedia work."
msgstr ""
"L'obiettivo di Debian Multimedia è quello di rendere disponibile una "
"piattaforma Debian orientata all'uso multimediale."

#: ../../english/blends/released.data:81
msgid ""
"The goal of Debian Science is to provide a better experience when using "
"Debian to researchers and scientists."
msgstr "Debian Science è pensata per le esigenze di ricercatori e scienziati."

#: ../../english/blends/released.data:89
msgid ""
"The goal of FreedomBox is to develop, design and promote personal servers "
"running free software for private, personal communications. Applications "
"include blogs, wikis, websites, social networks, email, web proxy and a Tor "
"relay on a device that can replace a wireless router so that data stays with "
"the users."
msgstr ""
"L'obiettivo di FreedomBox è quello di progettare, sviluppare e promuovere "
"piccoli server personali che eseguano Software Libero per comunicazioni ad "
"uso privato. Queste applicazioni includono blog, wiki, siti web, social "
"network, e-mail, proxy web e persino un relay Tor sul dispositivo che può "
"sostituire un router wireless."

#: ../../english/blends/unreleased.data:15
msgid ""
"The goal of Debian Accessibility is to develop Debian into an operating "
"system that is particularly well suited for the requirements of people with "
"disabilities."
msgstr ""
"L'obiettivo di Debian Accessibility è quello di realizzare un sistema "
"operativo Debian adatto alle esigenze delle persone con disabilità."

#: ../../english/blends/unreleased.data:23
msgid ""
"The goal of Debian Design is to provide applications for designers. This "
"includes graphic design, web design and multimedia design."
msgstr ""
"L'obiettivo di Debian Design è quello di fornire applicazioni per i "
"professionisti della grafica. Sono supportati: graphic design, web design e "
"design multimediale."

#: ../../english/blends/unreleased.data:30
msgid ""
"The goal of Debian EzGo is to provide culture-based open and free technology "
"with native language support and appropriate user friendly, lightweight and "
"fast desktop environment for low powerful/cost hardwares to empower human "
"capacity building and technology development  in many areas and regions, "
"like Africa, Afghanistan, Indonesia, Vietnam  using Debian."
msgstr ""
"L'obiettivo di Debian EzGo è quello di realizzare un sistema operativo "
"Libero e gratuito, adattato a contesti culturali non europei, con supporto "
"linguistico nativo e un ambiente desktop semplice da usare, leggero e "
"veloce, perfetto per hardware a basso costo e basse prestazioni, pensato per "
"lo sviluppo delle capacità sociali e tecnologiche in aree e regioni come "
"Africa, Afghanistan, Indonesia e Vietnam utilizzando Debian."

#: ../../english/blends/unreleased.data:38
msgid ""
"The goal of Debian Hamradio is to support the needs of radio amateurs in "
"Debian by providing logging, data mode and packet mode applications and more."
msgstr ""
"L'obiettivo di Debian Hamradio è quello di soddisfare le esigenze dei "
"radioamatori permettendo la tenuta del log di stazione, la modalità dati, "
"packet radio e altro ancora."

#: ../../english/blends/unreleased.data:47
msgid ""
"The goal of DebianParl is to provide applications to support the needs of "
"parliamentarians, politicians and their staffers all around the world."
msgstr ""
"L'obiettivo di DebianParl è quello di rispondere alle esigenze di "
"parlamentari, politici e dei loro collaboratori, in tutti i paesi del mondo."
