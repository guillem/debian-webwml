#use wml::debian::blend title="About the Blend"
#use "navbar.inc"
# $Id$

<p>The <b>Debian GIS Pure Blend</b> is a project of the <a
href="https://wiki.debian.org/Teams/DebianGis">Debian GIS Team</a>
who collaborate on maintenance of GIS related packages for Debian.
Every <a href="https://blends.debian.org/">Pure Blend</a> is a subset of Debian
that is configured to support a particular target group out-of-the-box. This
blend aims to support the needs of those working with map data, remote
sensing and earth observation.</p>

<p>The Blend is built from a curated list of GIS software in Debian.
The two main outputs from the Blend are a collection of "metapackages" and live
images that can be put onto a DVD or a USB stick.</p>

<h2>Metapackages</h2>

<p>Metapackages in Debian are packages that can be installed just like other
packages but that do not contain any software themselves, instead instructing
the packaging system to install a group of other packages.</p>

<p>See <a href="./get/metapackages">using the metapackages</a> for more
information on which metapackages are available and how to install the
metapackages on an existing Debian system.</p>

