<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Numerous security issues have been identified and fixed in Pidgin in
Debian/Wheezy.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2365">CVE-2016-2365</a>

    <p>MXIT Markup Command Denial of Service Vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2366">CVE-2016-2366</a>

    <p>MXIT Table Command Denial of Service Vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2367">CVE-2016-2367</a>

    <p>MXIT Avatar Length Memory Disclosure Vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2368">CVE-2016-2368</a>

    <p>MXIT g_snprintf Multiple Buffer Overflow Vulnerabilities</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2369">CVE-2016-2369</a>

    <p>MXIT CP_SOCK_REC_TERM Denial of Service Vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2370">CVE-2016-2370</a>

    <p>MXIT Custom Resource Denial of Service Vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2371">CVE-2016-2371</a>

    <p>MXIT Extended Profiles Code Execution Vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2372">CVE-2016-2372</a>

    <p>MXIT File Transfer Length Memory Disclosure Vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2373">CVE-2016-2373</a>

    <p>MXIT Contact Mood Denial of Service Vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2374">CVE-2016-2374</a>

    <p>MXIT MultiMX Message Code Execution Vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2375">CVE-2016-2375</a>

    <p>MXIT Suggested Contacts Memory Disclosure Vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2376">CVE-2016-2376</a>

    <p>MXIT read stage 0x3 Code Execution Vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2377">CVE-2016-2377</a>

    <p>MXIT HTTP Content-Length Buffer Overflow Vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2378">CVE-2016-2378</a>

    <p>MXIT get_utf8_string Code Execution Vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2380">CVE-2016-2380</a>

    <p>MXIT mxit_convert_markup_tx Information Leak Vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4323">CVE-2016-4323</a>

    <p>MXIT Splash Image Arbitrary File Overwrite Vulnerability</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.10.10-1~deb7u2.</p>

<p>We recommend that you upgrade your pidgin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-542.data"
# $Id: $
