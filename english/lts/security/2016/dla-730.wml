<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in the Mozilla Firefox web
browser: Multiple memory safety errors, buffer overflows and other
implementation errors may lead to the execution of arbitrary code or
bypass of the same-origin policy.</p>

<p>A man-in-the-middle attack in the addon update mechanism has been fixed.</p>

<p>A use-after-free vulnerability in the SVG Animation was discovered,
allowing a remote attacker to cause a denial of service (application
crash) or execute arbitrary code, if a user is tricked into opening a
specially crafted website.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
45.5.1esr-1~deb7u1.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-730.data"
# $Id: $
