<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Emilien Gaspar discovered that collectd, a statistics collection and
monitoring daemon, incorrectly processed incoming network
packets. This resulted in a heap overflow, allowing a remote attacker
to either cause a DoS via application crash, or potentially execute
arbitrary code.</p>

<p>Additionally, security researchers at Columbia University and the
University of Virginia discovered that collectd failed to verify a
return value during initialization. This meant the daemon could
sometimes be started without the desired, secure settings.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.1.0-3+deb7u1.</p>

<p>We recommend that you upgrade your collectd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-575.data"
# $Id: $
