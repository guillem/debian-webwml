<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in qemu-kvm:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8576">CVE-2016-8576</a>

    <p>qemu-kvm built with the USB xHCI controller emulation support is vulnerable
    to an infinite loop issue. It could occur while processing USB command ring
    in <q>xhci_ring_fetch</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8577">CVE-2016-8577</a>

    <p>qemu-kvm built with the virtio-9p back-end support is vulnerable to a memory
    leakage issue. It could occur while doing a I/O read operation in
    v9fs_read() routine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8578">CVE-2016-8578</a>

    <p>qemu-kvm built with the virtio-9p back-end support is vulnerable to a null
    pointer dereference issue. It could occur while doing an I/O vector
    unmarshalling operation in v9fs_iov_vunmarshal() routine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8669">CVE-2016-8669</a>

    <p>qemu-kvm built with the 16550A UART emulation support is vulnerable to a
    divide by zero issue. It could occur while updating serial device parameters
    in <q>serial_update_parameters</q>.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.2+dfsg-6+deb7u17.</p>

<p>We recommend that you upgrade your qemu-kvm packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-679.data"
# $Id: $
