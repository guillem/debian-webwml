<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in the network traffic analyzer Wireshark.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22207">CVE-2021-22207</a>

    <p>Excessive memory consumption in the MS-WSP dissector.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22235">CVE-2021-22235</a>

    <p>Crash in the DNP dissector.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39921">CVE-2021-39921</a>

    <p>NULL pointer exception in the Modbus dissector.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39922">CVE-2021-39922</a>

    <p>Buffer overflow in the C12.22 dissector.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39923">CVE-2021-39923</a>

    <p>Large loop in the PNRP dissector.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39924">CVE-2021-39924</a>

    <p>Large loop in the Bluetooth DHT dissector.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39925">CVE-2021-39925</a>

    <p>Buffer overflow in the Bluetooth SDP dissector.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39928">CVE-2021-39928</a>

    <p>NULL pointer exception in the IEEE 802.11 dissector.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39929">CVE-2021-39929</a>

    <p>Uncontrolled Recursion in the Bluetooth DHT dissector.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.6.20-0+deb9u2.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>For the detailed security status of wireshark please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wireshark">https://security-tracker.debian.org/tracker/wireshark</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2849.data"
# $Id: $
