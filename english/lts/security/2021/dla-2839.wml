<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>One security issue has been discovered in gerbv: a viewer for Gerber RS-274X files.</p>

<p>It was discovered that an out-of-bounds write vulnerability exists in the drill
format T-code tool. A specially-crafted drill file can lead to code execution.
An attacker can provide a malicious file to trigger this vulnerability.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.6.1-2+deb9u1.</p>

<p>We recommend that you upgrade your gerbv packages.</p>

<p>For the detailed security status of gerbv please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gerbv">https://security-tracker.debian.org/tracker/gerbv</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2839.data"
# $Id: $
