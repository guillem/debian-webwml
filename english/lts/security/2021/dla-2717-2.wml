<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>
It was discovered that there was a regression in a previous update to the Redis
key-value database.
</p>

<p>A test was not correctly backported from the latest upstream release which
meant that binaries were not available on all LTS platforms.  The Redis server
code was unaffected.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32761">CVE-2021-32761</a></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, this problem has been fixed in version
3:3.2.6-3+deb9u6.</p>

<p>We recommend that you upgrade your redis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2717.data"
# $Id: $
