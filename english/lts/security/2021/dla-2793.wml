<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>One security issue has been discovered in mosquitto: MQTT message broker.
A null dereference vulnerability was found which could lead to crashes for
applications using the library.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.4.10-3+deb9u5.</p>

<p>We recommend that you upgrade your mosquitto packages.</p>

<p>For the detailed security status of mosquitto please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mosquitto">https://security-tracker.debian.org/tracker/mosquitto</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2793.data"
# $Id: $
