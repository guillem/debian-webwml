<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in libzip, a library for reading, creating, and
modifying zip archives.
Crafted ZIP archives could allow remote attackers to cause denial of
service due to memory allocation failure by mishandling EOCD records.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1.1.2-1.1+deb9u1.</p>

<p>We recommend that you upgrade your libzip packages.</p>

<p>For the detailed security status of libzip please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libzip">https://security-tracker.debian.org/tracker/libzip</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2858.data"
# $Id: $
