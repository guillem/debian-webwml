<define-tag description>LTS security update</define-tag>
<define-tag moreinfo></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40982">CVE-2022-40982</a>

    <p>Daniel Moghimi discovered Gather Data Sampling (GDS), a hardware
    vulnerability for Intel CPUs which allows unprivileged speculative
    access to data which was previously stored in vector registers.</p>

    <p>This mitigation requires updated CPU microcode provided in the
    intel-microcode package.</p>

    <p>For details please refer to <a href="https://downfall.page/">https://downfall.page/</a> and
    <a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/gather-data-sampling.html">https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/gather-data-sampling.html</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-20569">CVE-2023-20569</a>

    <p>Daniel Trujillo, Johannes Wikner and Kaveh Razavi discovered
    INCEPTION, also known as Speculative Return Stack Overflow (SRSO),
    a transient execution attack that leaks arbitrary data on all AMD
    Zen CPUs. An attacker can mis-train the CPU BTB to predict non-architectural CALL instructions in kernel space and use this to
    control the speculative target of a subsequent kernel RET,
    potentially leading to information disclosure via a speculative
    side-channel.</p>

    <p>For details please refer to
    <a href="https://comsec.ethz.ch/research/microarch/inception/">https://comsec.ethz.ch/research/microarch/inception/</a> and
    <a href="https://www.amd.com/en/corporate/product-security/bulletin/amd-sb-7005">https://www.amd.com/en/corporate/product-security/bulletin/amd-sb-7005</a>.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
5.10.179-5~deb10u1.</p>

<p>We recommend that you upgrade your linux-5.10 packages.</p>

<p>For the detailed security status of linux-5.10 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux-5.10">https://security-tracker.debian.org/tracker/linux-5.10</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3525.data"
# $Id: $
