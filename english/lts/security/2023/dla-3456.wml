<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Requests, a Python HTTP library, has been leaking Proxy-Authorization headers
to destination servers when redirected to an HTTPS endpoint. For HTTP
connections sent through the tunnel, the proxy will identify the header in the
request itself and remove it prior to forwarding to the destination server.
However when sent over HTTPS, the `Proxy-Authorization` header must be sent in
the CONNECT request as the proxy has no visibility into the tunneled request.
This results in Requests forwarding proxy credentials to the destination
server unintentionally, allowing a malicious actor to potentially exfiltrate
sensitive information.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.21.0-1+deb10u1.</p>

<p>We recommend that you upgrade your requests packages.</p>

<p>For the detailed security status of requests please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/requests">https://security-tracker.debian.org/tracker/requests</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3456.data"
# $Id: $
