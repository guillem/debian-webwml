<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were discovered in Git, a distributed revision
control system. An attacker may trigger code execution in specific
situations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23521">CVE-2022-23521</a>

    <p>gitattributes are a mechanism to allow defining attributes for
    paths. These attributes can be defined by adding a
    `.gitattributes` file to the repository, which contains a set of
    file patterns and the attributes that should be set for paths
    matching this pattern. When parsing gitattributes, multiple
    integer overflows can occur when there is a huge number of path
    patterns, a huge number of attributes for a single pattern, or
    when the declared attribute names are huge. These overflows can be
    triggered via a crafted `.gitattributes` file that may be part of
    the commit history. Git silently splits lines longer than 2KB when
    parsing gitattributes from a file, but not when parsing them from
    the index. Consequentially, the failure mode depends on whether
    the file exists in the working tree, the index or both. This
    integer overflow can result in arbitrary heap reads and writes,
    which may result in remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41903">CVE-2022-41903</a>

    <p>`git log` can display commits in an arbitrary format using its
    `--format` specifiers. This functionality is also exposed to `git
    archive` via the `export-subst` gitattribute. When processing the
    padding operators, there is a integer overflow in
    `pretty.c::format_and_pad_commit()` where a `size_t` is stored
    improperly as an `int`, and then added as an offset to a
    `memcpy()`. This overflow can be triggered directly by a user
    running a command which invokes the commit formatting machinery
    (e.g., `git log --format=...`). It may also be triggered
    indirectly through git archive via the export-subst mechanism,
    which expands format specifiers inside of files within the
    repository during a git archive. This integer overflow can result
    in arbitrary heap writes, which may result in arbitrary code
    execution.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1:2.20.1-2+deb10u7.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>For the detailed security status of git please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/git">https://security-tracker.debian.org/tracker/git</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3282.data"
# $Id: $
