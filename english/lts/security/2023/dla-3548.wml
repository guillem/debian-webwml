<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in qpdf, a package with tools for
transforming and inspecting PDF files.
Crafted files may enable remote attackers to execute arbitrary code or
create recursive calls for a long time, which causes a denial of service.
Further a heap-based buffer overflow might occur when a certain downstream
write fails.</p>


<p>For Debian 10 buster, these problems have been fixed in version
8.4.0-2+deb10u1.</p>

<p>We recommend that you upgrade your qpdf packages.</p>

<p>For the detailed security status of qpdf please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qpdf">https://security-tracker.debian.org/tracker/qpdf</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3548.data"
# $Id: $
