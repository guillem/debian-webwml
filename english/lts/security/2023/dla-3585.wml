<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulneratibilities were found in exempi, an implementation of XMP
(Extensible Metadata Platform).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-18651">CVE-2020-18651</a>

    <p>A Buffer Overflow vulnerability was found
    in function ID3_Support::ID3v2Frame::getFrameValue
    allows remote attackers to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-18652">CVE-2020-18652</a>

    <p>A Buffer Overflow vulnerability was found in
    WEBP_Support.cpp allows remote attackers to cause a
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36045">CVE-2021-36045</a>

    <p>An out-of-bounds read vulnerability was found
    that could lead to disclosure of arbitrary memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36046">CVE-2021-36046</a>

    <p>A memory corruption vulnerability was found,
    potentially resulting in arbitrary code execution
    in the context of the current use</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36047">CVE-2021-36047</a>

    <p>An Improper Input Validation vulnerability was found,
    potentially resulting in arbitrary
    code execution in the context of the current use.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36048">CVE-2021-36048</a>

    <p>An Improper Input Validation was found,
    potentially resulting in arbitrary
    code execution in the context of the current user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36050">CVE-2021-36050</a>

    <p>A buffer overflow vulnerability was found,
    potentially resulting in arbitrary code execution
    in the context of the current user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36051">CVE-2021-36051</a>

    <p>A buffer overflow vulnerability was found,
    potentially resulting in arbitrary code execution
    in the context of the current user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36052">CVE-2021-36052</a>

    <p>A memory corruption vulnerability was found,
    potentially resulting in arbitrary code execution
    in the context of the current user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36053">CVE-2021-36053</a>

    <p>An out-of-bounds read vulnerability was found,
    that could lead to disclosure of arbitrary memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36054">CVE-2021-36054</a>

    <p>A buffer overflow vulnerability was found potentially
    resulting in local application denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36055">CVE-2021-36055</a>

    <p>A use-after-free vulnerability was found that could
    result in arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36056">CVE-2021-36056</a>

    <p>A buffer overflow vulnerability was found, potentially
    resulting in arbitrary code execution in the context of
    the current user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36057">CVE-2021-36057</a>

     <p>A write-what-where condition vulnerability was found,
     caused during the application's memory allocation process.
     This may cause the memory management functions to become
     mismatched resulting in local application denial of service
     in the context of the current user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36058">CVE-2021-36058</a>

    <p>An Integer Overflow vulnerability was found, potentially
    resulting in application-level denial of service in the
    context of the current user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36064">CVE-2021-36064</a>

    <p>A Buffer Underflow vulnerability was found which
    could result in arbitrary code execution in the context
    of the current user</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39847">CVE-2021-39847</a>

    <p>A stack-based buffer overflow vulnerability
    potentially resulting in arbitrary code execution in the
    context of the current user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40716">CVE-2021-40716</a>

    <p>An out-of-bounds read vulnerability was found that
    could lead to disclosure of sensitive memory</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40732">CVE-2021-40732</a>

    <p>A null pointer dereference vulnerability was found,
    that could result in leaking data from certain memory
    locations and causing a local denial of service</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42528">CVE-2021-42528</a>

    <p>A Null pointer dereference vulnerability was found
    when parsing a specially crafted file. An unauthenticated attacker
    could leverage this vulnerability to achieve an application
    denial-of-service in the context of the current user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42529">CVE-2021-42529</a>

    <p>A stack-based buffer overflow vulnerability was found
    potentially resulting in arbitrary code execution
    in the context of the current user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42530">CVE-2021-42530</a>

    <p>A stack-based buffer overflow vulnerability was found
    potentially resulting in arbitrary code execution in the
    context of the current user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42531">CVE-2021-42531</a>

    <p>A stack-based buffer overflow vulnerability
    potentially resulting in arbitrary code execution in
    the context of the current user</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42532">CVE-2021-42532</a>

    <p>A stack-based buffer overflow vulnerability
    potentially resulting in arbitrary code execution in the
    context of the current user.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.5.0-2+deb10u1.</p>

<p>We recommend that you upgrade your exempi packages.</p>

<p>For the detailed security status of exempi please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/exempi">https://security-tracker.debian.org/tracker/exempi</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3585.data"
# $Id: $
