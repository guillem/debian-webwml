<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Daniel Moghimi discovered Gather Data Sampling (GDS), a hardware
vulnerability for Intel CPUs which allows unprivileged speculative
access to data which was previously stored in vector registers.</p>

<p>This mitigation requires updated CPU microcode provided in the
intel-microcode package.</p>

<p>For details please refer to <a href="https://downfall.page/">https://downfall.page/</a> and
<a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/gather-data-sampling.html">https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/gather-data-sampling.html</a>.</p>

<p>For Debian 10 buster, this problem has been fixed in version
4.19.289-2.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3524.data"
# $Id: $
