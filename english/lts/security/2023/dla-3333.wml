<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several flaws were found in tiffcrop, a program distributed by tiff, a library
and tools providing support for the Tag Image File Format (TIFF).
A specially crafted tiff file can lead to an out-of-bounds write or read
resulting in a denial of service.</p>

<p>For Debian 10 buster, these problems have been fixed in version
4.1.0+git191117-2~deb10u7.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>For the detailed security status of tiff please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/tiff">https://security-tracker.debian.org/tracker/tiff</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3333.data"
# $Id: $
