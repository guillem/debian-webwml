<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in Thunderbird, which could
result in denial of service, the execution of arbitrary code or
spoofing.</p>

<p>For Debian 10 buster, these problems have been fixed in version
1:102.9.0-1~deb10u1.</p>

<p>We recommend that you upgrade your thunderbird packages.</p>

<p>For the detailed security status of thunderbird please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/thunderbird">https://security-tracker.debian.org/tracker/thunderbird</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3365.data"
# $Id: $
