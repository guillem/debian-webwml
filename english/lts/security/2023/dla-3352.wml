<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues were found in libde265, an open source implementation of the
h.265 video codec, which may result in denial of service, have unspecified
other impact, possibly code execution due to a heap-based buffer overflow.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47664">CVE-2022-47664</a>

    <p>	Libde265 1.0.9 is vulnerable to Buffer Overflow in ff_hevc_put_hevc_qpel_pixels_8_sse</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47665">CVE-2022-47665</a>

    <p>	Libde265 1.0.9 has a heap buffer overflow vulnerability in de265_image::set_SliceAddrRS(int, int, int)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24751">CVE-2023-24751</a>

    <p>libde265 v1.0.10 was discovered to contain a NULL pointer
    dereference in the mc_chroma function at motion.cc. This
    vulnerability allows attackers to cause a Denial of Service (DoS)
    via a crafted input file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24752">CVE-2023-24752</a>

    <p>libde265 v1.0.10 was discovered to contain a NULL pointer
    dereference in the ff_hevc_put_hevc_epel_pixels_8_sse function at
    sse-motion.cc. This vulnerability allows attackers to cause a Denial
    of Service (DoS) via a crafted input file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24754">CVE-2023-24754</a>

    <p>libde265 v1.0.10 was discovered to contain a NULL pointer
    dereference in the ff_hevc_put_weighted_pred_avg_8_sse function at
    sse-motion.cc. This vulnerability allows attackers to cause a Denial
    of Service (DoS) via a crafted input file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24755">CVE-2023-24755</a>

    <p>libde265 v1.0.10 was discovered to contain a NULL pointer
    dereference in the put_weighted_pred_8_fallback function at
    fallback-motion.cc. This vulnerability allows attackers to cause a
    Denial of Service (DoS) via a crafted input file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24756">CVE-2023-24756</a>

    <p>libde265 v1.0.10 was discovered to contain a NULL pointer
    dereference in the ff_hevc_put_unweighted_pred_8_sse function at
    sse-motion.cc. This vulnerability allows attackers to cause a Denial
    of Service (DoS) via a crafted input file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24757">CVE-2023-24757</a>

    <p>libde265 v1.0.10 was discovered to contain a NULL pointer
    dereference in the put_unweighted_pred_16_fallback function at
    fallback-motion.cc. This vulnerability allows attackers to cause a
    Denial of Service (DoS) via a crafted input file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24758">CVE-2023-24758</a>

    <p>libde265 v1.0.10 was discovered to contain a NULL pointer
    dereference in the ff_hevc_put_weighted_pred_avg_8_sse function at
    sse-motion.cc. This vulnerability allows attackers to cause a Denial
    of Service (DoS) via a crafted input file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-25221">CVE-2023-25221</a>

    <p>Libde265 v1.0.10 was discovered to contain a heap-buffer-overflow
    vulnerability in the derive_spatial_luma_vector_prediction function
    in motion.cc.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.0.11-0+deb10u4.</p>

<p>We recommend that you upgrade your libde265 packages.</p>

<p>For the detailed security status of libde265 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libde265">https://security-tracker.debian.org/tracker/libde265</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3352.data"
# $Id: $
