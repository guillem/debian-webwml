<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in sssd, a set of daemons to manage
access to remote directories and authentication mechanisms, which could
lead to privilege escalation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16838">CVE-2018-16838</a>

    <p>It was discovered that when the Group Policy Objects (GPO) are not
    readable by SSSD due to a too strict permission settings on the
    server side, SSSD allows all authenticated users to login instead of
    denying access.</p>

    <p>A new boolean setting <code>ad_gpo_ignore_unreadable</code> (defaulting to
    <code>False</code>) is introduced for environments where attributes in the
    </code>groupPolicyContainer</code> are not readable and changing the permissions
    on the GPO objects is not possible or desirable.  See <code>sssd-ad(5)</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3811">CVE-2019-3811</a>

    <p>It was discovered that if a user was configured with no home
    directory set, then <code>sssd(8)</code> returns <code>/</code> (i.e., the root directory)
    instead of the empty string (meaning no home directory).  This could
    impact services that restrict the user's filesystem access to within
    their home directory through <code>chroot()</code> or similar.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3621">CVE-2021-3621</a>

    <p>It was discovered that the <code>sssctl(8)</code> command was vulnerable to shell
    command injection via the <code>logs-fetch</code> and <code>cache-expire</code>
    subcommands.</p>

    <p>This flaw could allows an attacker to trick the root user into
    running a specially crafted <code>sssctl(8)</code> command, such as via sudo, in
    order to gain root privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4254">CVE-2022-4254</a>

    <p>It was discovered that <code>libsss_certmap</code> failed to sanitize certificate
    data used in LDAP filters.</p>

    <p>PKINIT enables a client to authenticate to the KDC using an X.509
    certificate and the corresponding private key, rather than a
    passphrase or keytab.  Mapping rules are used in order to map the
    certificate presented during a PKINIT authentication request to the
    corresponding principal.  However the mapping filter was found to be
    vulnerable to LDAP filter injection.  As the search result is be
    influenced by values in the certificate, which may be attacker
    controlled, this flaw could allow an attacker to gain control of the
    admin account, leading to full domain takeover.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.16.3-3.2+deb10u1.</p>

<p>We recommend that you upgrade your sssd packages.</p>

<p>For the detailed security status of sssd please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sssd">https://security-tracker.debian.org/tracker/sssd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3436.data"
# $Id: $
