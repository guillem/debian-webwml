<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that node-object-path, a Node.js module to access deep
object properties using dot-separated paths, was vulnerable to prototype
pollution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3805">CVE-2021-3805</a>

    <p>Prototype pollution vulnerability in the <code>del()</code>, <code>empty()</code>,
    <code>push()</code> and <code>insert()</code> functions when using the <q>inherited props</q>
    mode (e.g. when a new <code>object-path</code> instance is created with the
    <code>includeInheritedProps</code> option set to <code>true</code> or when using the
    <code>withInheritedProps</code> default instance).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23434">CVE-2021-23434</a>

    <p>A type confusion vulnerability can lead to a bypass of the
    <a href="https://security-tracker.debian.org/tracker/CVE-2020-15256">CVE-2020-15256</a> fix when the path components used in the path
    parameter are arrays, because the <code>===</code> operator returns always <code>false</code>
    when the type of the operands is different.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.11.4-2+deb10u2.</p>

<p>We recommend that you upgrade your node-object-path packages.</p>

<p>For the detailed security status of node-object-path please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-object-path">https://security-tracker.debian.org/tracker/node-object-path</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3291.data"
# $Id: $
