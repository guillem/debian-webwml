<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A Buffer Overflow vulnerability was found in the <code>LibRaw::stretch()</code>
function, which could lead to denial of service when parsing a malicious CRW file.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.19.2-2+deb10u4.</p>

<p>We recommend that you upgrade your libraw packages.</p>

<p>For the detailed security status of libraw please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libraw">https://security-tracker.debian.org/tracker/libraw</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3560.data"
# $Id: $
