<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a protential LDAP injection vulnerability
in Bouncy Castle, a cryptographic library for Java. During the certificate
validation process, bouncycastle used the certificate's "Subject Name" into an
LDAP search filter without any escaping.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-33201">CVE-2023-33201</a>

    <p>Bouncy Castle For Java before 1.74 is affected by an LDAP injection
    vulnerability. The vulnerability only affects applications that use an LDAP
    CertStore from Bouncy Castle to validate X.509 certificates. During the
    certificate validation process, Bouncy Castle inserts the certificate's
    Subject Name into an LDAP search filter without any escaping, which leads
    to an LDAP injection vulnerability.</p></li>
</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1.60-1+deb10u1.</p>

<p>We recommend that you upgrade your bouncycastle packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3514.data"
# $Id: $
