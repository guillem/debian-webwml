<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were found in lemonldap-ng, an OpenID-Connect, CAS
and SAML compatible Web-SSO system, that could result in information
disclosure or impersonation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16093">CVE-2020-16093</a>

    <p>Maxime Besson discovered that LemonLDAP::NG before 2.0.9 did not
    check validity of the X.509 certificate by default when connecting
    to remote LDAP backends, because the default configuration of the
    Net::LDAPS module for Perl is used.</p>

    <p>This update changes the default behavior to require X.509 validation
    against the distribution bundle <code>/etc/ssl/certs/ca-certificates.crt</code>.
    Previous behavior can reverted by running
    <code>/usr/share/lemonldap-ng/bin/lemonldap-ng-cli set ldapVerify none</code>.</p>

    <p>If a session backend is set to Apache::Session::LDAP or
    Apache::Session::Browseable::LDAP, then the complete fix involves
    upgrading the corresponding Apache::Session module
    (libapache-session-ldap-perl resp. libapache-session-browseable-perl)
    to 0.4-1+deb10u1 (or ≥0.5) resp. 1.3.0-1+deb10u1 (or ≥1.3.8).  See
    related advisories <a href="dla-3284">DLA-3284-1</a> and
    <a href="dla-3285">DLA-3285-1</a> for details.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37186">CVE-2022-37186</a>

    <p>Mickael Bride discovered that under certain conditions the session
    remained valid on handlers after being destroyed on portal.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.0.2+ds-7+deb10u8.</p>

<p>We recommend that you upgrade your lemonldap-ng packages.</p>

<p>For the detailed security status of lemonldap-ng please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/lemonldap-ng">https://security-tracker.debian.org/tracker/lemonldap-ng</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3287.data"
# $Id: $
