<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability has been found in phpCAS, a Central Authentication
Service client library in php, which may allow an attacker to gain
access to a victim's account on a vulnerable CASified service without
victim's knowledge, when the victim visits attacker's website while
being logged in to the same CAS server.</p>

<p>The fix for this vulnerabilty requires an API breaking change in php-cas
and will require that software using the library be updated.</p>

<p>For buster, all packages in the Debian repositories which are using
php-cas have been updated, though additional manual configuration is to
be expected, as php-cas needs additional site information -- the service
base URL -- for it to function. The DLAs for the respective packages
will have additional information, as well as the package's NEWS files.</p>

<p>For 3rd party software using php-cas, please be note that upstream
provided following instructions how to update this software [1]:</p>

<p>phpCAS now requires an additional service base URL argument when constructing
the client class. It accepts any argument of:</p>

<p>1. A service base URL string. The service URL discovery will always use this
   server name (protocol, hostname and port number) without using any external
   host names.
2. An array of service base URL strings. The service URL discovery will check
   against this list before using the auto discovered base URL. If there is no
   match, the first base URL in the array will be used as the default. This
   option is helpful if your PHP website is accessible through multiple domains
   without a canonical name, or through both HTTP and HTTPS.
3. A class that implements CAS_ServiceBaseUrl_Interface. If you need to
   customize the base URL discovery behavior, you can pass in a class that
   implements the interface.</p>

<p>Constructing the client class is usually done with phpCAS::client().</p>

<p>For example, using the first possiblity:
  phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context);
could become:
  phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context, "https://casified-service.example.org:8080");</p>


<p>Details of the vulnerability:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39369">CVE-2022-39369</a>

    <p>The phpCAS library uses HTTP headers to determine the service URL used
    to validate tickets. This allows an attacker to control the host header
    and use a valid ticket granted for any authorized service in the same
    SSO realm (CAS server) to authenticate to the service protected by
    phpCAS.  Depending on the settings of the CAS server service registry in
    worst case this may be any other service URL (if the allowed URLs are
    configured to "^(https)://.*") or may be strictly limited to known and
    authorized services in the same SSO federation if proper URL service
    validation is applied.</p>

<p>[1] <a href="https://github.com/apereo/phpCAS/blob/f3db27efd1f5020e71f2116f637a25cc9dbda1e3/docs/Upgrading#L1C1-L1C1">https://github.com/apereo/phpCAS/blob/f3db27efd1f5020e71f2116f637a25cc9dbda1e3/docs/Upgrading#L1C1-L1C1</a></p></li>

</ul>

<p>For Debian 10 buster, this problem has been fixed in version
1.3.6-1+deb10u1.</p>

<p>We recommend that you upgrade your php-cas packages.</p>

<p>For the detailed security status of php-cas please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/php-cas">https://security-tracker.debian.org/tracker/php-cas</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3485.data"
# $Id: $
