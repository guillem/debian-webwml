<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were found in PHP, a widely-used open source
general purpose scripting language, which could result in denial of
service or incorrect validation of BCrypt hashes.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31631">CVE-2022-31631</a>

    <p>Due to an uncaught integer overflow, <code>PDO::quote()</code> of
    <code>PDO_SQLite</code> may return an improperly quoted string.
    The exact details likely depend on the implementation of
    <code>sqlite3_snprintf()</code>, but with some versions it is possible to
    force the function to return a single apostrophe, if the function is
    called on user supplied input without any length restrictions in
    place.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0567">CVE-2023-0567</a>

    <p>Tim Düsterhus discovered that malformed BCrypt hashes that include a
    <code>&dollar;</code> character within their salt part trigger a buffer
    overread and may erroneously validate any password as valid.
    (<code>Password_verify()</code> always returns <code>true</code> with such
    inputs.)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0568">CVE-2023-0568</a>

    <p>1-byte array overrun when appending slash to paths during path
    resolution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0662">CVE-2023-0662</a>

    <p>Jakob Ackermann discovered a Denial of Service vulnerability when
    parsing multipart request body: the request body parsing in PHP
    allows any unauthenticated attacker to consume a large amount of CPU
    time and trigger excessive logging.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
7.3.31-1~deb10u3.</p>

<p>We recommend that you upgrade your php7.3 packages.</p>

<p>For the detailed security status of php7.3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/php7.3">https://security-tracker.debian.org/tracker/php7.3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3345.data"
# $Id: $
