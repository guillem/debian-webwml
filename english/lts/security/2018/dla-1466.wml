<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation or denial of service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5390">CVE-2018-5390</a>

    <p>(SegmentSmack)</p>

    <p>Juha-Matti Tilli discovered that a remote attacker can trigger the
    worst case code paths for TCP stream reassembly with low rates of
    specially crafted packets, leading to remote denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5391">CVE-2018-5391</a>

    <p>(FragmentSmack)</p>

    <p>Juha-Matti Tilli discovered a flaw in the way the Linux kernel
    handled reassembly of fragmented IPv4 and IPv6 packets. A remote
    attacker can take advantage of this flaw to trigger time and
    calculation expensive fragment reassembly algorithms by sending
    specially crafted packets, leading to remote denial of service.</p>

    <p>This is mitigated by reducing the default limits on memory usage
    for incomplete fragmented packets.  The same mitigation can be
    achieved without the need to reboot, by setting the sysctls:</p>

    <p>net.ipv4.ipfrag_high_thresh = 262144<br>
    net.ipv6.ip6frag_high_thresh = 262144<br>
    net.ipv4.ipfrag_low_thresh = 196608<br>
    net.ipv6.ip6frag_low_thresh = 196608</p>

    <p>The default values may still be increased by local configuration
    if necessary.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13405">CVE-2018-13405</a>

    <p>Jann Horn discovered that the inode_init_owner function in
    fs/inode.c in the Linux kernel allows local users to create files
    with an unintended group ownership allowing attackers to escalate
    privileges by making a plain file executable and SGID.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.9.110-3+deb9u2~deb8u1. This update includes fixes for several
regressions in the latest point release.</p>

<p>The earlier version 4.9.110-3+deb9u1~deb8u1 included all the above
fixes except for <a href="https://security-tracker.debian.org/tracker/CVE-2018-5391">CVE-2018-5391</a>, 
which may be mitigated as explained above.</p>

<p>We recommend that you upgrade your linux-4.9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1466.data"
# $Id: $
