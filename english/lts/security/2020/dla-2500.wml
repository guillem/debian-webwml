<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in curl, a command line tool for
transferring data with URL syntax and an easy-to-use client-side URL
transfer library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8284">CVE-2020-8284</a>

    <p>When curl performs a passive FTP transfer, it first tries the EPSV
    command and if that is not supported, it falls back to using PASV.
    Passive mode is what curl uses by default.  A server response to a
    PASV command includes the (IPv4) address and port number for the
    client to connect back to in order to perform the actual data
    transfer.  This is how the FTP protocol is designed to work.  A
    malicious server can use the PASV response to trick curl into
    connecting back to a given IP address and port, and this way
    potentially make curl extract information about services that are
    otherwise private and not disclosed, for example doing port scanning
    and service banner extractions.</p>

    <p>The IP address part of the response is now ignored by default, by
    making CURLOPT_FTP_SKIP_PASV_IP default to 1L instead of previously
    being 0L.  This has the minor drawback that a small fraction of use
    cases might break, when a server truly needs the client to connect
    back to a different IP address than what the control connection uses
    and for those CURLOPT_FTP_SKIP_PASV_IP can be set to 0L.  The same
    goes for the command line tool, which then might need
    --no-ftp-skip-pasv-ip set to prevent curl from ignoring the address
    in the server response.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8285">CVE-2020-8285</a>

    <p>libcurl offers a wildcard matching functionality, which allows a
    callback (set with CURLOPT_CHUNK_BGN_FUNCTION) to return information
    back to libcurl on how to handle a specific entry in a directory
    when libcurl iterates over a list of all available entries.  When
    this callback returns CURL_CHUNK_BGN_FUNC_SKIP, to tell libcurl to
    not deal with that file, the internal function in libcurl then calls
    itself recursively to handle the next directory entry.  If there's a
    sufficient amount of file entries and if the callback returns <q>skip</q>
    enough number of times, libcurl runs out of stack space.  The exact
    amount will of course vary with platforms, compilers and other
    environmental factors.  The content of the remote directory is not
    kept on the stack, so it seems hard for the attacker to control
    exactly what data that overwrites the stack - however it remains a
    Denial-Of-Service vector as a malicious user who controls a server
    that a libcurl-using application works with under these premises can
    trigger a crash.</p>

    <p>The internal function is rewritten to instead and more appropriately
    use an ordinary loop instead of the recursive approach. This way,
    the stack use will remain the same no matter how many files that are
    skipped.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8286">CVE-2020-8286</a>

    <p>libcurl offers <q>OCSP stapling</q> via the CURLOPT_SSL_VERIFYSTATUS
    option. When set, libcurl verifies the OCSP response that a server
    responds with as part of the TLS handshake.  It then aborts the TLS
    negotiation if something is wrong with the response. The same
    feature can be enabled with --cert-status using the curl tool.  As
    part of the OCSP response verification, a client should verify that
    the response is indeed set out for the correct certificate.  This
    step was not performed by libcurl when built or told to use OpenSSL
    as TLS backend.  This flaw would allow an attacker, who perhaps
    could have breached a TLS server, to provide a fraudulent OCSP
    response that would appear fine, instead of the real one.  Like if
    the original certificate actually has been revoked.</p>

    <p>The OCSP response checker function now also verifies that the
    certificate id is the correct one.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
7.52.1-5+deb9u13.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>For the detailed security status of curl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/curl">https://security-tracker.debian.org/tracker/curl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2500.data"
# $Id: $
