<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were found in package phpmyadmin.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19617">CVE-2019-19617</a>

    <p>phpMyAdmin does not escape certain Git information, related to
    libraries/classes/Display/GitRevision.php and libraries/classes
    /Footer.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26934">CVE-2020-26934</a>

    <p>A vulnerability was discovered where an attacker can cause an XSS
    attack through the transformation feature.</p>

    <p>If an attacker sends a crafted link to the victim with the malicious
    JavaScript, when the victim clicks on the link, the JavaScript will run
    and complete the instructions made by the attacker.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26935">CVE-2020-26935</a>

    <p>An SQL injection vulnerability was discovered in how phpMyAdmin
    processes SQL statements in the search feature. An attacker could use
    this flaw to inject malicious SQL in to a query.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.6.6-4+deb9u2.</p>

<p>We recommend that you upgrade your phpmyadmin packages.</p>

<p>For the detailed security status of phpmyadmin please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/phpmyadmin">https://security-tracker.debian.org/tracker/phpmyadmin</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2413.data"
# $Id: $
