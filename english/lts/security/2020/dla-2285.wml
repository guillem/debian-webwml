<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in librsvg, an SVG rendering
library. This update corrects some denial of service issues via
exponential element processing, stack exhaustion or application crash
when processing specially crafted files, as well as some memory safety
issues.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
2.40.21-0+deb9u1.</p>

<p>We recommend that you upgrade your librsvg packages.</p>

<p>For the detailed security status of librsvg please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/librsvg">https://security-tracker.debian.org/tracker/librsvg</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2285.data"
# $Id: $
