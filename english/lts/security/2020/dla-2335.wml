<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in ghostscript, an interpreter for
the PostScript language and for PDF, allowing an attacker to escalate
privileges and cause denial of service via crafted PS/EPS/PDF files.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
9.26a~dfsg-0+deb9u7.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>For the detailed security status of ghostscript please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ghostscript">https://security-tracker.debian.org/tracker/ghostscript</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2335.data"
# $Id: $
