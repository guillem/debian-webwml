<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>steelo discovered a remote code execution vulnerability in Samba, a
SMB/CIFS file, print, and login server for Unix. A malicious client with
access to a writable share, can take advantage of this flaw by uploading
a shared library and then cause the server to load and execute it.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:3.6.6-6+deb7u13.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-951.data"
# $Id: $
