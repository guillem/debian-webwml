<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Max Justicz reported a directory traversal vulnerability in
Dpkg::Source::Archive in dpkg, the Debian package management system.
This affects extracting untrusted source packages in the v2 and v3
source package formats that include a debian.tar.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.18.26.</p>

<p>We recommend that you upgrade your dpkg packages.</p>

<p>For the detailed security status of dpkg please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/dpkg">https://security-tracker.debian.org/tracker/dpkg</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3022.data"
# $Id: $
