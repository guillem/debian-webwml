<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in PHP, a widely-used open
source general purpose scripting language which could result in denial
of service, information disclosure, insecure cooking handling or
potentially the execution of arbitrary code.</p>

<p>For Debian 10 buster, these problems have been fixed in version
7.3.31-1~deb10u2.</p>

<p>We recommend that you upgrade your php7.3 packages.</p>

<p>For the detailed security status of php7.3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/php7.3">https://security-tracker.debian.org/tracker/php7.3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3243.data"
# $Id: $
