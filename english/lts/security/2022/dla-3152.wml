<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update fixes a wide range of vulnerabilities. A significant portion
affects character set conversion.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10228">CVE-2016-10228</a>

    <p>The iconv program in the GNU C Library when invoked with multiple
    suffixes in the destination encoding (TRANSLATE or IGNORE) along with
    the -c option, enters an infinite loop when processing invalid
    multi-byte input sequences, leading to a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19126">CVE-2019-19126</a>

    <p>On the x86-64 architecture, the GNU C Library fails to ignore the
    LD_PREFER_MAP_32BIT_EXEC environment variable during program
    execution after a security transition, allowing local attackers to
    restrict the possible mapping addresses for loaded libraries and
    thus bypass ASLR for a setuid program.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-25013">CVE-2019-25013</a>

    <p>The iconv feature in the GNU C Library, when processing invalid
    multi-byte input sequences in the EUC-KR encoding, may have a buffer
    over-read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10029">CVE-2020-10029</a>

    <p>The GNU C Library could overflow an on-stack buffer during range
    reduction if an input to an 80-bit long double function contains a
    non-canonical bit pattern, a seen when passing a
    0x5d414141414141410000 value to sinl on x86 targets. This is related
    to sysdeps/ieee754/ldbl-96/e_rem_pio2l.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1752">CVE-2020-1752</a>

    <p>A use-after-free vulnerability introduced in glibc was found in the
    way the tilde expansion was carried out. Directory paths containing
    an initial tilde followed by a valid username were affected by this
    issue. A local attacker could exploit this flaw by creating a
    specially crafted path that, when processed by the glob function,
    would potentially lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27618">CVE-2020-27618</a>

    <p>The iconv function in the GNU C Library, when processing invalid
    multi-byte input sequences in IBM1364, IBM1371, IBM1388, IBM1390,
    and IBM1399 encodings, fails to advance the input state, which could
    lead to an infinite loop in applications, resulting in a denial of
    service, a different vulnerability from <a href="https://security-tracker.debian.org/tracker/CVE-2016-10228">CVE-2016-10228</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6096">CVE-2020-6096</a>

    <p>An exploitable signed comparison vulnerability exists in the ARMv7
    memcpy() implementation of GNU glibc. Calling memcpy() (on ARMv7
    targets that utilize the GNU glibc implementation) with a negative
    value for the <q>num</q> parameter results in a signed comparison
    vulnerability. If an attacker underflows the <q>num</q> parameter to
    memcpy(), this vulnerability could lead to undefined behavior such as
    writing to out-of-bounds memory and potentially remote code
    execution.  Furthermore, this memcpy() implementation allows for
    program execution to continue in scenarios where a segmentation fault
    or crash should have occurred. The dangers occur in that subsequent
    execution and iterations of this code will be executed with this
    corrupted data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27645">CVE-2021-27645</a>

    <p>The nameserver caching daemon (nscd) in the GNU C Library, when
    processing a request for netgroup lookup, may crash due to a
    double-free, potentially resulting in degraded service or Denial of
    Service on the local system. This is related to netgroupcache.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3326">CVE-2021-3326</a>

    <p>The iconv function in the GNU C Library, when processing invalid
    input sequences in the ISO-2022-JP-3 encoding, fails an assertion in
    the code path and aborts the program, potentially resulting in a
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33574">CVE-2021-33574</a>

    <p>The mq_notify function in the GNU C Library has a use-after-free. It
    may use the notification thread attributes object (passed through
    its struct sigevent parameter) after it has been freed by the caller,
    leading to a denial of service (application crash) or possibly
    unspecified other impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-35942">CVE-2021-35942</a>

    <p>The wordexp function in the GNU C Library may crash or read arbitrary
    memory in parse_param (in posix/wordexp.c) when called with an
    untrusted, crafted pattern, potentially resulting in a denial of
    service or disclosure of information. This occurs because atoi was
    used but strtoul should have been used to ensure correct calculations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3999">CVE-2021-3999</a>

    <p>An off-by-one buffer overflow and underflow in getcwd() may lead to
    memory corruption when the size of the buffer is exactly 1. A local
    attacker who can control the input buffer and size passed to getcwd()
    in a setuid program could use this flaw to potentially execute
    arbitrary code and escalate their privileges on the system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23218">CVE-2022-23218</a>

    <p>The deprecated compatibility function svcunix_create in the sunrpc
    module of the GNU C Library copies its path argument on the stack
    without validating its length, which may result in a buffer overflow,
    potentially resulting in a denial of service or (if an application
    is not built with a stack protector enabled) arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23219">CVE-2022-23219</a>

    <p>The deprecated compatibility function clnt_create in the sunrpc module
    of the GNU C Library copies its hostname argument on the stack without
    validating its length, which may result in a buffer overflow,
    potentially resulting in a denial of service or (if an application is
    not built with a stack protector enabled) arbitrary code execution.</p>


<p>For Debian 10 buster, these problems have been fixed in version
2.28-10+deb10u2.</p>

<p>We recommend that you upgrade your glibc packages.</p>

<p>For the detailed security status of glibc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/glibc">https://security-tracker.debian.org/tracker/glibc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3152.data"
# $Id: $
