<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>There was a potential HTTP request smuggling vulnerability in
http-parser, a popular library for parsing HTTP messages.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.8.1-1+deb10u3.</p>

<p>We recommend that you upgrade your http-parser packages.</p>

<p>For the detailed security status of http-parser please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/http-parser">https://security-tracker.debian.org/tracker/http-parser</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3224.data"
# $Id: $
