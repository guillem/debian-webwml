<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<p>This update is unfortunately not available for the armel architecture.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1108">CVE-2018-1108</a>

    <p>It was discovered that the random driver could generate random
    bytes through /dev/random and the getrandom() system call before
    gathering enough entropy that these would be unpredictable.  This
    could compromise the confidentiality and integrity of encrypted
    communications.</p>

    <p>The original fix for this issue had to be reverted because it
    caused the boot process to hang on many systems.  In this version,
    the random driver has been updated, making it more effective in
    gathering entropy without needing a hardware RNG.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4149">CVE-2021-4149</a>

    <p>Hao Sun reported a flaw in the Btrfs fileysstem driver. There
    is a potential lock imbalance in an error path.  A local user
    might be able to exploit this for denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39713">CVE-2021-39713</a>

    <p>The syzbot tool found a race condition in the network scheduling
    subsystem which could lead to a use-after-free.  A local user
    could exploit this for denial of service (memory corruption or
    crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0494">CVE-2022-0494</a>

    <p>The scsi_ioctl() was susceptible to an information leak only
    exploitable by users with CAP_SYS_ADMIN or CAP_SYS_RAWIO
    capabilities.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0812">CVE-2022-0812</a>

    <p>It was discovered that the RDMA transport for NFS (xprtrdma)
    miscalculated the size of message headers, which could lead to a
    leak of sensitive information between NFS servers and clients.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0854">CVE-2022-0854</a>

    <p>Ali Haider discovered a potential information leak in the DMA
    subsystem. On systems where the swiotlb feature is needed, this
    might allow a local user to read sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1011">CVE-2022-1011</a>

    <p>Jann Horn discovered a flaw in the FUSE (Filesystem in User-Space)
    implementation. A local user permitted to mount FUSE filesystems
    could exploit this to cause a use-after-free and read sensitive
    information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1012">CVE-2022-1012</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-32296">CVE-2022-32296</a></p>

    <p>Moshe Kol, Amit Klein, and Yossi Gilad discovered a weakness
    in randomisation of TCP source port selection.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1016">CVE-2022-1016</a>

    <p>David Bouman discovered a flaw in the netfilter subsystem where
    the nft_do_chain function did not initialize register data that
    nf_tables expressions can read from and write to. A local attacker
    can take advantage of this to read sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1198">CVE-2022-1198</a>

    <p>Duoming Zhou discovered a race condition in the 6pack hamradio
    driver, which could lead to a use-after-free. A local user could
    exploit this to cause a denial of service (memory corruption or
    crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1199">CVE-2022-1199</a>

    <p>Duoming Zhou discovered race conditions in the AX.25 hamradio
    protocol, which could lead to a use-after-free or null pointer
    dereference. A local user could exploit this to cause a denial of
    service (memory corruption or crash) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1353">CVE-2022-1353</a>

    <p>The TCS Robot tool found an information leak in the PF_KEY
    subsystem. A local user can receive a netlink message when an
    IPsec daemon registers with the kernel, and this could include
    sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1516">CVE-2022-1516</a>

    <p>A NULL pointer dereference flaw in the implementation of the X.25
    set of standardized network protocols, which can result in denial
    of service.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1729">CVE-2022-1729</a>

    <p>Norbert Slusarek discovered a race condition in the perf subsystem
    which could result in local privilege escalation to root. The
    default settings in Debian prevent exploitation unless more
    permissive settings have been applied in the
    kernel.perf_event_paranoid sysctl.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1734">CVE-2022-1734</a>

    <p>Duoming Zhou discovered race conditions in the nfcmrvl NFC driver
    that could lead to a use-after-free, double-free or null pointer
    dereference. A local user might be able to exploit these for
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1974">CVE-2022-1974</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-1975">CVE-2022-1975</a></p>

    <p>Duoming Zhou discovered that the NFC netlink interface was
    suspectible to denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2153">CVE-2022-2153</a>

    <p><q>kangel</q> reported a flaw in the KVM implementation for x86
    processors which could lead to a null pointer dereference. A local
    user permitted to access /dev/kvm could exploit this to cause a
    denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21123">CVE-2022-21123</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-21125">CVE-2022-21125</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-21166">CVE-2022-21166</a></p>

    <p>Various researchers discovered flaws in Intel x86 processors,
    collectively referred to as MMIO Stale Data vulnerabilities.
    These are similar to the previously published Microarchitectural
    Data Sampling (MDS) issues and could be exploited by local users
    to leak sensitive information.</p>

    <p>For some CPUs, the mitigations for these issues require updated
    microcode.  An updated intel-microcode package may be provided at
    a later date.  The updated CPU microcode may also be available as
    part of a system firmware ("BIOS") update.</p>

    <p>Further information on the mitigation can be found at
    <https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/processor_mmio_stale_data.html>
    or in the linux-doc-4.9 package.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23036">CVE-2022-23036</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-23037">CVE-2022-23037</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-23038">CVE-2022-23038</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-23039">CVE-2022-23039</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-23040">CVE-2022-23040</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-23041">CVE-2022-23041</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-23042">CVE-2022-23042</a> (XSA-396)</p>

    <p>Demi Marie Obenour and Simon Gaiser of Invisible Things Lab
    discovered flaws in several Xen PV device frontends. These drivers
    misused the Xen grant table API in a way that could be exploited
    by a malicious device backend to cause data corruption, leaks of
    sensitive information, or a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23960">CVE-2022-23960</a>

    <p>Researchers at VUSec discovered that the Branch History Buffer in
    Arm processors can be exploited to create information side    channels with speculative execution.  This issue is similar to
    Spectre variant 2, but requires additional mitigations on some
    processors.</p>

    <p>This can be exploited to obtain sensitive information from a
    different security context, such as from user-space to the kernel,
    or from a KVM guest to the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24958">CVE-2022-24958</a>

    <p>A flaw was discovered that the USB gadget subsystem that could
    lead to a use-after-free. A local user permitted to configure USB
    gadgets could exploit this to cause a denial of service (crash or
    memory corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26490">CVE-2022-26490</a>

    <p>Buffer overflows in the STMicroelectronics ST21NFCA core driver
    can result in denial of service or privilege escalation.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26966">CVE-2022-26966</a>

    <p>A flaw was discovered in the sr9700 USB networking driver. A local
    user able to attach a specially designed USB device could use this
    to leak sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27223">CVE-2022-27223</a>

    <p>A flaw was discovered in the udc-xilinx USB gadget-mode controller
    driver. On systems using this driver, a malicious USB host could
    exploit this to cause a denial of service (crash or memory
    corruption) or possibly to execute arbitrary code.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28356">CVE-2022-28356</a>

    <p><q>Beraphin</q> discovered that the ANSI/IEEE 802.2 LLC type 2 driver did
    not properly perform reference counting on some error paths. A
    local attacker can take advantage of this flaw to cause a denial
    of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28390">CVE-2022-28390</a>

    <p>A double free vulnerability was discovered in the EMS CPC-USB/ARM7
    CAN/USB interface driver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30594">CVE-2022-30594</a>

    <p>Jann Horn discovered a flaw in the interaction between ptrace and
    seccomp subsystems. A process sandboxed using seccomp() but still
    permitted to use ptrace() could exploit this to remove the seccomp
    restrictions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32250">CVE-2022-32250</a>

    <p>Aaron Adams discovered a use-after-free in Netfilter which may
    result in local privilege escalation to root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33981">CVE-2022-33981</a>

    <p>Yuan Ming from Tsinghua University reported a a race condition in
    the floppy driver involving use of the FDRAWCMD ioctl, which could
    lead to a use-after-free. A local user with access to a floppy
    drive device could exploit this to cause a denial of service
    (crash or memory corruption) or possibly for privilege escalation.
    This ioctl is now disabled by default.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.9.320-2.</p>

<p>For the armhf architecture, this update enables optimised
implementations of several cryptographic and CRC algorithms.  For at
least AES, this should remove a timing side-channel that could lead to
a leak of sensitive information.</p>

<p>This update includes many more bug fixes from stable updates
4.9.304-4.9.320 inclusive.  The random driver has been backported from
Linux 5.19, fixing numerous performance and correctness issues.  Some
changes will be visible:</p>

<p>- The entropy pool size is now 256 bits instead of 4096.  You may need
  to adjust the configuration of system monitoring or user-space
  entropy gathering services to allow for this.</p>

<p>- On systems without a hardware RNG, the kernel will log many more
  uses of /dev/urandom before it is fully initialised.  These uses
  were previously under-counted and this is not a regression.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3065.data"
# $Id: $
