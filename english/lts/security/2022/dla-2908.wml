<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in librecad, a
computer-aided design (CAD) system. Buffer overflows may lead to remote code
execution if a specially crafted JWW document is processed.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
2.1.2-1+deb9u3.</p>

<p>We recommend that you upgrade your librecad packages.</p>

<p>For the detailed security status of librecad please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/librecad">https://security-tracker.debian.org/tracker/librecad</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2908.data"
# $Id: $
