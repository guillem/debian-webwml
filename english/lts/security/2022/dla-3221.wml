<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Cristian-Alexandru Staicu discovered a prototype pollution vulnerability
in inode-cached-path-relative, a Node.js module used to cache (memoize)
the result of path.relative.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16472">CVE-2018-16472</a>

    <p>An attacker controlling both the path and the cached value, can
    mount a prototype pollution attack and thus overwrite arbitrary
    properties on Object.prototype, which may result in denial of
    service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23518">CVE-2021-23518</a>

    <p>The fix for <a href="https://security-tracker.debian.org/tracker/CVE-2018-16472">CVE-2018-16472</a> was incomplete and other prototype
    pollution vulnerabilities were found in the meantime, resulting in a
    new CVE.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.0.1-2+deb10u1.</p>

<p>We recommend that you upgrade your node-cached-path-relative packages.</p>

<p>For the detailed security status of node-cached-path-relative please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-cached-path-relative">https://security-tracker.debian.org/tracker/node-cached-path-relative</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3221.data"
# $Id: $
