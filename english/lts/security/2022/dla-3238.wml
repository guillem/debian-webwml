<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in pngcheck, a tool to verify the
integrity of PNG, JNG and MNG files, which could potentially result
in the execution of arbitrary code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35511">CVE-2020-35511</a>

    <p>A global buffer overflow was discovered in pngcheck function in
    pngcheck-2.4.0 (5 patches applied) via a crafted png file.</p>


<p>For Debian 10 buster, these problems have been fixed in version
3.0.3-1~deb10u2.</p>

<p>We recommend that you upgrade your pngcheck packages.</p>

<p>For the detailed security status of pngcheck please refer to its security
tracker page at: <a href="https://security-tracker.debian.org/tracker/pngcheck">https://security-tracker.debian.org/tracker/pngcheck</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3238.data"
# $Id: $
