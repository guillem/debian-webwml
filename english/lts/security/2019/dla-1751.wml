<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in suricata, the network threat
detection engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10242">CVE-2018-10242</a>

    <p>Missing length check causing out-of-bounds read in SSHParseBanner
    (app-layer-ssh.c). Remote attackers might leverage this vulnerability
    to cause DoS or potentially unauthorized disclosure of information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10243">CVE-2018-10243</a>

    <p>Unexpected end of Authorization field causing heap-based buffer
    over-read in htp_parse_authorization_digest (htp_parsers.c, from the
    embedded copy of LibHTP). Remote attackers might leverage this
    vulnerability to cause DoS or potentially unauthorized disclosure of
    information.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.0.7-2+deb8u4.</p>

<p>We recommend that you upgrade your suricata packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1751.data"
# $Id: $
