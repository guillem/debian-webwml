<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jann Horn discovered that the ptrace subsystem in the Linux kernel
mishandles the management of the credentials of a process that wants
to create a ptrace relationship, allowing a local user to obtain root
privileges under certain scenarios.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4.9.168-1+deb9u4~deb8u1.</p>

<p>We recommend that you upgrade your linux-4.9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1863.data"
# $Id: $
