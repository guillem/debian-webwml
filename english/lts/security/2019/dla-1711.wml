<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A memory leak was discovered in the backport of fixes for
<a href="https://security-tracker.debian.org/tracker/CVE-2018-16864">CVE-2018-16864</a> in systemd-journald.</p>

<p>Function dispatch_message_real() in journald-server.c does not free
allocated memory to store the `_CMDLINE=` entry. A local attacker may
use this flaw to make systemd-journald crash.</p>

<p>Note that as the systemd-journald service is not restarted automatically
a restart of the service or more safely a reboot is advised.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
215-17+deb8u11.</p>

<p>We recommend that you upgrade your systemd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1711.data"
# $Id: $
