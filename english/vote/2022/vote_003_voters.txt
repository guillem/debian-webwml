-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    1           93sam	Steve McIntyre
    2             abe	Axel Beckert
    3           aboll	Andreas Boll
    4          absurd	Stephan Suerken
    5         adejong	Arthur de Jong
    6       adrianorg	Adriano Rafael Gomes
    7            adsb	Adam D. Barratt
    8       aferraris	Arnaud Ferraris
    9             agx	Guido Guenther
   10        aigarius	Aigars Mahinovs
   11           alexm	Alex Muntada
   12           alexp	Alex Pennace
   13        amacater	Andrew Martin Adrian Cater
   14           amaya	Amaya Rodrigo Sastre
   15        ametzler	Andreas Metzler
   16             ana	Ana Beatriz Guerrero López
   17         anarcat	Antoine Beaupré
   18            anbe	Andreas Beckmann
   19            andi	Andreas B. Mundt
   20        andrewsh	Andrej Shadura
   21        angdraug	Dmitry Borodaenko
   22           angel	Angel Abad
   23          anibal	Anibal Monsalve Salazar
   24          ansgar	Ansgar
   25        anuradha	Anuradha Weeraman
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   26             apo	Markus Koschany
   27            aron	Aron Xu
   28          arturo	Arturo Borrero González
   29         aurel32	Aurelien Jarno
   30              az	Alexander Zangerl
   31        azekulic	Alen Zekulic
   32        ballombe	Bill Allombert
   33             bam	Brian May
   34           bartm	Bart Martens
   35             bas	Bas Zoetekouw
   36          bbaren	Benjamin Barenblat
   37           bdale	Bdale Garbee
   38          bengen	Hilko Bengen
   39            benh	Ben Hutchings
   40          bernat	Vincent Bernat
   41           berni	Bernhard Schmidt
   42           berto	Alberto Garcia
   43            beuc	Sylvain Beucler
   44           biebl	Michael Biebl
   45         bigeasy	Sebastian Andrzej Siewior
   46           blade	Eduard Bloch
   47           bluca	Luca Boccassi
   48           bmarc	Bertrand Marc
   49             bod	Brendan O'Dea
   50           bootc	Chris Boot
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   51         branden	Branden Robinson
   52         broonie	Mark Brown
   53            bunk	Adrian Bunk
   54           byang	Boyuan Yang
   55            bzed	Bernd Zeimetz
   56        calculus	Jerome Georges Benoit
   57          carnil	Salvatore Bonaccorso
   58        cascardo	Thadeu Cascardo
   59           cavok	Domenico Andreoli
   60            cech	Petr Cech
   61          chrism	Christoph Martin
   62       chronitis	Gordon Ball
   63        cjwatson	Colin Watson
   64             ckk	Christian Kastner
   65           cklin	Chuan-kai Lin
   66           clint	Clint Adams
   67             cmb	Chris Boyle
   68        codehelp	Neil Williams
   69         coucouf	Aurélien Couderc
   70          csmall	Craig Small
   71             cts	Christian T. Steigies
   72           curan	Kai Wasserbäch
   73           cwryu	Changwoo Ryu
   74          czchen	ChangZhuo Chen
   75             dai	Daisuke Higuchi
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   76          daniel	Daniel Baumann
   77            dank	Nick Loren Black
   78           dannf	Dann Frazier
   79            dave	Dave Holland
   80       debalance	Philipp Huebner
   81        deltaone	Patrick Franz
   82       dktrkranz	Luca Falavigna
   83          dlange	Daniel Lange
   84           dlehn	David I. Lehn
   85        dleidert	Daniel Leidert
   86             dmn	Damyan Ivanov
   87             dod	Dominique Dumont
   88         dogsleg	Lev Lamberov
   89         donkult	David Kalnischkies
   90        dsilvers	Daniel Silverstone
   91            duck	Marc Dequènes
   92          ebourg	Emmanuel Bourg
   93             edd	Dirk Eddelbuettel
   94         edmonds	Robert Edmonds
   95          eevans	Eric Evans
   96            eike	Eike Sauer
   97          elbrus	Paul Mathijs Gevers
   98             ema	Emanuele Rocca
   99        emollier	Étienne Mollier
  100         emorrp1	Phil Morrell
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  101          enrico	Enrico Zini
  102        eriberto	Joao Eriberto Mota Filho
  103           eriks	Erik Schanze
  104           eriol	Daniele Tricoli
  105           fabbe	Fabian Fagerholm
  106          fabian	Fabian Greffrath
  107             faw	Felipe Augusto van de Wiel
  108        federico	Federico Ceratto
  109          fgeyer	Felix Geyer
  110         filippo	Filippo Giunchedi
  111         florian	Florian Ernst
  112        formorer	Alexander Wirt
  113         fpeters	Frederic Peters
  114        francois	Francois Marier
  115           fuddl	Bruno Kleinert
  116         genannt	Jonas Genannt
  117        georgesk	Georges Khaznadar
  118             gfa	Gustavo Panizzo
  119           ghedo	Alessandro Ghedini
  120             gio	Giovanni Mascellani
  121         giovani	Giovani Augusto Ferreira
  122           gladk	Anton Gladky
  123          glondu	Stéphane Glondu
  124          gniibe	NIIBE Yutaka
  125          gregoa	Gregor Herrmann
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  126            gspr	Gard Spreemann
  127         guilhem	Guilhem Moulin
  128         guillem	Guillem Jover
  129          gusnan	Andreas Rönnquist
  130            guus	Guus Sliepen
  131           gwolf	Gunnar Wolf
  132        hartmans	Sam Hartman
  133           hefee	Sandro Knauß
  134         helmutg	Helmut Grohne
  135         hertzog	Raphaël Hertzog
  136      hlieberman	Harlan Lieberman-Berg
  137             hmh	Henrique de Moraes Holschuh
  138         hoexter	Sven Hoexter
  139          holger	Holger Levsen
  140      hvhaugwitz	Hannes von Haugwitz
  141             ijc	Ian James Campbell
  142       intrigeri	Intrigeri
  143          iustin	Iustin Pop
  144        iwamatsu	Nobuhiro Iwamatsu
  145             iwj	Ian Jackson
  146             jak	Julian Andres Klode
  147        jamessan	James McCoy
  148           jandd	Jan Dittberner
  149             jas	Simon Josefsson
  150          jathan	Jonathan Bustillos
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  151          jbicha	Jeremy Bicha
  152             jcc	Jonathan Cristopher Carter
  153            jcfp	Jeroen Ploemen
  154        jcristau	Julien Cristau
  155          jlines	John Lines
  156             jmm	Moritz Muehlenhoff
  157            jmtd	Jonathan Dowland
  158             jmw	Jonathan Wiltshire
  159           joerg	Joerg Jaspert
  160           johns	John Sullivan
  161         joostvb	Joost van Baal
  162           josch	Johannes Schauer Marin Rodrigues
  163           josue	Josué Ortega
  164         joussen	Mario Joussen
  165             joy	Josip Rodin
  166          jrtc27	Jessica Clarke
  167              js	Jonas Smedegaard
  168        jspricke	Jochen Sprickerhof
  169       jvalleroy	James Valleroy
  170          kaliko	Geoffroy Berret
  171          keithp	Keith Packard
  172          kenhys	HAYASHI Kentaro
  173          khalid	Khalid Aziz
  174            kibi	Cyril Brulebois
  175        kilobyte	Adam Borowski
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  176       kitterman	Scott Kitterman
  177      kkremitzki	Kurt Kremitzki
  178            knok	Takatsugu Nokubi
  179           kobla	Ondřej Kobližek
  180          koster	Kanru Chen
  181         kreckel	Richard Kreckel
  182      kritzefitz	Sven Bartscher
  183            kula	Marcin Kulisz
  184           lamby	Chris Lamb
  185           laney	Iain Lane
  186           lange	Thomas Lange
  187         larjona	Laura Arjona Reina
  188        lavamind	Jerome Charaoui
  189        lawrencc	Christopher Lawrence
  190         lechner	Felix Lechner
  191         legoktm	Kunal Mehta
  192         lenharo	Daniel Lenharo de Souza
  193             leo	Carsten Leonhardt
  194        lightsey	John Lightsey
  195           lindi	Timo Juhani Lindfors
  196         lingnau	Anselm Lingnau
  197            lool	Loïc Minier
  198         lopippo	Filippo Rusconi
  199           lucab	Luca Bruno
  200           lucas	Lucas Nussbaum
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  201           luigi	Luigi Gangitano
  202           lumin	Mo Zhou
  203         madduck	Martin F. Krafft
  204             mak	Matthias Klumpp
  205           mattb	Matthew Brown
  206         matthew	Matthew Vernon
  207          mattia	Mattia Rizzolo
  208           mazen	Mazen Neifer
  209          mbanck	Michael Banck
  210         mbehrle	Mathias Behrle
  211              md	Marco d'Itri
  212            mejo	Jonas Meurer
  213             mfv	Matteo F. Vescovi
  214             mhy	Mark Hymers
  215           micha	Micha Lenk
  216            mika	Michael Prokop
  217        mjeanson	Michael Jeanson
  218             mjr	Mark J Ray
  219           mones	Ricardo Mones Lastra
  220           moray	Moray Allan
  221           morph	Sandro Tosi
  222           mpitt	Martin Pitt
  223        mquinson	Martin Quinson
  224          mstone	Michael Stone
  225     mtecknology	Michael Lustfield
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  226            mwei	Ming-ting Yao Wei
  227            myon	Christoph Berg
  228          nbreen	Nicholas Breen
  229           neilm	Neil McGovern
  230           nickm	Nick Morrott
  231          nilesh	Nilesh Patra
  232           noahm	Noah Meyerhans
  233          nodens	Clément Hermann
  234            noel	Noèl Köthe
  235         noodles	Jonathan McDowell
  236        nthykier	Niels Thykier
  237           ntyni	Niko Tyni
  238            odyx	Didier Raboud
  239           ohura	Makoto OHURA
  240           olasd	Nicolas Dandrimont
  241         olebole	Ole Streicher
  242            olly	Olly Betts
  243         onlyjob	Dmitry Smirnov
  244           osamu	Osamu Aoki
  245            pabs	Paul Wise
  246    paddatrapper	Kyle Robbertze
  247          paride	Paride Legovini
  248             peb	Pierre-Elliott Bécue
  249             pgt	Pierre Gruet
  250           philh	Philip Hands
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  251            phls	Paulo Henrique de Lima Santana
  252            pini	Gilles Filippini
  253           piotr	Piotr Ożarowski
  254             pjb	Phil Brooke
  255           pkern	Philipp Kern
  256          plessy	Charles Plessy
  257       pmatthaei	Patrick Matthäi
  258          pmhahn	Philipp Matthias Hahn
  259           pollo	Louis-Philippe Véronneau
  260        porridge	Marcin Owsiany
  261         praveen	Praveen Arimbrathodiyil
  262        pvaneynd	Peter Van Eynde
  263    rattusrattus	Andy Simpkins
  264             rcw	Robert Woodcock
  265             reg	Gregory Colpart
  266         reichel	Joachim Reichel
  267            rene	Rene Engelhard
  268      rfrancoise	Romain Francoise
  269           rinni	Philip Rinn
  270         rlaager	Richard Laager
  271             rlb	Rob Browning
  272        rmayorga	Rene Mayorga
  273         roberto	Roberto C. Sanchez
  274        roehling	Timo Röhling
  275          roland	Roland Rosenfeld
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  276             ron	Ron Lee
  277        rousseau	Ludovic Rousseau
  278           rover	Roberto Lumbreras
  279             rra	Russ Allbery
  280             rrs	Ritesh Raj Sarraf
  281     rvandegrift	Ross Vandegrift
  282        sakirnth	Sakirnth Nagarasa
  283           salve	Davide G. M. Salvetti
  284       samueloph	Samuel Henrique
  285        santiago	Santiago Ruano Rincón
  286           satta	Sascha Steinbiss
  287        sblondon	Stéphane Blondon
  288             seb	Sebastien Delafond
  289       sebastien	Sébastien Villemot
  290        sergiodj	Sergio Durigan Junior
  291         serpent	Tomasz Rybak
  292           sesse	Steinar H. Gunderson
  293              sf	Stefan Fritsch
  294        siretart	Reinhard Tartler
  295             sjr	Simon Richter
  296           skitt	Stephen Kitt
  297            smcv	Simon McVittie
  298             smr	Steven Michael Robbins
  299           smurf	Matthias Urlichs
  300         sophieb	Sophie Brun
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  301       spwhitton	Sean Whitton
  302       sramacher	Sebastian Ramacher
  303             sre	Sebastian Reichel
  304          ssgelm	Stephen Gelman
  305             ssm	Stig Sandbeck Mathisen
  306      stapelberg	Michael Stapelberg
  307        stappers	Geert Stappers
  308          steele	David Steele
  309        stefanor	Stefano Rivera
  310            sten	Nicholas D Steeves
  311  stephanlachnit	Stephan Lachnit
  312         stevenc	Steven Chamberlain
  313       sthibault	Samuel Thibault
  314             sto	Sergio Talens-Oliag
  315          stuart	Stuart Prescott
  316            sune	Sune Vuorela
  317           sur5r	Jakob Haufe
  318       sylvestre	Sylvestre Ledru
  319            tach	Taku Yasui
  320          taffit	David Prévot
  321          takaki	Takaki Taniguchi
  322             tao	David Weinehall
  323             tbm	Martin Michlmayr
  324        terceiro	Antonio Terceiro
  325              tg	Thorsten Glaser
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  326           thijs	Thijs Kinkhorst
  327           tiago	Tiago Bortoletto Vaz
  328          tianon	Tianon Gravi
  329          tijuca	Carsten Schoenert
  330            timo	Timo Jyrinki
  331        tjhukkan	Teemu Hukkanen
  332        tmancill	Tony Mancill
  333            tobi	Tobias Frost
  334           toddy	Tobias Quathamer
  335         treinen	Ralf Treinen
  336           troyh	Troy Heber
  337        tvainika	Tommi Vainikainen
  338        tvincent	Thomas Vincent
  339         tzafrir	Tzafrir Cohen
  340            ucko	Aaron M. Ucko
  341          uhoreg	Hubert Chathi
  342       ultrotter	Guido Trotter
  343        umlaeute	IOhannes m zmölnig
  344           urbec	Judit Foglszinger
  345         vagrant	Vagrant Cascadian
  346         vasudev	Vasudev Sathish Kamath
  347          vcheng	Vincent Cheng
  348         venthur	Bastian Venthur
  349           viiru	Arto Jantunen
  350           vseva	Victor Seva
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  351          vvidic	Valentin Vidic
  352          wagner	Hanno Wagner
  353           waldi	Bastian Blank
  354          weasel	Peter Palfrader
  355        weinholt	Göran Weinholt
  356          wijnen	Bas Wijnen
  357          wookey	Wookey
  358          wouter	Wouter Verhelst
  359            wrar	Andrey Rahmatullin
  360             xam	Max Vozeler
  361            yadd	Xavier Guimard
  362            zack	Stefano Zacchiroli
  363            zeha	Christian Hofstaedtler
  364            zhsj	Shengjing Zhu
  365            zigo	Thomas Goirand
  366       zugschlus	Marc Haber
