<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Raphael Arrouas and Jean Lejeune discovered an access control bypass
vulnerability in mod_jk, the Apache connector for the Tomcat Java
servlet engine. The vulnerability is addressed by upgrading mod_jk to
the new upstream version 1.2.46, which includes additional changes.</p>

<ul>
<li><a href="https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.42_and_1.2.43">https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.42_and_1.2.43</a></li>
<li><a href="https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.43_and_1.2.44">https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.43_and_1.2.44</a></li>
<li><a href="https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.44_and_1.2.45">https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.44_and_1.2.45</a></li>
<li><a href="https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.45_and_1.2.46">https://tomcat.apache.org/connectors-doc/miscellaneous/changelog.html#Changes_between_1.2.45_and_1.2.46</a></li>
</ul>

<p>For the stable distribution (stretch), this problem has been fixed in
version 1:1.2.46-0+deb9u1.</p>

<p>We recommend that you upgrade your libapache-mod-jk packages.</p>

<p>For the detailed security status of libapache-mod-jk please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libapache-mod-jk">\
https://security-tracker.debian.org/tracker/libapache-mod-jk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4357.data"
# $Id: $
