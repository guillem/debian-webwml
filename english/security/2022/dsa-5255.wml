<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>An integer overflow flaw was discovered in the CRL parser in libksba, an
X.509 and CMS support library, which could result in denial of service
or the execution of arbitrary code.</p>

<p>Details can be found in the upstream advisory at
<a href="https://gnupg.org/blog/20221017-pepe-left-the-ksba.html">https://gnupg.org/blog/20221017-pepe-left-the-ksba.html</a></p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 1.5.0-3+deb11u1.</p>

<p>We recommend that you upgrade your libksba packages.</p>

<p>For the detailed security status of libksba please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/libksba">https://security-tracker.debian.org/tracker/libksba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5255.data"
# $Id: $
