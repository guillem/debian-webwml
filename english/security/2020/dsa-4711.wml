<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in coturn, a TURN and STUN
server for VoIP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4067">CVE-2020-4067</a>

    <p>Felix Doerre reported that the STUN response buffer was not properly
    initialised, which could allow an attacker to leak bytes in the
    padding bytes from the connection of another client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6061">CVE-2020-6061</a>

    <p>Aleksandar Nikolic reported that a crafted HTTP POST request can
    lead to information leaks and other misbehavior.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6062">CVE-2020-6062</a>

    <p>Aleksandar Nikolic reported that a crafted HTTP POST request can
    lead to server crash and denial of service.</p></li>

</ul>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 4.5.0.5-1+deb9u2.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 4.5.1.1-1.1+deb10u1.</p>

<p>We recommend that you upgrade your coturn packages.</p>

<p>For the detailed security status of coturn please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/coturn">https://security-tracker.debian.org/tracker/coturn</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4711.data"
# $Id: $
