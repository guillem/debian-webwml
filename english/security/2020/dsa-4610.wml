<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the webkit2gtk
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8835">CVE-2019-8835</a>

    <p>An anonymous researcher discovered that maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8844">CVE-2019-8844</a>

    <p>William Bowling discovered that maliciously crafted web content
    may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8846">CVE-2019-8846</a>

    <p>Marcin Towalski of Cisco Talos discovered that maliciously crafted
    web content may lead to arbitrary code execution.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.26.3-1~deb10u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4610.data"
# $Id: $
