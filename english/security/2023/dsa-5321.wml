<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Matthieu Barjole and Victor Cutillas discovered that sudoedit in sudo, a
program designed to provide limited super user privileges to specific
users, does not properly handle '--' to separate the editor and
arguments from files to edit. A local user permitted to edit certain
files can take advantage of this flaw to edit a file not permitted by
the security policy, resulting in privilege escalation.</p>

<p>Details can be found in the upstream advisory at
<a href="https://www.sudo.ws/security/advisories/sudoedit_any/">https://www.sudo.ws/security/advisories/sudoedit_any/</a> .</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 1.9.5p2-3+deb11u1.</p>

<p>We recommend that you upgrade your sudo packages.</p>

<p>For the detailed security status of sudo please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/sudo">https://security-tracker.debian.org/tracker/sudo</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5321.data"
# $Id: $
