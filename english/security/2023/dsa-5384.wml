<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in OpenImageIO, a
library for reading and writing images. Buffer overflows and out-of-bounds
read and write programming errors may lead to a denial of service
(application crash) or the execution of arbitrary code if a malformed image
file is processed.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.2.10.1+dfsg-1+deb11u1.</p>

<p>We recommend that you upgrade your openimageio packages.</p>

<p>For the detailed security status of openimageio please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openimageio">\
https://security-tracker.debian.org/tracker/openimageio</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5384.data"
# $Id: $
