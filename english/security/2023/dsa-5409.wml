<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two security issues have been discovered in libssh, a tiny C SSH library:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1667">CVE-2023-1667</a>

    <p>Philip Turnbull discovered a NULL pointer dereference which could
    result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2283">CVE-2023-2283</a>

    <p>Kevin Backhouse discovered that pki_verify_data_signature() may
    fail to correctly validate authentication in memory pressure
    situations.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 0.9.7-0+deb11u1.</p>

<p>We recommend that you upgrade your libssh packages.</p>

<p>For the detailed security status of libssh please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libssh">\
https://security-tracker.debian.org/tracker/libssh</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5409.data"
# $Id: $
