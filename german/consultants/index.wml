#use wml::debian::template title="Debian-Berater" MAINPAGE="true" NOCOMMENTS="yes" GENTIME="yes"
#use wml::debian::translation-check translation="1776cd107720f975fba5147dfb40a13806ae3626"
#  Translator: Thimo Neubauer <thimo@debian.org>
# Updated: Holger Wansing <linux@wansing-online.de>, 2013, 2019, 2022.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian listet die Berater nur
         als Gefälligkeit an die Benutzer auf und übernimmt keine Garantien für die
         aufgelisteten Personen oder Firmen. Die Einträge stehen unter der
         alleinigen Verantwortung des jeweiligen Beraters. Bitte beachten Sie die
         <a href="info">Seite mit Informationen für Berater</a> bezüglich unserer
         Richtlinien zum Hinzufügen oder Aktualisieren von Einträgen.
</p>
</aside>

<p> Debian ist freie Software und bietet Hilfe über
    <a href="$(HOME)/MailingLists/">Mailinglisten</a> an. Einige
    Menschen haben jedoch entweder keine Zeit dafür oder spezielle
    Wünsche, so dass sie bereit sind, jemanden dafür zu
    bezahlen, einen Debian-Rechner zu warten oder neue
    Funktionalität hinzuzufügen. Es folgt eine Liste von Leuten, die
    zumindest einen Teil ihres Einkommens durch <strong>bezahlte</strong>
    Unterstützung von Debian verdienen.
</p>

<p> Die Namen sind nach Ländern sortiert, aber innerhalb eines Landes folgt die
    Sortierung lediglich der zeitlichen Abfolge der Anmeldung. Wenn Ihr Land
    nicht aufgeführt wird, schauen Sie auch in den
    Nachbarländern, weil manche Berater auch international tätig
    sind bzw. häufig eh über eine Remove-Verbindung arbeiten.
</p> 

<p>Es gibt weitere Listen von Beratern für spezielle Anwendungsfelder von Debian:</p>

<ul>
<li><a href="https://wiki.debian.org/DebianEdu/Help/ProfessionalHelp">DebianEdu</a>:
für die Verwendung von Debian in Schulen, Universitäten und anderen Bildungseinrichtungen;
<li><a href="https://wiki.debian.org/LTS/Funding">Debian LTS</a>:
für die Langzeit-Sicherheits-Unterstützung von Debian.
</ul>

<hrline>

<h2>Länder, in denen es aufgelistete Debian-Berater gibt:</h2>

#include "../../english/consultants/consultant.data"

<hrline>

# left for backwards compatibility - in case someone still links to #policy
# which is fairly unlikely, but hey
<h2><a name="policy"></a>Richtlinien für Debians Berater-Seite</h2>
<p>Bitte lesen Sie die <a href="info">Seite mit Informationen für Berater</a>
   bezüglich unserer Richtlinien zum Hinzufügen oder Aktualisieren von
   Einträgen.
</p>
