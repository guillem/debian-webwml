#use wml::debian::translation-check translation="1f51232f1964140bbf71ecfd1fc7bdcbd252e744" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 12 aktualisiert: 12.1 veröffentlicht</define-tag>
<define-tag release_date>2023-07-22</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>Bookworm</define-tag>
<define-tag revision>12.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die erste Aktualisierung seiner Stable-Distribution
Debian <release> (Codename <q><codename></q>) ankündigen zu können. Diese
Zwischenveröffentlichung behebt hauptsächlich Sicherheitslücken der Stable-Veröffentlichung
sowie einige ernste Probleme. Es sind bereits separate Sicherheitsankündigungen
veröffentlicht worden, auf die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version von
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete auffrischt.
Es gibt keinen Grund, <q><codename></q>-Medien zu entsorgen, da deren Pakete
auch nach der Installation durch einen aktualisierten Debian-Spiegelserver auf
den neuesten Stand gebracht werden können.
</p>

<p>Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht viele
Pakete auf den neuesten Stand bringen müssen. Die meisten dieser Aktualisierungen sind
in dieser Revision enthalten.</p>

<p>Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.</p>

<p>Vorhandene Installationen können auf diese Revision angehoben werden, indem
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian verwiesen
wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerbehebungen</h2>

<p>Diese Stable-Aktualisierung fügt den folgenden Paketen einige wichtige Korrekturen hinzu:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction aide "Systembenutzer ordentlich anlegen; Unterverzeichnis-Verarbeitung bei Übereinstimmung korrigiert">
<correction autofs "Hänger bei Verwendung von Kerberos-authentifiziertem LDAP behoben">
<correction ayatana-indicator-datetime "Wiedergabe eigener Alarmtöne überarbeitet">
<correction base-files "Aktualisierung auf die Zwischenveröffentlichung 12.1">
<correction bepasty "Darstellung von Text-Uploads überarbeitet">
<correction boost1.81 "Fehlende Abhängigkeit von libboost-json1.81.0 für libboost-json1.81-dev nachgetragen">
<correction bup "POSIX-ACLs richtig wiederherstellen">
<correction context "Socket in ConTeXt-mtxrun aktivieren">
<correction cpdb-libs "Anfälligkeit für Pufferüberlauf behoben [CVE-2023-34095]">
<correction cpp-httplib "Problem mit CRLF-Injektion (Zeilenumbruch-Injektion) behoben [CVE-2023-26130]">
<correction crowdsec "Standard-acquis.yaml überarbeitet, damit sie auch journalctl als Datenquelle enthält, diese aber begrenzt auf die ssh.service-Unit, sodass sichergestellt wird, dass die Übernahme auch ohne die traditionelle auth.log-Datei funktioniert; sichergestellt, dass die Engine nicht wegen einer ungültigen Datenquelle mit Fehler aussteigt">
<correction cups "Sicherheitskorrekturen: Weiterverwendung nach Freigabe (use-after-free) [CVE-2023-34241]; Heap-Puffer-Überlauf [CVE-2023-32324]">
<correction cvs "Vollen Pfad zu ssh in Konfiguration hinterlegen">
<correction dbus "Neue Veröffentlichung der Originalautoren; Dienstblockade-Problem gelöst [CVE-2023-34969]; nicht länger versuchen, DPKG_ROOT zu berücksichtigen, sodass eher systemd's /etc/machine-id kopiert wird, statt eine komplett neue Maschinen-ID zu erzeugen">
<correction debian-installer "Linux-Kernel-ABI auf 6.1.0-10 angehoben; Neukompilierung gegen proposed-updates">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction desktop-base "emerald-Alternativen bei Paket-Deinstallation ebenfalls entfernen">
<correction dh-python "Beschädigt/Ersetzt-Abhängigkeit von python2 wiederhergestellt, um APT in einigen Upgrade-Szenarios zu helfen">
<correction dkms "Beschädigt-Abhängigkeit von obsoleten, inkompatiblen *-dkms-Paketen hinzugefügt">
<correction dnf "Standard-DNF-Konstante PYTHON_INSTALL_DIR korrigiert">
<correction dpdk "Neue Veröffentlichung der Originalautoren">
<correction exim4 "Argumentauswertung für ${run }-Expansion überarbeitet; behoben, dass ${srs_encode ..} alle 1024 Tage falsche Resultate zurückliefert">
<correction fai "Gültigkeit der IP-Adresse angepasst">
<correction glibc "Pufferüberlauf in gmon behoben; Deadlock in getaddrinfo (__check_pf) mit verzögertem Abbruch korrigiert; Jahr-2038-Unterstützung in strftime auf 32-Bit-Architekturen überarbeitet; Spezialfall in der Verarbeitung von /etc/gshadow korrigiert, die zu fehlerhaften Zeigern führen kann, was wiederum Speicherzugriffsfehler in Anwendungen verursachen kann; Deadlock in system() behoben, wenn es gleichzeitig von mehreren Threads aufgerufen wird; cdefs: Definition von Fortifizierungs-Makros auf __FORTIFY_LEVEL &gt; 0 begrenzt, um alte C90-Compiler zu unterstützen">
<correction gnome-control-center "Neue fehlerbereinigte Version der Originalautoren">
<correction gnome-maps "Neue fehlerbereinigte Version der Originalautoren">
<correction gnome-shell "Neue fehlerbereinigte Version der Originalautoren">
<correction gnome-software "Neue Veröffentlichung der Originalautoren; Speicherlecks geflickt">
<correction gosa "PHP 8.2-Missbilligungswarnungen abgestellt; fehlende Vorlage im Vorgabe-Thema nachgereicht; Tabellengestaltung überarbeitet; Verwendung des debugLevel &gt; 0 korrigiert">
<correction groonga "Links zur Dokumentation korrigiert">
<correction guestfs-tools "Sicherheitskorrektur [CVE-2022-2211]">
<correction indent "ROUND_UP-Makro wiederhergestellt und anfängliche Puffergröße korrigiert">
<correction installation-guide "Indonesische Übersetzung aktiviert">
<correction kanboard "Bösartige Injektion von HTML-Tags in das DOM [CVE-2023-32685] behoben; parameterbasierte indirekte Objektreferenzierung, die zur Offenlegung privater Dateien führen kann, behoben [CVE-2023-33956]; fehlende Zugriffskontrollen nachgereicht [CVE-2023-33968, CVE-2023-33970]; Stored-XSS in Funktionalität Task External Link behoben [CVE-2023-33969]">
<correction kf5-messagelib "Auch nach Unterschlüsseln suchen">
<correction libmatekbd "Speicherlecks behoben">
<correction libnginx-mod-http-modsecurity "Binäre Neukompilierung mit pcre2">
<correction libreoffice "Neue fehlerbereinigte Version der Originalautoren">
<correction libreswan "Potenzielles Dienstblockade-Problem behoben [CVE-2023-30570]">
<correction libxml2 "Problem mit Nullzeiger-Dereferenzierung behoben [CVE-2022-2309]">
<correction linux "Neue Veröffentlichung der Originalautoren; netfilter: nf_tables: Genmask nicht ignorieren, wenn Kette anhand der ID nachgeschlagen wird [CVE-2023-31248], OOB-Zugriff in nft_byteorder_eval verhindern [CVE-2023-35001]">
<correction linux-signed-amd64 "Neue Veröffentlichung der Originalautoren; netfilter: nf_tables: Genmask nicht ignorieren, wenn Kette anhand der ID nachgeschlagen wird [CVE-2023-31248], OOB-Zugriff in nft_byteorder_eval verhindern [CVE-2023-35001]">
<correction linux-signed-arm64 "Neue Veröffentlichung der Originalautoren; netfilter: nf_tables: Genmask nicht ignorieren, wenn Kette anhand der ID nachgeschlagen wird [CVE-2023-31248], OOB-Zugriff in nft_byteorder_eval verhindern [CVE-2023-35001]">
<correction linux-signed-i386 "Neue Veröffentlichung der Originalautoren; netfilter: nf_tables: Genmask nicht ignorieren, wenn Kette anhand der ID nachgeschlagen wird [CVE-2023-31248], OOB-Zugriff in nft_byteorder_eval verhindern [CVE-2023-35001]">
<correction mailman3 "Redundanten Cronjob gelöscht; Einreihung von Diensten übernehmen, wenn MariaDB vorhanden ist">
<correction marco "Wenn Eigentum vom Superuser, richtigen Fenstertitel anzeigen">
<correction mate-control-center "Mehrere Speicherlecks behoben">
<correction mate-power-manager "Mehrere Speicherlecks behoben">
<correction mate-session-manager "Mehrere Speicherlecks behoben; auch andere Clutter-Backends als X11 erlauben">
<correction multipath-tools "Darunterliegende Pfade vor dem LVM verbergen; Fehlschlag bei der ersten Dienstausführung bei neuen Installationen verhindern">
<correction mutter "Neue fehlerbereinigte Version der Originalautoren">
<correction network-manager-strongswan "Editor-Komponente mit GTK-4-Unterstützung kompiliert">
<correction nfdump "Beim Starten Erfolg zurückmelden; Speicherzugriffsfehler bei Optionenauswertung behoben">
<correction nftables "Regression beim Setzen des Listenformats behoben">
<correction node-openpgp-seek-bzip "Installation der Dateien im seek-bzip-Paket überarbeitet">
<correction node-tough-cookie "Prototype Pollution behoben [CVE-2023-26136]">
<correction node-undici "Sicherheitskorrekturen: <q>Host</q>-HTTP-Kopfzeile vor CLRF-Injektion [CVE-2023-23936] schützen; potenzielle Dienstblockade durch reguläre Ausdrücke auf Headers.set und Headers.append unterbunden [CVE-2023-24807]">
<correction node-webpack "Sicherheitskorrektur (cross-realm objects) [CVE-2023-28154]">
<correction nvidia-cuda-toolkit "Mitgeliefertes openjdk-8-jre aktualisiert">
<correction nvidia-graphics-drivers "Neue Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-graphics-drivers-tesla "Neue Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-graphics-drivers-tesla-470 "Neue Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-modprobe "Neue fehlerbereinigte Version der Originalautoren">
<correction nvidia-open-gpu-kernel-modules "Neue Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-support "Beschädigt-Abhängigkeit von inkompatiblen Paketen in Bullseye hinzugefügt">
<correction onionshare "Installation von Desktop-Möbeln überarbeitet">
<correction openvpn "Speicherleck und ins Leere deutenden Zeiger (möglicher Absturz-Vektor) behoben">
<correction pacemaker "Regression im Ressourcen-Scheduler behoben">
<correction postfix "Neue fehlerbereinigte Version der Originalautoren; <q>postfix set-permissions</q> behoben">
<correction proftpd-dfsg "Bei der Installation keinen Socket im inetd-Stil aktivieren">
<correction qemu "Neue Veröffentlichung der Originalautoren; Nicht-Verfügbarkeit von USB-Geräten für den XEN HVM domUs behoben; 9pfs: Öffnen von Spezialdateien behoben [CVE-2023-2861]; Probleme mit Eintrittsvarianz im LSI-Controller behoben [CVE-2023-0330]">
<correction request-tracker5 "Links zur Dokumentation korrigiert">
<correction rime-cantonese "Wörter und Zeichen anhand Häufigkeit sortieren">
<correction rime-luna-pinyin "Fehlende Pinyin-Schemadaten nachgereicht">
<correction samba "Neue Veröffentlichung der Originalautoren; sicherstellen, dass Handbuchseiten während der Kompilierung erzeugt werden; Unterstützung fürs Speichern von Kerberos-Tickets im Kernel-Schlüsselbund aktiviert; Kompilierungsprobleme auf armel und mipsel behoben; Windows-Anmelde- und Vertrauensprobleme seit den Windows-Updates 2023-07 behoben">
<correction schleuder-cli "Sicherheitskorrektur (Wertmaskierung)">
<correction smarty4 "Eigenmächtige Codeausführung behoben [CVE-2023-28447]">
<correction spip "Verschiedene Sicherheitskorrekturen; Sicherheitskorrektur (Filterung von Authentifizierungsdaten)">
<correction sra-sdk "Dateiinstallation in libngs-java überarbeitet">
<correction sudo "Format des Ereignisprotokolls überarbeitet">
<correction systemd "Neue fehlerbereinigte Version der Originalautoren">
<correction tang "Race Condition beim Erstellen/Rotieren von Schlüsseln behoben [CVE-2023-1672]">
<correction texlive-bin "Socket in luatex standardmäßig deaktivieren [CVE-2023-32668]; auf i386 installierbar gemacht">
<correction unixodbc "Beschädigt+Ersetzt-Abhängigkeit für odbcinst1debian1 hinzugefügt">
<correction usb.ids "Enthaltene Daten aktualisiert">
<correction vm "Byte-Kompilierung abgeschaltet">
<correction vte2.91 "Neue fehlerbereinigte Version der Originalautoren">
<correction xerial-sqlite-jdbc "Eine UUID als Verbindungs-ID verwenden [CVE-2023-32697]">
<correction yajl "Speicherleck-Sicherheitskorrektur; Dienstblockade unterbunden [CVE-2017-16516], außerdem Ganzzahlüberlauf abgestellt [CVE-2022-24795]">
</table>


<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für
jede davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2023 5423 thunderbird>
<dsa 2023 5425 php8.2>
<dsa 2023 5427 webkit2gtk>
<dsa 2023 5428 chromium>
<dsa 2023 5429 wireshark>
<dsa 2023 5430 openjdk-17>
<dsa 2023 5432 xmltooling>
<dsa 2023 5433 libx11>
<dsa 2023 5434 minidlna>
<dsa 2023 5435 trafficserver>
<dsa 2023 5436 hsqldb1.8.0>
<dsa 2023 5437 hsqldb>
<dsa 2023 5439 bind9>
<dsa 2023 5440 chromium>
<dsa 2023 5443 gst-plugins-base1.0>
<dsa 2023 5444 gst-plugins-bad1.0>
<dsa 2023 5445 gst-plugins-good1.0>
<dsa 2023 5446 ghostscript>
<dsa 2023 5447 mediawiki>
<dsa 2023 5448 linux-signed-amd64>
<dsa 2023 5448 linux-signed-arm64>
<dsa 2023 5448 linux-signed-i386>
<dsa 2023 5448 linux>
<dsa 2023 5449 webkit2gtk>
<dsa 2023 5450 firefox-esr>
<dsa 2023 5451 thunderbird>
</table>


<h2>Debian-Installer</h2>

<p>Der Installer wurde aktualisiert, damit er die Korrekturen enthält,
die mit dieser Zwischenveröffentlichung in Stable eingeflossen sind.</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert haben:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>


<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern
Freier Software, die ihre Zeit und Mühen einbringen, um das
vollständig freie Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Website unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken Sie eine Mail
(auf Englisch) an &lt;press@debian.org&gt; oder kontaktieren Sie das
Stable-Veröffentlichungs-Team (auf Englisch)
unter &lt;debian-release@lists.debian.org&gt;.</p>



