#use wml::debian::cdimage title="Verificando a autenticidade das imagens do Debian" BARETITLE=true
#use wml::debian::translation-check translation="e96ee42901d20a8878ded2c462204cd1b7a7210f"

<p>
As versões oficiais de instalação do Debian e imagens live vêm com arquivos de
checksum assinados; procure-os junto com as imagens nos diretórios
<code>iso-cd</code>, <code>jigdo-dvd</code>, <code>iso-hybrid</code>, etc. Esses
arquivos permitem que você verifique se as imagens baixadas estão corretas. Em
primeiro lugar, o checksum pode ser usado para verificar se as imagens não
foram corrompidas durante o download. Em segundo lugar, as assinaturas nos
arquivos de checksum permitem que você confirme que as imagens são as criadas e
lançadas pelo Debian, e não foram adulteradas.
</p>

<p>
Para validar o conteúdo de um arquivo de imagem, certifique-se de usar a
ferramenta de verificação de checksum apropriada. Algoritmos de verificação de
checksum criptograficamente fortes (SHA256 e SHA512) estão disponíveis para
todas as versões; você deve usar as ferramentas de correspondência
<code>sha256sum</code> ou <code>sha512sum</code> para trabalhar com elas.
</p>

<p>
Para garantir que os próprios arquivos de checksum estejam corretos, use o
GnuPG para verificá-los em relação aos arquivos de assinatura que os acompanham
(por exemplo, <code>SHA512SUMS.sign</code>). As chaves usadas para essas
assinaturas estão todas no
<a href="https://keyring.debian.org">chaveiro Debian GPG</a> e a melhor maneira
de verificá-las é usar esse chaveiro para validar a confiança via web. Para
facilitar a vida das pessoas que não têm acesso imediato a uma máquina Debian,
aqui estão os detalhes das chaves que foram usadas para assinar as
versões nos últimos anos e links para baixar as chaves públicas diretamente:
</p>

#include "$(ENGLISHDIR)/CD/CD-keys.data"
