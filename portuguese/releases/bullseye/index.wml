#use wml::debian::template title="Informações de lançamento do Debian &ldquo;bullseye&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="6bafc9758f46d9e4fe5b48e5d85484d9f3dc8e3e"

<p>O Debian <current_release_bullseye> foi lançado em
<a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>.
<ifneq "11.0" "<current_release>"
  "Debian 11.0 foi lançado inicialmente em <:=spokendate('2021-08-14'):>."
/>
O lançamento incluiu várias grandes mudanças descritas em
nosso <a href="$(HOME)/News/2021/20210814">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

<p><strong>O Debian 11 foi substituído pelo
<a href="../bookworm/">Debian 12 (<q>bookworm</q>)</a>.
#Atualizações de segurança foram descontinuadas em <:=spokendate('2023-06-10'):>.
</strong></p>

#<p><strong>No entanto, o bullseye se beneficia do suporte de longo prazo (LTS -
#Long Term Support) até o final de agosto de 2026. O LTS é limitado ao i386,
#amd64, armel, armhf e arm64.
#Todas as outras arquiteturas não são mais suportadas no bullseye.
#Para obter mais informações, consulte a
#<a href="https://wiki.debian.org/LTS">seção LTS da wiki do Debian</a>.
#</strong></p>

<p>Para obter e instalar o Debian, consulte
a página de <a href="debian-installer/">informação de instalação</a> e o
<a href="installmanual">Manual de Instalação</a>. Para atualizar a partir de uma
versão mais antiga do Debian, consulte as instruções nas
<a href="releasenotes">notas de lançamento</a>.</p>

###  Ative o seguinte, quando o período LTS começar.
#<p>Arquiteturas suportadas durante o suporte de longo prazo:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Arquiteturas de computadores suportadas no lançamento inicial do bullseye:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Apesar de nossos desejos, podem existir alguns problemas nesta versão,
embora ela tenha sido declarada <em>estável (stable)</em>. Nós fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode <a href="../reportingbugs">relatar outros problemas</a>
para nós.</p>

<p>Por último mas não menos importante, nós temos uma lista de
<a href="credits">pessoas que recebem crédito</a> por fazerem este lançamento
acontecer.</p>
