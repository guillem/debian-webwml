#use wml::debian::template title="Säkerhetsinformation" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="e49cfbc5f5d8df40e0852a5da747d32544d67b06"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Att hålla ditt Debiansystem säkert</a></li>
<li><a href="#DSAS">Senaste bulletinerna</a></li>
</ul>

<p>Debian ser väldigt allvarligt på säkerhet.
Vi hanterar alla säkerhetsproblem vi görs uppmärksamma på och ser till
att de korrigeras inom en rimlig tidsram.
Många problem samordnas med andra distributörer av fri programvara och
publiceras samma dag som sårbarheten görs allmänt känd, och vi har även en
<a href="audit/">säkerhetsgranskningsgrupp</a> som går genom arkivet och
söker efter nya eller ej rättade säkerhetsfel.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian ser väldigt allvarligt på säkerhet. 
Vi hanterar alla säkerhetsproblem vi görs uppmärksamma på och ser till att de
korrigeras inom en rimlig tidsram.</p>
</aside>
 
<p>
Erfarenhet visar att <q>säkerhet genom otydlighet</q> aldrig fungerar. Där tillåter publikt offentliggörande snabbare och bättre lösningar av säkerhetsproblem. 
I det avseendet tar den här sidan upp Debians status angående olika kända säkerhetshål, som potentiellt kan påverka Debianoperativsystemet.
</p>

<p>
Debianprojektet koordinerar många säkerhetsbulletiner med andra
mjukvaruutvecklare, och som ett resultat av detta publiceras dessa bulletiner
samma dag som sårbarheten är publik.
</p>
 
<p>
Debian deltar även i projekt för standardisering av säkerhetsinformation:
</p>

<ul>
<li>
   <a href="#DSAS">Debians säkerhetsbulletiner</a> är <a href="cve-compatibility">CVE-kompatibla</a> (se <a href="crossreferences">korsreferenser</a>).</li>
<li>Debian finns representerat i styrelsen för projektet <a href="https://oval.cisecurity.org/">Open Vulnerability Assessment Language</a>.</li>
</ul>


<h2><a id="keeping-secure">Håll ditt Debiansystem säkert</a></h2>

<p>För att få de senaste säkerhetsbulletinerna från Debian, prenumerera på
sändlistan <a href="https://lists.debian.org/debian-security-announce/">
debian-security-announce</a>.</p>

<p>Utöver detta kan du använda
<a href="https://packages.debian.org/stable/admin/apt">apt</a>
för att enkelt hämta de senaste säkerhetsuppdateringarna.
För att hålla ditt Debiansystem uppdaterat med säkerhetspatchar, vänligen lägg till följande rad till
din <code>/etc/apt/sources.list</code>:
</p>



<pre>
deb http://security.debian.org/debian-security <current_release_security_name> main contrib non-free non-free-firmware
</pre>

<p>
Efter att du har sparat ändringarna, kör följande två kommandon föratt hämta
och installera de väntande uppdateringarna:
</p>

<pre>
apt-get update &amp;&amp; apt-get upgrade
</pre>

<p>
Säkerhetsarkivet är signerat med Debians normala 
<a href="https://ftp-master.debian.org/keys.html">arkivsigneringsnyckel</a>.
</p>

<p>
För ytterligare information om säkerhetsfrågor i Debian, vänligen
se vår FAQ och vår dokumentation.
</p>

<p style="text-align:center"><button type="button"><span 
class="fas fa-book-open fa-2x"></span> <a href="faq">Säkerhets-FAQ</a></button> 
<button type="button"><span class="fas fa-book-open fa-2x"></span> <a 
href="../doc/user-manuals#securing">Säkra Debian</a></button></p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Senaste säkerhetsbulletinerna</a></h2>

<p>Dessa webbsidor innehåller ett förkortat arkiv över de
säkerhetsbulletiner som postas på sändlistan
<a href="https://lists.debian.org/debian-security-announce/">
debian-security-announce</a>.<br>
<a href="dsa.html#DSAS">Nytt listformat</a>


<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debians säkerhetsbulletiner (endast rubriker)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debians säkerhetsbulletiner (sammanfattning)" href="dsa-long">
:#rss#}
<p>De senaste säkerhetsbulletinerna från Debian finns även tillgängliga som
<a href="dsa">RDF-filer</a>.
Vi har även en något
<a href="dsa-long">längre version</a>
som innehåller första stycket från den aktuella bulletinen. Detta gör att du
enkelt kan se vad det handlar om.
</p>

#include "$(ENGLISHDIR)/security/index.include"
<p>Äldre säkerhetsbulletiner finns även de tillgängliga:
<:= get_past_sec_list(); :>

<p>
Debians distributioner är inte sårbara för alla säkerhetsproblem.
<a href="https://security-tracker.debian.org/">Debians säkerhetsspårare</a>
samlar information om status för sårbarheter i Debianpaket, och du kan söka
i den på CVE-namn eller per paket.
</p>
