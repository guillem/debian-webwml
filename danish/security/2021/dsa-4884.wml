#use wml::debian::translation-check translation="8841a09ee24923f86538151f0ba51430b8414c1a" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Adskillige sårbarheder blev opdaget i ldb, en LDAP-lignede indlejret database 
opbygget på toppen af TDB.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10730">CVE-2020-10730</a>

    <p>Andrew Bartlett opdagede en NULL-pointerdereference og anvendelse efter 
    frigivelse-fejl ved håndtering af LDAP-kontrollerne <q>ASQ</q> og 
    <q>VLV</q>, samt kombinationer med LDAP-funktionaliteten 
    paged_results.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27840">CVE-2020-27840</a>

    <p>Douglas Bagnall opdagede en heapkorruptionsfejl gennem fabrikerede 
    DN-strenge.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20277">CVE-2021-20277</a>

    <p>Douglas Bagnall opdagede en sårbarhed i forbindelse med læsning udenfor 
    grænserne i håndteringen af LDAP-attributter, som i begyndelsen indeholder 
    adskillige mellemrum efter hinanden.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 2:1.5.1+really1.4.6-3+deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine ldb-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende ldb, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/ldb">\
https://security-tracker.debian.org/tracker/ldb</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4884.data"
