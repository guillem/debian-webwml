#use wml::debian::translation-check translation="cb61e48c0b590145d526b25534c156232550c9c0" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3702">CVE-2020-3702</a>

    <p>En fejl blev fundet i driveren til Atheros IEEE 802.11n-familien af 
    chipset (ath9k), hvilket muliggjorde informationsafsløring.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16119">CVE-2020-16119</a>

    <p>Hadar Manor rapporterede om anvendelse efter frigivelse i 
    implementeringen af DCCP-protokollen i Linux-kernen.  En lokal angriber 
    kunne drage nytte af fejlen til at forårsage et lammelsesangreb eller 
    potentielt udføre vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3653">CVE-2021-3653</a>

    <p>Maxim Levitsky opdagede en sårbarhed i implementeringen af 
    KVM-hypervisor'en til AMD-processorerer i Linux-kernen:  Manglende 
    validering af WMCB-feltet <q>int_ctl</q> kunne gøre det muligt for en 
    ondsindet L1-gæst at aktivere AVIC-understøttelse (Advanced Virtual 
    Interrupt Controller) for L2-gæster.  L2-gæsten kunne drage nytte af 
    fejlen til at skrive til et begrænset, men dog relativ stor delmængde 
    af værtens fysiske hukommelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3656">CVE-2021-3656</a>

    <p>Maxim Levitsky og Paolo Bonzini opdagede en fejl i implementeringen af 
    KVM-hypervisor'en til AMD-processorer i Linux-kernen.  Manglende validering 
    af WMCB-feltet <q>virt_ext</q> kunne gøre det muligt for en ondsindet 
    L1-gæst at deaktivere begge VMLOAD-/VMSAVE-opfangelser og VLS (Virtual 
    VMLOAD/VMSAVE) for L2-gæsten.  Under disse omstændigheder, var L2-gæsten i 
    stand til at køre VMLOAD/VMSAVE uden opfangelser, og dermed læse/skrive dele 
    af værtens fysiske hukommelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3679">CVE-2021-3679</a>

    <p>En fejl i Linux-kernens tracingmodulfunktionalitet kunne gøre det muligt 
    for en priviligeret lokal bruger (med CAP_SYS_ADMIN-muligheden), at 
    forårsage et lammelsesangreb (ressourceudsultning).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3732">CVE-2021-3732</a>

    <p>Alois Wohlschlager rapporterede om en fejl i implementeringen af 
    undersystemet overlayfs, hvilket gjorde det muligt for en lokal angriber, 
    med rettigheder til at mount'e et filsystem, at blotlægge filer skjult i den 
    oprindelige mount.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3739">CVE-2021-3739</a>

    <p>En NULL-pointerdereferencefejl blev fundet i filsystemet btrfs, hvilket 
    gjorde det muligt for en lokal angreber, med CAP_SYS_ADMIN-muligheden, at 
    forårsage et lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3743">CVE-2021-3743</a>

    <p>En hukommelseslæsning udenfor grænserne blev opdaget i implementeringen 
    af routeprotokollen Qualcomm IPC, hvilket gjorde det muligt at forårsage 
    lammelsesangreb eller informationslækage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3753">CVE-2021-3753</a>

    <p>Minh Yuan rapporterede om en kapløbstilstand i vt_k_ioctl i 
    drivers/tty/vt/vt_ioctl.c, hvilke kunne forårsage en læsning udenfor 
    grænserne i vt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37576">CVE-2021-37576</a>

    <p>Alexey Kardashevskiy rapporterede om et bufferoverløb i KVM-undersystemet 
    på powerpc-platformen, hvilke gjorde det muligt for 
    KVM-gæstestyresystemsbrugere at forårsage hukommelseskorruption på 
    værten.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38160">CVE-2021-38160</a>

    <p>En fejl i virtio_console blev opdaget, hvilket muliggjorde datakorruption 
    eller datatab gennem en enhed, der ikke er tillid til.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38166">CVE-2021-38166</a>

    <p>En heltalsoverløbsfejl i BPF-undersystemet kunne gøre det muligt for en 
    lokal angriber at forårsage et lammelsesangreb eller potentielt udførelse af 
    vilkårlig kode.  Fejlen er som standard afhjulpet i Debian, da 
    upriviligerede kald til bpf() er deaktiveret.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38199">CVE-2021-38199</a>

    <p>Michael Wakabayashi rapporterede om en fejl i implementeringen af 
    NFSv4-klienten, hvor ukorrekt forbindelsesopsætningsrækkefølge, muliggjorde 
    at handlinger fra en fjern NFSv4-server kunne forårsage 
    lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40490">CVE-2021-40490</a>

    <p>En kapløbstilstand blev opdaget i ext4-undersystemet ved skrivning til en 
    inline_data-fil mens dens xattrs ændres.  Det kunne medføre 
    lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41073">CVE-2021-41073</a>

    <p>Valentina Palmiotti opdagede en fejl i io_uring, hvilket gjorde det 
    muligt for en lokal angriber at forøge rettigheder.</p></li>

</ul>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 5.10.46-5.  Denne opdatering indeholder rettelser vedrørende #993948 og 
#993978.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4978.data"
