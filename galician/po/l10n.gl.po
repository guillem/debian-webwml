# Jorge Barreiro <yortx.barry@gmail.com>, 2012.
# Parodper <parodper@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-05-13 23:59+0200\n"
"Last-Translator: Parodper <parodper@gmail.com>\n"
"Language-Team: Galician <debian-l10n-galician@lists.debian.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.0.1\n"

#: ../../english/international/l10n/dtc.def:10
msgid "File"
msgstr "Ficheiro"

#: ../../english/international/l10n/dtc.def:14
msgid "Package"
msgstr "Paquete"

#: ../../english/international/l10n/dtc.def:18
msgid "Score"
msgstr "Puntuación"

#: ../../english/international/l10n/dtc.def:22
msgid "Translator"
msgstr "Tradutor"

#: ../../english/international/l10n/dtc.def:26
msgid "Team"
msgstr "Equipo"

#: ../../english/international/l10n/dtc.def:30
msgid "Date"
msgstr "Data"

#: ../../english/international/l10n/dtc.def:34
msgid "Status"
msgstr "Estado"

#: ../../english/international/l10n/dtc.def:38
msgid "Strings"
msgstr "Cadeas"

#: ../../english/international/l10n/dtc.def:42
msgid "Bug"
msgstr "Informe de fallo"

#: ../../english/international/l10n/dtc.def:49
msgid "<get-var lang />, as spoken in <get-var country />"
msgstr "<get-var lang />, tal como se fala en <get-var country />"

#: ../../english/international/l10n/dtc.def:54
msgid "Unknown language"
msgstr "Idioma descoñecido"

#: ../../english/international/l10n/dtc.def:64
msgid "This page was generated with data collected on: <get-var date />."
msgstr "Esta páxina foi xerada con datos recollidos en:  <get-var date />."

#: ../../english/international/l10n/dtc.def:69
msgid "Before working on these files, make sure they are up to date!"
msgstr ""
"Antes de traballar nestes ficheiros asegúrese de que estean actualizados!"

#: ../../english/international/l10n/dtc.def:79
msgid "Section: <get-var name />"
msgstr "Sección: <get-var name />"

#: ../../english/international/l10n/menu.def:10
msgid "L10n"
msgstr "L10n"

#: ../../english/international/l10n/menu.def:14
msgid "Language list"
msgstr "Lista de idiomas"

#: ../../english/international/l10n/menu.def:18
msgid "Ranking"
msgstr "Clasificación"

#: ../../english/international/l10n/menu.def:22
msgid "Hints"
msgstr "Pistas"

#: ../../english/international/l10n/menu.def:26
msgid "Errors"
msgstr "Erros"

#: ../../english/international/l10n/menu.def:30
msgid "POT files"
msgstr "Ficheiros POT"

#: ../../english/international/l10n/menu.def:34
msgid "Hints for translators"
msgstr "Consellos para tradutores"
