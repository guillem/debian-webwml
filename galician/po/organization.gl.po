# Jorge Barreiro <yortx.barry@gmail.com>, 2012, 2014.
# Parodper <parodper@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-05-14 01:23+0200\n"
"Last-Translator: Parodper <parodper@gmail.com>\n"
"Language-Team: Galician <debian-l10n-galician@lists.debian.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.0.1\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "correo de delegacións"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "correo de nomeamentos"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegado"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegada"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"he_him\"/>o"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"she_her\"/>a"

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"gender_neutral\"/>o/a"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"they_them\"/>os/as"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "actual"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "membro"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "xestor"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Xestor da versión estable"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "mago"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "presidente"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "asistente"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "secretario"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "representantes"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "rol"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"Nesta lista <q>actual</q> indica aqueles cargos\n"
"que son temporais (elixidos ou nomeados ate certa data)."

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Dirixentes"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:104
msgid "Distribution"
msgstr "Distribución"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:195
msgid "Communication and Outreach"
msgstr "Comunicación e Divulgación"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:198
msgid "Data Protection team"
msgstr "Equipo de Protección de Datos"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:203
msgid "Publicity team"
msgstr "Equipo de Publicidade"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:270
msgid "Membership in other organizations"
msgstr "Afiliación noutras organizacións"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:293
msgid "Support and Infrastructure"
msgstr "Asistencia e infraestrutura"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Líder"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Comité técnico"

#: ../../english/intro/organization.data:99
msgid "Secretary"
msgstr "Secretaría"

#: ../../english/intro/organization.data:107
msgid "Development Projects"
msgstr "Proxectos de desenvolvemento"

#: ../../english/intro/organization.data:108
msgid "FTP Archives"
msgstr "Arquivos FTP"

#: ../../english/intro/organization.data:110
msgid "FTP Masters"
msgstr "Xestores do FTP"

#: ../../english/intro/organization.data:116
msgid "FTP Assistants"
msgstr "Asistentes do FTP"

#: ../../english/intro/organization.data:122
msgid "FTP Wizards"
msgstr "Magos do FTP"

#: ../../english/intro/organization.data:125
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:127
msgid "Backports Team"
msgstr "Equipo de backports"

#: ../../english/intro/organization.data:131
msgid "Release Management"
msgstr "Xestión das publicacións"

#: ../../english/intro/organization.data:133
msgid "Release Team"
msgstr "Equipo de publicación"

#: ../../english/intro/organization.data:143
msgid "Quality Assurance"
msgstr "Garantía de calidade"

#: ../../english/intro/organization.data:144
msgid "Installation System Team"
msgstr "Equipo do sistema de instalación"

#: ../../english/intro/organization.data:145
msgid "Debian Live Team"
msgstr "Equipo de Debian Live"

#: ../../english/intro/organization.data:146
msgid "CD/DVD/USB Images"
msgstr "Imaxes de CD/DVD/USB"

#: ../../english/intro/organization.data:148
msgid "Production"
msgstr "Produción"

#: ../../english/intro/organization.data:156
msgid "Testing"
msgstr "Probas"

#: ../../english/intro/organization.data:158
msgid "Cloud Team"
msgstr "Equipo da Nube"

#: ../../english/intro/organization.data:160
msgid "Autobuilding infrastructure"
msgstr "Infraestrutura de construción automática"

#: ../../english/intro/organization.data:162
msgid "Wanna-build team"
msgstr "Equipo de peticións de construción"

#: ../../english/intro/organization.data:169
msgid "Buildd administration"
msgstr "Administración de buildd"

#: ../../english/intro/organization.data:186
msgid "Documentation"
msgstr "Documentación"

#: ../../english/intro/organization.data:190
msgid "Work-Needing and Prospective Packages list"
msgstr "Lista de paquetes que precisan traballo e por crear"

#: ../../english/intro/organization.data:206
msgid "Press Contact"
msgstr "Contacto coa prensa"

#: ../../english/intro/organization.data:208
msgid "Web Pages"
msgstr "Páxinas web"

#: ../../english/intro/organization.data:216
msgid "Planet Debian"
msgstr "Planeta Debian"

#: ../../english/intro/organization.data:221
msgid "Outreach"
msgstr "Difusión"

#: ../../english/intro/organization.data:226
msgid "Debian Women Project"
msgstr "Proxecto muller Debian"

#: ../../english/intro/organization.data:234
msgid "Community"
msgstr "Comunidade"

#: ../../english/intro/organization.data:241
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""
"Para enviarlle unha mensaxe privada a todos os membros do Equipo da "
"Comunidade use a chave privada <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."

#: ../../english/intro/organization.data:243
msgid "Events"
msgstr "Eventos"

#: ../../english/intro/organization.data:250
msgid "DebConf Committee"
msgstr "Comité da DebConf"

#: ../../english/intro/organization.data:257
msgid "Partner Program"
msgstr "Programa de socios"

#: ../../english/intro/organization.data:261
msgid "Hardware Donations Coordination"
msgstr "Coordinador das doazóns de hardware"

#: ../../english/intro/organization.data:276
msgid "GNOME Foundation"
msgstr "Fundación GNOME"

#: ../../english/intro/organization.data:278
msgid "Linux Professional Institute"
msgstr "Linux Professional Institute"

#: ../../english/intro/organization.data:279
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:281
msgid "Linux Standards Base"
msgstr "Base Normalizada de Linux (LSB)"

#: ../../english/intro/organization.data:282
msgid "Free Standards Group"
msgstr "Free Standards Group"

#: ../../english/intro/organization.data:283
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS: Organización\n"
"      para o Avance de Normativas de Información Estructurada"

#: ../../english/intro/organization.data:286
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""
"OVAL: Linguaxe de Avaliación\n"
"      de Vulnerabilidades Abertas"

#: ../../english/intro/organization.data:289
msgid "Open Source Initiative"
msgstr "Open Source Initiative"

#: ../../english/intro/organization.data:296
msgid "Bug Tracking System"
msgstr "Sistema de seguimento dos informes de fallos"

#: ../../english/intro/organization.data:301
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Administración das listas de correo o dos seus arquivos"

#: ../../english/intro/organization.data:309
msgid "New Members Front Desk"
msgstr "Recepción de novos membros"

#: ../../english/intro/organization.data:318
msgid "Debian Account Managers"
msgstr "Xestores de contas de Debian (DAM)"

#: ../../english/intro/organization.data:324
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Para enviarlle un correo a todos os DAM use a chave GPG "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:325
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Mantedores dos chaveiros (PGP e GPG)"

#: ../../english/intro/organization.data:329
msgid "Security Team"
msgstr "Equipo de seguridade"

#: ../../english/intro/organization.data:341
msgid "Policy"
msgstr "Políticas"

#: ../../english/intro/organization.data:344
msgid "System Administration"
msgstr "Administración de sistemas"

#: ../../english/intro/organization.data:345
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Este é o enderezo a usar cando se atopen problemas en algunha das máquinas "
"de Debian, incluídos problemas cos contrasinais ou se precisa que se instale "
"un paquete."

#: ../../english/intro/organization.data:355
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Se ten problemas co hardware de máquinas Debian, consulte a páxina <a href="
"\"https://db.debian.org/machines.cgi\">máquinas Debian</a>. Debería conter "
"información dos administradores de cada máquina."

#: ../../english/intro/organization.data:356
msgid "LDAP Developer Directory Administrator"
msgstr "Administrador do directorio LDAP de desenvolvedores"

#: ../../english/intro/organization.data:357
msgid "Mirrors"
msgstr "Réplicas"

#: ../../english/intro/organization.data:360
msgid "DNS Maintainer"
msgstr "Mantedor do DNS"

#: ../../english/intro/organization.data:361
msgid "Package Tracking System"
msgstr "Sistema de seguimento de paquetes"

#: ../../english/intro/organization.data:363
msgid "Treasurer"
msgstr "Tesoureiro"

#: ../../english/intro/organization.data:368
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Peticións de uso da <a name=\"trademark\" href=\"m4_HOME/trademark\">marca "
"rexistrada</a>"

#: ../../english/intro/organization.data:371
msgid "Salsa administrators"
msgstr "Administradores de Salsa"

#~ msgid "Alioth administrators"
#~ msgstr "Administradores de alioth"

#~ msgid "Anti-harassment"
#~ msgstr "Anti-acoso"

#~ msgid "Auditor"
#~ msgstr "Auditor"

#~ msgid "Bits from Debian"
#~ msgstr "Novas breves de Debian"

#~ msgid "CD Vendors Page"
#~ msgstr "Páxina de vendedores de CDs"

#~ msgid "Consultants Page"
#~ msgstr "Páxina de asesores técnicos"

#~ msgid "DebConf chairs"
#~ msgstr "Presidencias DebConf"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Mantedores do chaveiro de mantedores de Debian (DM)"

#~ msgid "Debian Pure Blends"
#~ msgstr "Debian Pure Blends"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Debian para a educación"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian para nenos de 1 a 99"

#~ msgid "Debian for education"
#~ msgstr "Debian para a educación"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian para práctica e investigación médica"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian para persoas con discapacidades"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian para investigación científica e relacionada"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian en oficinas legais"

#~ msgid "Embedded systems"
#~ msgstr "Sistemas embebidos"

#~ msgid "Firewalls"
#~ msgstr "Devasas"

#~ msgid "Individual Packages"
#~ msgstr "Paquetes individuais"

#~ msgid "Laptops"
#~ msgstr "Portátiles"

#~ msgid "Live System Team"
#~ msgstr "Equipo do sistema Live"

#~ msgid "Ports"
#~ msgstr "Adaptacións"

#~ msgid "Publicity"
#~ msgstr "Publicidade"

#~ msgid "Release Notes"
#~ msgstr "Notas da publicación"

#~ msgid "Security Audit Project"
#~ msgstr "Proxecto de auditoría de seguridade"

#~ msgid "Special Configurations"
#~ msgstr "Configuracións especiais"

#~ msgid "Testing Security Team"
#~ msgstr "Equipo de seguridade en testing"

#~ msgid "User support"
#~ msgstr "Asistencia ao usuario"

#~ msgid "current Debian Project Leader"
#~ msgstr "Actual líder do proxecto Debian"
