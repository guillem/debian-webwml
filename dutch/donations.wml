#use wml::debian::template title="Doneren aan het Debian-project" MAINPAGE="true"
#use wml::debian::translation-check translation="77730b1058b58e76cb29910f6acf353ec5bf9847"

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#spi">Software in the Public Interest</a></li>
  <li><a href="#debianfrance">Debian Frankrijk</a></li>
  <li><a href="#debianch">debian.ch</a></li>
  <li><a href="#debian">Debian</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Dank aan alle donateurs die Debian steunen met apparatuur of diensten: <a href="https://db.debian.org/machines.cgi">sponsors van hostingdiensten en hardware</a>, <a href="mirror/sponsors">sponsors van spiegelservers</a>, <a href="partners/">partners voor ontwikkeling en dienstverlening</a>.</p>
</aside>

<p>
Donaties worden beheerd door de <a href="$(HOME)/devel/leader">Projectleider van Debian</a> (Debian Project Leader - DPL) en stellen Debian in staat om <a href="https://db.debian.org/machines.cgi">machines</a>, <a href="https://wiki.debian.org/Teams/DSA/non-DSA-HW">andere hardware</a>, domeinen en cryptografische certificaten te verwerven, en om <a href="https://www.debconf.org">de conferenties van Debian</a> te laten plaatsvinden, net als <a href="https://wiki.debian.org/MiniDebConf">Debian mini-conferenties</a> en <a href="https://wiki.debian.org/Sprints">ontwikkelingssprints</a>, en om aanwezig te zijn op andere evenementen, enz.
</p>

<p id="default">
Verschillende <a href="https://wiki.debian.org/Teams/Treasurer/Organizations">organisaties</a> hebben activa in beheer voor Debian en ontvangen donaties namens Debian. Deze pagina vermeldt verschillende methoden om te doneren aan het Debian-project. De eenvoudigste methode om aan Debian te doneren is via PayPal aan <a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest</a> (Software in het algemeen belang), een non-profitorganisatie die activa in bewaring heeft voor Debian.
</p>

<p style="text-align:center"><button type="button"><span class="fab fa-paypal fa-2x"></span> <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">Doneren via PayPal</a></button></p>

<aside class="light">
  <span class="fas fa-gifts fa-5x"></span>
</aside>

<table>
<tr>
<th>Organisatie</th>
<th>Methodes</th>
<th>Opmerkingen</th>
</tr>
<tr>
<td><a href="#spi"><acronym title="Software in the Public Interest">SPI</acronym></a></td>
<td>
 <a href="#spi-paypal">PayPal</a>,
 <a href="#spi-click-n-pledge">Click &amp; Pledge</a>,
 <a href="#spi-cheque">cheque</a>),
 <a href="#spi-other">andere</a>
</td>
<td>USA, belastingvrij non-profit</td>
</tr>
<tr>
<td><a href="#debianfrance">Debian France</a></td>
<td>
 <a href="#debianfrance-bank">overschrijving</a>,
 <a href="#debianfrance-paypal">PayPal</a>
</td>
<td>Frankrijk, belastingvrij non-profit</td>
</tr>
<tr>
<td><a href="#debianch">debian.ch</a></td>
<td>
 <a href="#debianch-bank">overschrijving</a>,
 <a href="#debianch-other">andere</a>
</td>
<td>Zwitserland, non-profit</td>
</tr>
<tr>
<td><a href="#debian">Debian</a></td>
<td>
 <a href="#debian-equipment">uitrusting</a>,
 <a href="#debian-time">tijd</a>,
 <a href="#debian-other">andere</a>
</td>
<td></td>
</tr>

# Template:
#<tr>
#<td><a href="#"><acronym title=""></acronym></a></td>
#<td>
# <a href="#"></a>,
# <a href="#"></a> (allows recurring donations),
# <a href="#cheque"></a> (CUR)
#</td>
#<td>, tax-exempt non-profit</td>
#</tr>

</table>

<h2 id="spi">Software in the Public Interest</h2>

<p>
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest, Inc.</a>
is een non-profitorganisatie in de Verenigde Staten die belastingvrije
giften kan ontvangen. Ze werd door mensen van Debian opgericht in 1997
om organisaties voor vrije software/apparatuur te ondersteunen.
</p>

<h3 id="spi-paypal">PayPal</h3>

<p>
Eenmalige en periodieke giften kunnen gedaan worden via <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">de pagina van SPI</a> op de website van PayPal. Om een periodieke gift te doen moet u het vakje aanvinken bij <em>Make this a monthly donation</em>.
</p>

<h3 id="spi-click-n-pledge">Click &amp; Pledge</h3>

<p>
Eenmalige en periodieke giften kunnen gedaan worden via <a href="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">de pagina van SPI</a> op de website van Click &amp; Pledge. Om een periodieke gift te doen, kiest u aan de rechterzijde hoe vaak u wilt doneren (<em>Repeat payment</em>); scrol daarna naar beneden naar <em>Debian Project Donation</em>; vul het bedrag in dat u wenst te schenken; klik op het item <em>Add to cart</em> en werk de rest
van de procedure af.
</p>

<h3 id="spi-cheque">Cheque</h3>

<p>
Giften kunnen ook gedaan worden met een cheque of een postwissel in <abbr title="United States dollar">USD</abbr> en <abbr title="Canadian dollar">CAD</abbr>. Gelieve Debian te vermelden in het mededelingenveld en de cheque of postwissel te sturen aan SPI op het adres dat vermeld staat op de webpagina <a href="https://www.spi-inc.org/donations/">Donaties aan SPI</a>.
</p>

<h3 id="spi-other">Andere</h3>

<p>
Donaties kunnen ook gebeuren via overschrijving of andere methodes. Voor sommige delen van de wereld kan het gemakkelijker zijn om een gift te doen aan een van de partnerorganisaties van Software in the Public Interest. Raadpleeg voor verdere details de webpagina <a href="https://www.spi-inc.org/donations/">Donaties aan SPI</a>.
</p>

<h2 id="debianfrance">Debian France</h2>

<p>
De organisatie <a href="https://france.debian.net/">Debian France Association</a>  is een erkende Franse organisatie die valt onder de <q>wet van 1901</q>, en opgericht werd om het Debian Project te steunen en te promoten in Frankrijk.
</p>

<h3 id="debianfrance-bank">Overschrijving</h3>

<p>
Giften kunnen gebeuren via overschrijving op het rekeningnummer dat vermeld wordt op de webpagina <a href="https://france.debian.net/soutenir/#compte">Giften aan Debian France</a>. Een ontvangstbewijs voor deze giften kan aangevraagd worden met een e-mailbericht aan <a href="mailto:donation@france.debian.net">donation@france.debian.net</a>.
</p>

<h3 id="debianfrance-paypal">PayPal</h3>

<p>
Men kan ook een gift doen via de webpagina <a href="https://france.debian.net/galette/plugins/paypal/form">Debian France PayPal</a>. De bestemmeling van die gift kan Debian France in het bijzonder zijn of het Debian Project in het algemeen.
</p>

<h2 id="debianch">debian.ch</h2>

<p>
<a href="https://debian.ch/">debian.ch</a> werd opgericht om het Debian-project te vertegenwoordigen in Zwitserland en in het Vorstendom Liechtenstein.
</p>

<h3 id="debianch-bank">Overschrijving</h3>

<p>
Giften kunnen vanuit Zwitserse en internationale banken gebeuren via overschrijving op de rekeningnummers die vermeld worden op de <a href="https://debian.ch/">website van debian.ch</a>.
</p>

<h3 id="debianch-other">Andere</h3>

<p>
Giften via andere methodes zijn mogelijk door contact op te nemen met het donatieadres dat vermeld wordt op de <a href="https://debian.ch/">website</a>.
</p>

# Template:
#<h3 id=""></h3>
#
#<p>
#</p>
#
#<h4 id=""></h4>
#
#<p>
#</p>

<h2 id="debian">Debian</h2>

<p>
Debian kan op dit ogenblik donaties van <a href="#debian-equipment">uitrusting</a> aanvaarden, maar geen <a href="#debian-other">andere</a> donaties.
</p>

<h3 id="debian-equipment">Uitrusting en diensten</h3>

<p>
Debian vertrouwt ook op de donatie van apparatuur en diensten van individuen, bedrijven, universiteiten, enz. om Debian verbonden te houden met de wereld.
</p>

<p>
Als uw bedrijf inactieve machines of reserveapparatuur (harde schijven, SCSI-controllers, netwerkkaarten, enz.) heeft rondslingeren, overweeg dan om deze aan Debian te schenken. Neem voor meer informatie contact op met onze <a href="mailto:hardware-donations@debian.org">gemachtigde voor hardwaredonaties</a>.
</p>

<p>
Debian houdt een <a href="https://wiki.debian.org/Hardware/Wanted">lijst met gewenste hardware</a> bij voor verschillende diensten en groepen binnen het project.
</p>

<h3 id="debian-time">Tijd</h3>

<p>
Er bestaan veel manieren om <a href="$(HOME)/intro/help">Debian te helpen</a> tijdens uw vrije tijd of werktijd.
</p>

<h3 id="debian-other">Andere</h3>

<p>
Debian kan op dit moment geen cryptomunten aanvaarden, maar we onderzoeken of het voor ons mogelijk is deze donatiemethode te ondersteunen.
# Brian Gupta requested we discuss this before including it:
#If you have cryptocurrency to donate, or insights to share, please
#get in touch with <a href="mailto:madduck@debian.org">Martin f. krafft</a>.
</p>
