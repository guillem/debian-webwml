#use wml::debian::translation-check translation="2e6574c8189c823994786d48167daa0d6f74d2e6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans la servlet
Tomcat et le moteur JSP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42252">CVE-2022-42252</a>

<p>Apache Tomcat a été configuré pour ignorer les en-têtes HTTP non valables
au moyen du réglage de rejectIllegalHeader à faux. Tomcat ne rejetait
pas une requête contenant un en-tête Content-Length non valable rendant
possible une attaque de dissimulation de requête si Tomcat était situé
derrière un mandataire inverse qui échouait aussi à rejeter la requête avec
l'en-tête non valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-45143">CVE-2022-45143</a>

<p>JsonErrorReportValve dans Apache Tomcat ne protégeait pas les valeurs
type, message ou description. Dans certaines circonstances, elles sont
construites à partir de données fournies par l'utilisateur et il était donc
possible à des utilisateurs de fournir des valeurs qui invalidaient ou
manipulaient la sortie JSON.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28708">CVE-2023-28708</a>

<p>Lors de l'utilisation de RemoteIpFilter avec des requêtes reçues d'un
mandataire inverse par HTTP qui incluent un en-tête X-Forwarded-Proto
défini à https, les cookies de session créés par Apache Tomcat n'incluaient
pas l'attribut <q>secure</q>. Cela pouvait avoir pour conséquence la
transmission par l'agent utilisateur d'un cookie de session sur un canal non
sécurisé.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 9.0.43-2~deb11u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tomcat9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tomcat9">\
https://security-tracker.debian.org/tracker/tomcat9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5381.data"
# $Id: $
