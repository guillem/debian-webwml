#use wml::debian::translation-check translation="931b551f95b186c421d64727b9d144c9b9328e0a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Tavis Ormandy a découvert que, sous des circonstances particulière liées
à la microarchitecture, un registre de vecteur dans les processeurs
<q>Zen 2</q> pouvait ne pas être mis à zéro correctement. Ce défaut permet
à un attaquant de divulguer le contenu du registre à travers des processus
concurrents, des hyper flux et des clients virtualisés.</p>

<p>Pour plus de détails veuillez consulter
<a href="https://lock.cmpxchg8b.com/zenbleed.html">\
https://lock.cmpxchg8b.com/zenbleed.html</a> et
<a href="https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8">\
https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8</a>.</p>

<p>La version initiale du microcode d'AMD ne fournit des mises à jour que
pour les processeurs EPYC de deuxième génération : divers processeurs Ryzen
sont aussi affectés, mais les mises à jour ne sont pas encore disponibles.
Des correctifs seront fournis dans une mise à jour ultérieure dès qu'ils
seront publiés.</p>

<p>Pour plus de détails particuliers et les dates prévues, veuillez
consulter la page :
<a href="https://www.amd.com/en/resources/product-security/bulletin/amd-sb-7008.html">\
https://www.amd.com/en/resources/product-security/bulletin/amd-sb-7008.html</a></p>

<p>Pour la distribution oldstable (Bullseye), ce problème a été corrigé
dans la version 3.20230719.1~deb11u1. En complément, la mise à jour fournit
un correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9836">CVE-2019-9836</a>.</p>

<p>Pour la distribution stable (Bookworm), ce problème a été corrigé dans
la version 3.20230719.1~deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets amd64-microcode.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de amd64-microcode,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/amd64-microcode">\
https://security-tracker.debian.org/tracker/amd64-microcode</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5459.data"
# $Id: $
