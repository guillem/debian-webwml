#use wml::debian::translation-check translation="3c7770e4886a1ac93ee9eedccf9096d9c654c2f9" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans BIND, une
implémentation de serveur DNS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-3341">CVE-2023-3341</a>

<p>Un défaut d'épuisement de pile a été découvert dans le code du canal de
contrôle qui peut avoir pour conséquence un déni de service (plantage du
démon named).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4236">CVE-2023-4236</a>

<p>Robert Story a découvert qu'un défaut dans le code réseau traitant les
requêtes DNS-over-TLS pouvait provoquer une interruption de named due à un
échec d'assertion, avec pour conséquence un déni de service dans des
conditions de forte charge de requêtes DNS-over-TLS.</p></li>

</ul>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 1:9.16.44-1~deb11u1. La distribution oldstable
(Bullseye) n'est affectée que par le
<a href="https://security-tracker.debian.org/tracker/CVE-2023-3341">CVE-2023-3341</a>.</p>

<p>Pour la distribution stable (Bookworm), ces problèmes ont été corrigés
dans la version 1:9.18.19-1~deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bind9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5504.data"
# $Id: $
