#use wml::debian::translation-check translation="0949cbef906dd651b75e87a224abb4db6096c1ae" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Matthieu Barjole et Victor Cutillas ont découvert que sudoedit dans
sudo, un programme conçu pour donner des droits limités de superutilisateur
à des utilisateurs particuliers, ne gérait pas correctement les <q>--</q>
pour séparer l'éditeur et les paramètres provenant des fichiers à modifier.
Un utilisateur local autorisé à éditer certains fichiers peut tirer
avantage de ce défaut pour modifier un ficher non autorisé par la politique
de sécurité, avec pour conséquence une élévation de privilèges.</p>

<p>Plus de détails sont disponibles dans l'annonce amont à l'adresse
<a href="https://www.sudo.ws/security/advisories/sudoedit_any/">\
https://www.sudo.ws/security/advisories/sudoedit_any/</a> .</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 1.9.5p2-3+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sudo.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sudo, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sudo">\
https://security-tracker.debian.org/tracker/sudo</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5321.data"
# $Id: $
