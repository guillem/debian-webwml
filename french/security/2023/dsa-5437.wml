#use wml::debian::translation-check translation="b10a43649b0de5541771790b60f20ad5527d78d4" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Gregor Kopf de Secfault Security GmbH a découvert que HSQLDB, un moteur
de base de données SQL Java, permettait l'exécution de commandes de script
frauduleuses dans les fichiers .script et .log. HSQLDB prend en charge un
mot-clé <q>SCRIPT</q> qui est normalement utilisé pour enregistrer
l'entrée des commandes par l'administration de la base de données pour
produire ce type de script. En combinaison avec LibreOffice, un attaquant
pouvait contrefaire un fichier odb contenant un fichier <q>database/script</q>
qui lui-même contenait une commande SCRIPT où le contenu du fichier pouvait
être écrit dans un nouveau fichier dont l'emplacement était déterminé par
l'attaquant.</p>

<p>Pour la distribution oldstable (Bullseye), ce problème a été corrigé
dans la version 2.5.1-1+deb11u2.</p>

<p>Pour la distribution stable (Bookworm), ce problème a été corrigé dans
la version 2.7.1-1+deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets hsqldb.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de hsqldb, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/hsqldb">\
https://security-tracker.debian.org/tracker/hsqldb</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5437.data"
# $Id: $
