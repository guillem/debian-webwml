#use wml::debian::translation-check translation="006fd719ddd7701428df2e4ab411c878c33ce4ba" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour fournit un microcode de processeur mis à jour pour
certains types de processeur Intel et fournit des palliatifs pour des
vulnérabilités de sécurité.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21123">CVE-2022-21123</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-21125">CVE-2022-21125</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-21127">CVE-2022-21127</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-21166">CVE-2022-21166</a>

<p>Divers chercheurs ont découvert des défauts dans les processeurs Intel,
communément appelées vulnérabilités « MMIO Stale Data », qui peuvent avoir
pour conséquence une fuite d'informations vers des utilisateurs locaux.</p>

<p>Pour plus de détails, veuillez consulter la page
<a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/processor-mmio-stale-data-vulnérabilités.html">\
https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/processor-mmio-stale-data-vulnérabilities.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21151">CVE-2022-21151</a>

<p>Alysa Milburn, Jason Brandt, Avishai Redelman et Nir Lavi ont découvert
que pour certains processeurs Intel la suppression de l'optimisation ou la
modification de code critique du point de vue de la sécurité peuvent avoir
pour conséquence la divulgation d'informations à des utilisateurs locaux.</p></li>

</ul>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 3.20220510.1~deb10u1.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 3.20220510.1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets intel-microcode.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de intel-microcode,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5178.data"
# $Id: $
