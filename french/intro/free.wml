#use wml::debian::template title="Que signifie libre ?" NOHEADER="yes"
#use wml::debian::translation-check translation="2de158b0e259c30eefd06baf983e4930de3d7412" maintainer="Jean-Pierre Giraud"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freesoftware">Libre comme dans… ?</a></li>
    <li><a href="#licenses">Les licences des logiciels</a></li>
    <li><a href="#choose">Comment choisir une licence ?</a></li>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> En février 1998, un groupe a décidé de remplacer le terme « logiciel libre » (<i><a href="https://www.gnu.org/philosophy/free-sw">Free Software</a></i>) par « logiciel ouvert » (<i><a href="https://opensource.org/docs/definition.html">Open Source Software</a></i>). Ce débat sur la terminologie reflète les différences philosophiques sous-jacentes, mais les exigences pratiques comme les autres sujets discutés sur le site sont essentiellement similaires pour le logiciel libre et le logiciel ouvert.</p>
</aside>

<h2><a id="freesoftware">Libre comme dans… ?</a></h2>

<p>
Beaucoup de néophytes des logiciels libres sont désorientés à cause du mot
« free ». Il n'est pas utilisé dans le sens qu'ils attendent – pour eux,
« free » signifie gratuit. Si vous cherchez dans un dictionnaire anglais, il
propose presque une vingtaine de sens au mot « free » et un seul signifie
« gratuit ». Les autres font référence à la « liberté » et à « l'absence de
contraintes ». Aussi, lorsque l'on parle de <em>Free Software</em> ou de
<em>logiciel libre</em>, nous parlons de liberté et non de prix.
</p>

<p>
<em>N.d.T. :</em> En français, la traduction limite la confusion, mais elle
existe dans les documents en anglais.
</p>

<p>
Un logiciel décrit comme libre, mais uniquement dans le sens où il ne faut pas
payer pour l'avoir, est rarement complètement libre. Il se peut qu'il soit
interdit de le donner à quelqu'un et vous ne pouvez vraisemblablement pas
l'améliorer. Les logiciels distribués gratuitement sont en général une arme dans
une campagne marketing, pour promouvoir un produit apparenté, ou pour mener un
concurrent plus petit à la faillite. Il n'y a aucune garantie que ce logiciel
reste gratuit.
</p>

<p>
Pour les non-initiés, un logiciel est libre ou il ne l'est pas. La réalité
est bien plus complexe. Pour comprendre ce que les gens entendent lorsqu'ils
qualifient un logiciel de libre, nous devons faire un petit détour vers le monde
des licences des logiciels.
</p>

<h2><a id="licenses">Les licences des logiciels</a></h2>

<p>
Le copyright est une méthode pour protéger les droits du créateur de certains
types d'œuvre. Dans la plupart des pays, les logiciels que vous écrivez
acquièrent automatiquement un copyright. Une licence est un moyen pour l'auteur
d'autoriser l'utilisation d'une création (le logiciel dans ce cas) par d'autres
personnes, dans des conditions acceptables pour eux. C'est à l'auteur d'ajouter
une licence qui indique de quelle façon le logiciel peut être utilisé.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.copyright.gov/">Plus d'information sur le copyright</a></button></p>

<p>

Bien sûr des circonstances différentes entraînent des licences différentes. Les
sociétés de logiciels cherchent à protéger leurs intérêts, aussi elles ne
publient souvent que le code compilé qui n'est pas humainement lisible. Elles
imposent aussi beaucoup de restrictions sur l'utilisation du logiciel. Les 
auteurs de logiciels libres, quant à eux, cherchent des règles différentes,
parfois même une combinaison des propriétés suivantes :
</p>

<ul>
  <li>Il n'est pas autorisé d'utiliser leur code dans un logiciel propriétaire. Puisqu'ils publient le code pour que tout le monde puisse l'utiliser, ils ne veulent pas voir quelqu'un d'autre le voler. Dans ce cas, l'utilisation du code est vue comme un accord basé sur la confiance : vous pouvez l'utiliser à condition de jouer suivant les mêmes règles.</li>
  <li>La paternité du code doit être protégée. Les auteurs sont très fiers de leur œuvre et ne veulent pas que quelqu'un vienne supprimer leur nom de celle-ci ou même prétende l'avoir écrit.</li>
  <li>Le code source doit être distribué. Un des problèmes principaux avec les logiciels propriétaires réside dans l'impossibilité de corriger les bogues ou de personnaliser le logiciel puisque les sources ne sont pas disponibles. Par ailleurs, le fabricant peut décider d'arrêter le support de votre matériel. La distribution du code source, que la plupart des licences libres imposent, protège les utilisateurs en les autorisant à personnaliser les logiciels et à les adapter à leurs besoins.</li>
  <li>Toute œuvre qui inclut une partie du travail de l'auteur (appelée aussi <em>œuvre dérivée</em> dans les discussions sur les copyrights) doit utiliser la même licence.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Les trois des licences les plus répandues dans le monde sont la <a href="https://www.gnu.org/copyleft/gpl.html">GNU General Public License (GPL)</a> (Licence publique générale GNU), la <a href="https://opensource.org/licenses/artistic-license.php">licence artistique</a> et la <a href="https://opensource.org/licenses/BSD-3-Clause">licence de type BSD</a>.
</aside>

<h2><a id="choose">Comment choisir une licence ?</a></h2>

<p>
Parfois les gens écrivent leur propre licence, ce qui peut être problématique,
aussi cela est déconseillé dans la communauté du logiciel libre. Trop souvent
les termes sont ambigus ou les conditions sont contradictoires. Écrire une
licence juridiquement valable est encore plus difficile. Heureusement, il existe
un certain nombre de licences Open Source parmi lesquelles vous pouvez choisir.
Elles ont en commun les éléments suivants :
</p>

<ul>
  <li>Les utilisateurs peuvent utiliser le logiciel sur autant de machines qu'ils le désirent.</li>
  <li>Un nombre quelconque de personnes peuvent utiliser le logiciel en même temps.</li>
  <li>Les utilisateurs peuvent faire autant de copies du logiciel qu'ils le veulent ou en ont besoin et les donner à d'autres utilisateurs (redistribution libre).</li>
  <li>Il n'y a aucune restriction sur les modifications des logiciels (sauf de garder certaines mentions de crédit).</li>
  <li>Les utilisateurs peuvent non seulement distribuer le logiciel, mais aussi le vendre.</li>
</ul>

<p>
Ce dernier point en particulier, qui autorise la vente des logiciels, semble
aller contre l'idée générale des logiciels libres, mais c'est en fait l'une de
ses forces. Puisque les licences permettent une redistribution libre, une fois
qu'une personne a récupéré une copie, il ou elle peut la distribuer elle-même et
peut même essayer de la vendre.
</p>

<p>
Bien que les logiciels libres ne soient pas complètement libres de contraintes,
ils laissent à l'utilisateur la flexibilité de faire ce dont il a besoin pour
accomplir son travail. En même temps, ils protègent les droits de l'auteur – c'est
cela la liberté. Le projet Debian et ses membres sont de fervents partisans des
logiciels libres. Nous avons réuni les <a
href="../social_contract#guidelines">principes du logiciel libre selon Debian
(<i>Debian Free Software Guidelines</i> ou <i>DFSG</i>)</a> pour arriver à une
définition correcte de ce qui constitue les logiciels libres à notre avis.
Seuls les logiciels qui obéissent aux règles du DFSG sont autorisés à figurer dans la distribution principale (<i>main</i>) de Debian.
</p>
