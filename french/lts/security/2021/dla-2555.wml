#use wml::debian::translation-check translation="4fcc70e9176cd63d44d1565ff55a606c9c42f4ee" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème de fichier temporaire non
sécurisé qui pourrait avoir abouti à une divulgation de fichiers locaux
arbitraires.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21290">CVE-2021-21290</a>

<p>Netty est un cadriciel applicatif au code source ouvert, piloté par les
évènements réseau asynchrones, pour un développement rapide de serveurs et
clients de haute performance et d’entretien facile pour divers protocoles.
Dans Netty avant la version 4.1.59.Final, il existait une vulnérabilité pour
les systèmes de type Unix impliquant un fichier temporaire non
sécurisé. [...]</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:4.1.7-2+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets netty.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2555.data"
# $Id: $
