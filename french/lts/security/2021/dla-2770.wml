#use wml::debian::translation-check translation="a09938ead753e05f933fb0e40e96820d71fa8e73" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans le client de clavardage
WeeChat.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8955">CVE-2020-8955</a>

<p>Un message irc 324 contrefait (mode canal) pourrait aboutir à un plantage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9759">CVE-2020-9759</a>

<p>Un message irc 352 contrefait (qui) pourrait aboutir à un plantage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9760">CVE-2020-9760</a>

<p>Un message irc 005 contrefait (définition d’un nouveau mode pour un pseudo)
pourrait aboutir à un plantage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40516">CVE-2021-40516</a>

<p>Une trame WebSocket contrefaite pourrait aboutir à un plantage dans le
greffon Relay.</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 1.6-1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets weechat.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de weechat,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/weechat">\
https://security-tracker.debian.org/tracker/weechat</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2770.data"
# $Id: $
