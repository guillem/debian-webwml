#use wml::debian::translation-check translation="39eeafd0987e598aa50f6159bfc57db251bb95fb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Exiv2, une bibliothèque
C++ et un utilitaire en ligne de commande pour gérer les métadonnées d’image,
qui pourraient aboutir à un déni de service ou à l'exécution de code
arbitraire si un fichier mal formé est analysé.</p>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 0.25-3.1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets exiv2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de exiv2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/exiv2">\
https://security-tracker.debian.org/tracker/exiv2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2750.data"
# $Id: $
