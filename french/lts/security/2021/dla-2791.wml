#use wml::debian::translation-check translation="058727de99a3388a9ff376664abb88ee6062bab7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité a été découverte dans le gestionnaire de listes de diffusion Mailman qui pouvait aboutir à une élévation des privilèges distants. Certaines valeurs du jeton CSRF étaient dérivées du mot de passe de l’administrateur et auraient pu être utilisées pour conduire une attaque par force brute sur ce mot de passe.</p>

<ul>

<li>
<a href="https://security-tracker.debian.org/tracker/CVE-2021-42096">CVE-2021-42096</a>
&amp;
<a href="https://security-tracker.debian.org/tracker/CVE-2021-42097">CVE-2021-42097</a>

<p>Les versions de GNU Mailman antérieures à 2.1.35 pourrait permettre une élévation des
privilèges distants. Une certaine valeur csrf_token est dérivée du mot de passe de
l’administrateur et pourrait permettre de conduire une attaque par force
brute sur ce mot de passe.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:2.1.23-1+deb9u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mailman.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2791.data"
# $Id: $
