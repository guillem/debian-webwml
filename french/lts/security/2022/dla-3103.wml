#use wml::debian::translation-check translation="9bd51963ec20ba4e5b6250a1d6ffd4a09e6d6428" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Evgeny Legerov a signalé une vulnérabilité de dépassement de tas dans
l'opération de décompression dans zlib, qui pouvait avoir pour conséquences
un déni de service ou éventuellement l'exécution de code arbitraire lors du
traitement d'une entrée contrefaite pour l'occasion.</p>

<p>Pour Debian 10 Buster, ce problème a été corrigé dans la version
1:1.2.11.dfsg-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zlib.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de zlib, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/zlib">\
https://security-tracker.debian.org/tracker/zlib</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3103.data"
# $Id: $
