#use wml::debian::translation-check translation="c8fbe069fed50f1911039ebb51169ed5352bb8b4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans cacti, une
interface web de représentation graphique de système de supervision, qui
pouvaient aboutir à une divulgation d'informations, un contournement
d’authentification ou une exécution de code à distance.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8813">CVE-2020-8813</a>

<p>Askar a découvert qu’un client authentifié avec le privilège de
représentation en temps réel pouvait exécuter du code arbitraire sur un serveur
exécutant Cacti, à l’aide de métacaractères d’interpréteur dans un cookie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-23226">CVE-2020-23226</a>

<p>Jing Chen a découvert plusieurs vulnérabilités de script intersite (XSS) dans
plusieurs pages, qui pouvaient conduire à une divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25706">CVE-2020-25706</a>

<p>joelister a découvert une vulnérabilité de script intersite (XSS) dans
templates_import.php, qui pouvait conduire à une divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0730">CVE-2022-0730</a>

<p>Il a été découvert que l’authentification de Cacti pouvait être contournée
quand la liaison anonyme LDAP était activée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46169">CVE-2022-46169</a>

<p>Stefan Schiller a découvert une vulnérabilité d’injection de commande,
permettant à un utilisateur non authentifié d’exécuter du code arbitraire sur
le serveur exécutant Cacti, si une source de données spécifique était choisie
(ce qui est probable pour une instance en production) pour n’importe quel
périphérique supervisé.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.2.2+ds1-2+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cacti.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cacti,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cacti">\
https://security-tracker.debian.org/tracker/cacti</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3252.data"
# $Id: $
