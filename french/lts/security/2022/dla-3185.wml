#use wml::debian::translation-check translation="b8487441d55baec9dfb1245cf744e4beccdf72d7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été trouvées dans l’extension Xkb du serveur X
d’X.org, qui pourraient aboutir à un déni de service ou éventuellement à une
élévation des privilèges si le serveur X est exécuté avec des privilèges.</p>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 2:1.20.4-1+deb10u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xorg-server.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de xorg-server,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/xorg-server">\
https://security-tracker.debian.org/tracker/xorg-server</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3185.data"
# $Id: $
