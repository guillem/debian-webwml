#use wml::debian::translation-check translation="952f5d731014820260fc65adde3cbbf11e530398" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs bogues ont été découverts dans PostgreSQL, un système de
serveur de base de données relationnelle. Cette nouvelle version mineure
LTS corrige plus de 25 bogues qui ont été signalés durant les derniers
mois. La liste complète et détaillée des problèmes peut être trouvée sur :
<a href="https://www.postgresql.org/docs/release/11.18.">\
https://www.postgresql.org/docs/release/11.18.</a></p>

<p>Pour Debian 10 Buster, ce problème a été corrigé dans la version
11.18-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets postgresql-11.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de postgresql-11,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/postgresql-11">\
https://security-tracker.debian.org/tracker/postgresql-11</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3189.data"
# $Id: $
