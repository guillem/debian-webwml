#use wml::debian::translation-check translation="096b0654525f88e1c9c2114d7db1ce7486c62ce4" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l'exécutable
d'OpenJDK Java. Cela pouvait avoir pour conséquences un déni de service, un
contournement de restrictions de désérialisation ou la divulgation
d'informations.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 8u322-b06-1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openjdk-8.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openjdk-8, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openjdk-8">\
https://security-tracker.debian.org/tracker/openjdk-8</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2917.data"
# $Id: $
