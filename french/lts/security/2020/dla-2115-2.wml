#use wml::debian::translation-check translation="878f72cb081124703607185f6d9943b0051edc2c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une régression dans le précédent correctif
pour une vulnérabilité d’utilisation de mémoire après libération dans le serveur
FTP <tt>proftpd-dfsg</tt>.</p>

<p>L’exploitation de la vulnérabilité originale dans la gestion de la mémoire
mutuelle pouvait permettre à un attaquant distant d’exécuter du code arbitraire
sur le système affecté. Cependant, le correctif publié dans <tt>proftpd-dfsg</tt>,
version <tt>1.3.5e+r1.3.5-2+deb8u6</tt>, possédait une régression concernant le
traitement du formatage des journaux.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9273">CVE-2020-9273</a>

<p>Dans ProFTPD 1.3.7, il était possible de corrompre la mémoire mutuelle en
interrompant le canal de transfert de données. Cela provoquait une utilisation
de mémoire après libération dans <tt>alloc_pool</tt> dans <tt>pool.c</tt>, et
une possible exécution de code à distance.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.3.5e+r1.3.5-2+deb8u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets proftpd-dfsg.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2115-2.data"
# $Id: $
