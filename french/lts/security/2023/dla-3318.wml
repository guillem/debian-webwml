#use wml::debian::translation-check translation="6727d22602a15b40b5696d997490da58463fd3aa" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de sécurité a été découverte dans HAProxy, un serveur
mandataire inverse de répartition de charge rapide et fiable, qui pouvait
aboutir à un déni de service, ou un contournement des contrôles d’accès et des
règles de routage à l’aide de requêtes contrefaites pour l'occasion.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.8.19-1+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets haproxy.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de haproxy,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/haproxy">\
https://security-tracker.debian.org/tracker/haproxy</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3318.data"
# $Id: $
