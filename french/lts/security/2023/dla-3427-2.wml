#use wml::debian::translation-check translation="c0d63ed65467b392ff6f79948b18bcd6daaf86cd" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2023-32700">CVE-2023-32700</a>
dans texlive-bin, publié dans la DLA-3427-1, était incomplet et causait une
erreur lors de l’exécution de la commande lualatex.</p>

<p>La vulnérabilité de sécurité suivante a été aussi corrigée.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18604">CVE-2019-18604</a>

<p>Un défaut a été découvert dans axohelp dans axodraw2. La fonction sprintf
était mal gérée, ce qui pouvait causer une erreur de débordement de pile.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2018.20181218.49446-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets texlive-bin.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de texlive-bin,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/texlive-bin">\
https://security-tracker.debian.org/tracker/texlive-bin</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3427-2.data"
# $Id: $
