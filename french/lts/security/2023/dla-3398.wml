#use wml::debian::translation-check translation="d23103bfae1499d647bd9ed94c857e48a552f315" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans cURL, une
bibliothèque de transfert d’URL, coté client et facile à utiliser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27533">CVE-2023-27533</a>

<p>Une vulnérabilité dans la validation d’entrée existait dans curl lors de
communications utilisant le protocole TELNET et pouvait permettre à un attaquant
de passer un nom d’utilisateur contrefait de manière malveillante et <q>telnet
options</q> lors de la négociation de serveur. L’absence de nettoyage correct
d’entrée permettait à un attaquant d’envoyer du contenu ou de réaliser une
option de négociation sans intention de l’application. Cette vulnérabilité
pouvait être exploitée si une application permettait une entrée d’utilisateur,
autorisant par là même à des attaquants d’exécuter du code arbitraire sur
le système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27535">CVE-2023-27535</a>

<p>Une vulnérabilité de contournement d’authentification existait dans libcurl
dans la fonction de réutilisation de connexion FTP, qui pouvait aboutir dans
l’utilisation de mauvaises accréditations lors de transferts subséquents.
Les connexions créées précédemment sont conservées dans un réservoir de
connexions pour une réutilisation si elles correspondent à la configuration en
cours. Cependant, certains réglages FTP tels que CURLOPT_FTP_ACCOUNT,
CURLOPT_FTP_ALTERNATIVE_TO_USER, CURLOPT_FTP_SSL_CCC et CURLOPT_USE_SSL
n’étaient pas inclus dans les vérifications de correspondance de configuration,
faisant qu’elles correspondaient trop facilement. Cela pouvait conduire à ce
que libcurl utilise les mauvaises accréditations lors de la réalisation d’un
transfert, potentiellement permettant un accès non autorisé à des informations
sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27536">CVE-2023-27536</a>

<p>Une vulnérabilité de contournement d’authentification existait dans libcurl
dans la fonction de réutilisation de connexion qui permettait de réutiliser des
connexions précédemment établies avec des permissions d’utilisateur incorrectes
à cause d’un défaut de vérification de modifications dans l’option
CURLOPT_GSSAPI_DELEGATION. Cette vulnérabilité affectait les transferts
krb5/kerberos/negotiate/GSSAPI et pouvait éventuellement raboutir à un accès non
autorisé à des informations sensibles. L’option la plus sûre est de ne pas
réutiliser les connexions si l’option CURLOPT_GSSAPI_DELEGATION a été modifiée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27538">CVE-2023-27538</a>

<p>Une vulnérabilité de contournement d’authentification existait dans libcurl
quand elle réutilise une connexion SSH précédemment établie malgré le fait
qu’une option ait été modifiée, ce qui aurait dû empêcher la réutilisation. Libcurl
entretient un réservoir de connexions utilisées précédemment pour les réutiliser
dans des transferts subséquents si la configuration correspond. Cependant,
deux réglages SSH étaient omis de la vérification de configuration, permettant
une correspondance facile et conduisant éventuellement à une
réutilisation d’une connexion inappropriée.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 7.64.0-4+deb10u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de curl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3398.data"
# $Id: $
