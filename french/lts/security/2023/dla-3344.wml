#use wml::debian::translation-check translation="71b29ec7b7306024d44dddbdf9f9e0165e93d35a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des vulnérabilités ont été découvertes dans Node.js, qui pouvaient aboutir à
des attaques de <q>DNS rebinding</q> ou à l’exécution de code arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43548">CVE-2022-43548</a>

<p>Le dispositif de protection contre le <q>rebinding</q> de Node.js pour
<code>--inspect</code> autorisait encore les adresses IP non valables,
particulièrement dans le format octal, que les navigateurs tels que Firefox
essayaient de résoudre à l’aide du service DNS. Lorsque combiné avec une session
active <code>--inspect</code>, comme dans l’utilisation de VSCode, un attaquant
pouvait réaliser un <q>DNS rebinding</q> et exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23920">CVE-2023-23920</a>

<p>Ben Noordhuis a signalé que Node.js recherchait et éventuellement chargeait
des données ICU lors d’une exécution avec des privilèges élevés. Node.js est
désormais construit avec <code>ICU_NO_USER_DATA_OVERRIDE</code> pour éviter
cela.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 10.24.0~dfsg-1~deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nodejs.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nodejs,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/nodejs">\
https://security-tracker.debian.org/tracker/nodejs</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3344.data"
# $Id: $
