#use wml::debian::translation-check translation="4e0e87438f01f956273f78bffb3a6923c8c4ec62" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert qu’OpenSSH gérait incorrectement le chargement de certains
fournisseurs PKCS#11. Si un utilisateur retransmettait son ssh-agent à un
système non fiable, un attaquant distant pouvait éventuellement utiliser ce
défaut pour charger des bibliothèques arbitraires du système de l’utilisateur
et exécuter du code arbitraire.</p>

<p>En plus de ce problème de sécurité, cette mise à jour corrige un autre bogue :
mauvaise interaction entre les directives de ssh_config ConnectTimeout et
ConnectionAttempts, les essais de connexion après le premier ignoraient la
temporisation requise. Plus de détails sont disponibles sur
<a href="https://bugzilla.mindrot.org/show_bug.cgi?id)18.">https://bugzilla.mindrot.org/show_bug.cgi?id)18.</a>.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:7.9p1-10+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssh.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openssh,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openssh">\
https://security-tracker.debian.org/tracker/openssh</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3532.data"
# $Id: $
