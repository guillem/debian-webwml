#use wml::debian::translation-check translation="c7e8f26e04590f45c331888ec5b0b63b0b0dd4c4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découverts dans ring/jami, une plateforme
sécurisée et distribuée de voix, de vidéo et de clavardage. Ces problèmes
concernaient des vérifications manquantes de limites, aboutissant à un accès
en lecture hors limites, un dépassement de tampon ou un déni de service.</p>


<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 20190215.1.f152c98~ds1-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ring/jami.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ring,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ring">\
https://security-tracker.debian.org/tracker/ring</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3549.data"
# $Id: $
