#use wml::debian::translation-check translation="603febabb7a882ece7cc07acbf0a53b47779772a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans renderdoc, un outil
de débogage graphique autonome, qui éventuellement permettait à un attaquant
distant d’exécuter du code arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-33863">CVE-2023-33863</a>

<p>Un dépassement d'entier aboutissant à un dépassement de tampon de tas pouvait
être exploitable par un attaquant distant pour exécuter du code arbitraire sur
la machine exécutant RenderDoc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-33864">CVE-2023-33864</a>

<p>Un dépassement d'entier par le bas aboutissant à un dépassement de tampon de
 tas pouvait être exploitable par un attaquant distant pour exécuter du code
arbitraire sur la machine exécutant RenderDoc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-33865">CVE-2023-33865</a>

<p>Une vulnérabilité de lien symbolique pouvait être exploitable par un
attaquant local sans privilèges pour obtenir ceux de l’utilisateur exécutant
RenderDoc.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.2+dfsg-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets renderdoc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de renderdoc,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/renderdoc">\
https://security-tracker.debian.org/tracker/renderdoc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3501.data"
# $Id: $
