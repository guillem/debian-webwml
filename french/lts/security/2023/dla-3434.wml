#use wml::debian::translation-check translation="f08fdbace0b5722a102b89bef3f2a1b560d6abb7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que sysstat, des outils de performance du système pour
Linux, corrigeait incomplètement le
<a href="https://security-tracker.debian.org/tracker/CVE-2022-39377">CVE-2022-39377</a>
(tel que publié dans la DLA-3188-1), ce qui pouvait conduire à un plantage et,
éventuellement, à une exécution de code à distance.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-33204">CVE-2023-33204</a>

<p>sysstat permettait une dépassement d'entier dans une multiplication dans
check_overflow dans common.c. Remarque : ce problème existait à cause d’une correction incomplète du
<a href="https://security-tracker.debian.org/tracker/CVE-2022-39377">CVE-2022-39377</a>.</p>

<p>Pour référence, la vulnérabilité initiale était :</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39377">CVE-2022-39377</a>

<p>Sur les systèmes 32 bits, allocate_structures contient un dépassement de
size_t dans sa_common.c. La fonction allocate_structures vérifie insuffisamment
les limites avant les multiplications arithmétiques, permettant un dépassement
dans la taille allouée pour le tampon représentant les activités du système. Ce
problème pouvait conduire à une exécution de code à distance (RCE).</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 12.0.3-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sysstat.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sysstat,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sysstat">\
https://security-tracker.debian.org/tracker/sysstat</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3434.data"
# $Id: $
