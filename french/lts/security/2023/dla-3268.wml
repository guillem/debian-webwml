#use wml::debian::translation-check translation="28915167354d5ccbff46bdf1fb47ffb7005d3bf5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités d'épuisement de mémoire, de dépassement de pile
ou de dissimulation de requête HTTP ont été découvertes dans Netty, un
environnement de développement de sockets client/serveur NIO en Java, qui
pouvaient permettre à des attaquants de provoquer un déni de service ou de
contourner des restrictions lors de son utilisation comme mandataire.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:4.1.33-1+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets netty.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de netty,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/netty">\
https://security-tracker.debian.org/tracker/netty</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3268.data"
# $Id: $
