#use wml::debian::translation-check translation="c0d63ed65467b392ff6f79948b18bcd6daaf86cd" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Max Chernoff a découvert que des séquences d’échappement d’interpréteur
improprement sécurisées dans LuaTeX pouvait aboutir à l’exécution de commandes
arbitraires d’interpréteur, même si ces séquences étaient désactivées, si des
fichiers tex contrefaits spécialement étaient traitées.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2018.20181218.49446-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets texlive-bin.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de texlive-bin,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/texlive-bin">\
https://security-tracker.debian.org/tracker/texlive-bin</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3427.data"
# $Id: $
