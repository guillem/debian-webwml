#use wml::debian::translation-check translation="669b0da1c72840305674fb7728737cb9ac7b2bdd" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des vulnérabilités ont été trouvées dans libssh2, une bibliothèque C côté
client mettant en œuvre le protocole SSH2, qui pouvaient conduire à un déni de
service ou à une divulgation d'informations distante.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13115">CVE-2019-13115</a>

<p>Kevin Backhouse a découvert une vulnérabilité de dépassement d’entier dans
<code>kex_method_diffie_hellman_group_exchange_sha256_key_exchange()</code> de
la fonction <code>kex.c</code>, qui pouvait conduire à une lecture hors limites
dans la manière dont les paquets étaient lus à partir du serveur. Un attaquant
distant qui pouvait compromettre un serveur SSH pouvait divulguer des
informations sensibles ou causer une condition de déni de service sur le système
client quand un utilisateur se connectait au serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17498">CVE-2019-17498</a>

<p>Kevin Backhouse a découvert que la logique de <code>SSH_MSG_DISCONNECT</code>
dans packet.c avait un dépassement d'entier dans la vérification de limites,
permettant à un attaquant de spécifier une position arbitraire (hors limites)
pour une lecture suivante de mémoire. Un serveur SSH malveillant pouvait être
capable de divulguer des informations sensibles ou de causer une condition de
déni de service sur le système client quand un utilisateur se connectait au
serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22218">CVE-2020-22218</a>

<p>Un problème a été découvert dans la fonction <code>_libssh2_packet_add()</code>.
Cela pouvait permettre à des attaquants d’accéder hors limites à la mémoire.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.8.0-2.1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libssh2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libssh2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libssh2">\
https://security-tracker.debian.org/tracker/libssh2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3559.data"
# $Id: $
