#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>simplesamlphp, une application d'authentification et de fédération était
vulnérable à une attaque de script intersite (XSS), à un contournement de
validation de signature et à une utilisation non sécurisée de jeux de
caractères de connexion.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18121">CVE-2017-18121</a>

<p>Un problème de script intersite (XSS) a été découvert dans le module
consentAdmin de SimpleSAMLphp jusqu'à la version 1.14.15, permettant à un
attaquant de contrefaire manuellement des liens qu'une victime peut ouvrir,
exécutant du code JavaScript arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18122">CVE-2017-18122</a>

<p>Un problème de contournement de validation de signature a été découvert
dans SimpleSAMLphp jusqu'à la version 1.14.16. Un fournisseur de service
utilisant SAML 1.1 considérera comme valable toute réponse SAML non signée
contenant plus d'une assertion signée, à condition que la signature d'au
moins une des assertions soit valable. Les attributs contenus dans toutes
les assertions reçues seront regroupés et l'ID d'entité de la première
assertion sera utilisée, permettant à un attaquant de se faire passer pour
n'importe quel utilisateur d'un fournisseur d'identité (IdP) ayant reçu une
assertion signée par l'IdP cible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6521">CVE-2018-6521</a>

<p>Le module sqlauth dans SimpleSAMLphp, version antérieure à 1.15.2,
repose sur le jeu de caractère utf8 de MySQL qui tronque les requêtes
lorsqu'il rencontre des caractères codés sur quatre octets. Il pourrait y
avoir un scénario qui permet à des attaquants distants de contourner des
restrictions d'accès voulues.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.9.2-1+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets simplesamlphp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1273.data"
# $Id: $
