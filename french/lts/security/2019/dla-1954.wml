#use wml::debian::translation-check translation="5d9c13b130b4e3dd1fe3a9bd378f22e9e769e363" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de sécurité a été découverte dans lucene-solr, un serveur
de recherche d’entreprise.</p>

<p>DataImportHandler, un module facultatif mais populaire pour obtenir des
données à partir de bases de données ou d’autres sources avait une fonction
dans laquelle la configuration entière DIH pouvait venir d’un paramètre
<q>dataConfig</q> de requête. Le mode débogage de l’écran DIH de l’administrateur
utilise cela pour permettre le développement ou le débogage pratique de la
configuration DIH. Puisqu’une configuration DIH peut contenir des scripts, ce
paramètre est un risque de sécurité. Désormais, l’utilisation de ce paramètre
nécessite le réglage de la propriété du système Java
<q>enable.dih.dataConfigParam</q> à vrai. Par exemple, cela peut être réalisé
avec solr-tomcat en ajoutant  -Denable.dih.dataConfigParam=true à JAVA_OPTS dans
/etc/default/tomcat7.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 3.6.2+dfsg-5+deb8u3.</p>
<p>Nous vous recommandons de mettre à jour vos paquets lucene-solr.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1954.data"
# $Id: $
