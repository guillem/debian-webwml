#use wml::debian::template title="Lý do nên dùng Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b" maintainer="Trần Ngọc Quân"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#users">Debian cho Người dùng</a></li>
    <li><a href="#devel">Debian cho Nhà phát triển</a></li>
    <li><a href="#enterprise">Debian cho Môi trường kinh doanh</a></li>
  </ul>
</div>

<p>
Có nhiều lý do để chọn Debian làm hệ điều hành của bạn –
với tư cách người dùng, nhà phát triển phần mềm, và thậm chí là trong môi trường kinh doanh. Phần lớn
người dùng đánh giá cao tính ổn định, quy trình cập nhật mượt mà cho cả nâng cấp
các gói hay toàn bộ bản phân phối. Debian cũng được sử dụng rộng rãi
bởi những người phát triển phần cứng cũng như phần mềm bởi vì nó chạy được trên nhiều
kiến trúc và thiết bị khấc nhau, cung cấp một bộ theo dõi lỗi công khai và các công cụ khác
cho phát triển. Nếu bạn có kế hoạch sử dụng Debian trong một môi trường chuyên nghiệp,
còn có các lợi ích thêm nữa như là phiên bản LTS và các ảnh cho điện toán đám mây.
</p>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Đối với tôi, nó đạt đến mức hoàn hảo khi sử dụng và sự ổn định. Tôi từng sử dụng nhiều bản phân phối khác nhau trong nhiều năm nhưng Debian cái duy nhất phù hợp với công việc. <a href="https://www.reddit.com/r/debian/comments/8r6d0o/why_debian/e0osmxx">NorhamsFinest trên Reddit</a></p>
</aside>

<h2><a id="users">Debian cho Người dùng</a></h2>

<dl>
  <dt><strong>Debian là Phần mềm tự do.</strong></dt>
  <dd>
    Debian được tạo bởi phần mềm tự do và nguồn mở và sẽ luôn 100%
    <a href="free">tự do</a>. Tự do để bất kỳ ai cũng có thể sử dụng, sửa đổi, và
    phân phối lại. Đây là lời hứa của chúng tôi với <a href="../users">những người dùng của mình</a>. Dùng nó cũng không tốn xu nào.
  </dd>
</dl>

<dl>
  <dt><strong>Debian ổn định và an toàn.</strong></dt>
  <dd>
    Debian là hệ điều hành dựa trên Linux các thiết bị ở các lĩnh vực khác nhau bao gồm
    máy tính xách tay, để bàn và máy chủ. Chúng tôi cung ứng một cấu hình mặc định hợp lý cho
    mọi gói cũng như là cập nhật bảo mật định kỳ trong suốt vòng đời của các gói.
  </dd>
</dl>

<dl>
  <dt><strong>Debian hỗ trợ phần cứng rộng rãi.</strong></dt>
  <dd>
    Phần lớn phần cứng được hỗ trợ bởi hạt nhân Linux điều đó có nghĩa là
    Debian cũng sẽ hỗ trợ nó. Trình điều khiển độc quyền cho phần cứng
    cũng sẵn có nếu cần.
  </dd>
</dl>

<dl>
  <dt><strong>Debian đưa ra một Trình cài đặt uyển chuyển.</strong></dt>
  <dd>
    <a
    href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live
    CD</a> của chúng tôi là dành cho những người muốn thử dùng Debian trước khi
    cài đặt nó. Nó cũng bao gồm sẵn bộ cài đặt Calamares cái mà làm cho
    nó dễ dàng để cài đặt Debian từ hệ thống trực tiếp. Những người dùng
    giàu kinh nghiệm có thể sử dụng bộ cài đặt Debian với thêm các tùy chọn để tinh chỉnh,
    bao gồm có thể sử dụng một công cụ cài đặt mạng được
    tự động hóa.
  </dd>
</dl>

<dl>
  <dt><strong>Debian cung cấp các cập nhật mượt mà.</strong></dt>
  <dd>
    Rất dễ dàng để giữ cho hệ điều hành của chúng tôi được cập nhật, bất kể là bạn muốn
    nâng cấp toàn bộ bản phát hành hay là chỉ cập nhật một gói đơn lẻ.
  </dd>
</dl>

<dl>
  <dt><strong>Debian là Cơ sở nền tảng cho nhiều bản phân phối khác.</strong></dt>
  <dd>
    Nhiều bản phân phối Linux phổ biến, như Ubuntu, Knoppix, PureOS hay Tails,
    được dựa trên Debian. Chúng tôi cung cấp mọi công cụ do đó mọi người có thể
    mở rộng các gói phần mềm từ kho chứa Debian bằng các gói mà họ
    sở hữu nếu muốn.
  </dd>
</dl>

<dl>
  <dt><strong>Dự án Debian là một Cộng đồng.</strong></dt>
  <dd>
    Mọi người có thể là một phần của cộng đồng của chúng tôi; bạn không
    cần phải là nhà phát triển phần mềm hay quản trị hệ thống. Debian có một
    <a href="../devel/constitution">cấu trúc cai trị
    dân chủ</a>. Do mọi thành viên của dự án có quyền ngang nhau,
    nên Debian không thể bị kiểm soát bởi một công ty đơn lẻ nào. Những nhà
    phát triển phần mềm của chúng tôi đến từ hơn 60 quốc gia khác nhau, và bản thân Debian
    đã được dịch ra hơn 80 thứ tiếng.
  </dd>
</dl>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Lý do đằng sau việc Debian được coi là hệ điều hành của những nhà phát triển phần mềm vì nó hỗ trợ một lượng lớn gói và phần mềm, đây là thứ quan trọng cho những nhà phát triển. Rất khuyến nghị cho những lập trình viên cấp cao và những nhà quản trị hệ thống. <a href="https://fossbytes.com/best-linux-distros-for-programming-developers/">Adarsh Verma trên Fossbytes</a></p>
</aside>

<h2><a id="devel">Debian cho Nhà phát triển</a></h2>

<dl>
  <dt><strong>Đa kiến trúc phần cứng</strong></dt>
  <dd>
    Debian hỗ trợ <a href="../ports">danh sách dài</a> kiến trúc CPU,
    bao gồm amd64, i386, nhiều phiên bản của ARM và MIPS, POWER7, POWER8,
    IBM System z và RISC-V. Debian cũng sẵn có cho các kiến trúc niche.
  </dd>
</dl>

<dl>
  <dt><strong>Các thiết bị IoT và Nhúng</strong></dt>
  <dd>
    Debian chạy được trên các thiết bị của nhiều lĩnh vực khác nhau, như là Raspberry Pi,
    biến thể của QNAP, thiết bị di động, bộ định tuyến gia đình và rất nhiều Máy tính
    trên một bảng mạch đơn (SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Số lượng khổng lồ gói phần mềm</strong></dt>
  <dd>
    Debian có số lượng <a
    href="$(DISTRIB)/packages">gói</a> khổng lồ (hiện tại trong
    bản ổn định: <packages_in_stable> gói) cái mà sử dụng <a
    href="https://manpages.debian.org/unstable/dpkg-dev/deb.5.en.html">định dạng
    deb</a>.
  </dd>
</dl>

<dl>
  <dt><strong>Các bản phát hành khác nhau</strong></dt>
  <dd>
    Bên cạnh bản phát hành ổn định của chúng tôi, bạn có thể cài đặt các phiên bản phần mềm mới hơn
    bằng cách sử dụng các bản phát hành thử nghiệm hoặc chưa ổn định.
  </dd>
</dl>

<dl>
  <dt><strong>Bộ theo dõi lỗi công khai</strong></dt>
  <dd>
    <a href="../Bugs">Hệ thống theo dõi lỗi</a> Debian của chúng tôi (BTS) sẵn có
    công khai để mọi người xem thông qua trình duyệt web. Chúng tôi không che giấu các lỗi
    phần mềm của mình, và bạn có thể dễ dàng gửi báo cáo lỗi mới hay tham gia tranh luận.
  </dd>
</dl>

<dl>
  <dt><strong>Chính sách Debian và các công cụ phát triển</strong></dt>
  <dd>
    Debian đưa ra các phần mềm chất lượng cao. Để tìm hiểu thêm về
    các tiêu chuẩn của chúng tôi, hãy đọc <a href="../doc/debian-policy/">chính sách</a>
    để biết các định nghĩa về yêu cầu kỹ thuật cho mọi gói được bao gồm trong
    bản phân phối. Chiến lược Phân tích Liên tục của chúng tôi gọi lệnh
    Autopkgtest (chạy các kiểm tra trên các gói), Piuparts (kiểm tra cài đặt,
    nâng cấp hay gỡ bỏ), và Lintian (kiểm chuẩn các gói để tìm ra các mâu thuẫn
    và lỗi).
  </dd>
</dl> 

<aside>
<p><span class="far fa-comment-dots fa-3x"></span>Ổn định là đồng nghĩa với Debian. [...]  An toàn là một trong những tính năng quan trọng nhất của Debian. <a href="https://www.pontikis.net/blog/five-reasons-to-use-debian-as-a-server">Christos Pontikis trên pontikis.net</a></p>
</aside>

<h2><a id="enterprise">Debian cho Môi trường kinh doanh</a></h2>

<dl>
  <dt><strong>Debian là đáng tin cậy.</strong></dt>
  <dd>
    Debian chứng minh tính ổn định hàng ngày trong hàng ngàn bối cảnh
    thực tế, trải dài trong các lĩnh vực từ người dùng máy tính xách tay đơn lẻ đến siêu máy gia tốc hạt nhân, sàn giao dịch
    chứng khoán và công nghiệp xe hơi. Nó cũng phổ biến trong giới
    hàn lâm, trong khoa học và trong khu vực công.
  </dd>
</dl>

<dl>
  <dt><strong>Debian có nhiều Chuyên gia.</strong></dt>
  <dd>
    Những người bảo trì gói của chũng tôi không chỉ quan tâm đến đóng gói cho Debian
    gắn liền với các phiên bản thượng nguồn mới. Thường thì họ cũng tinh thông
    cả chính bản thân ứng dụng đó nữa và do đó cũng có thể đóng góp trực tiếp để
    phát triển phần mềm trên thượng nguồn.
  </dd>
</dl>

<dl>
  <dt><strong>Debian là an toàn.</strong></dt>
  <dd>
    Debian đưa ra hỗ trợ bảo mật cho các bản phát hành ổn định của nó. Nhiều
    bản phân phối và các nhà nghiên cứu bảo mật tin cậy vào bộ theo dõi bảo mật của Debian.
  </dd>
</dl>

<dl>
  <dt><strong>Hỗ trợ dài hạn</strong></dt>
  <dd>
    Phiên bản <a href="https://wiki.debian.org/LTS">Hỗ trợ dài hạn</a>
    (LTS) mở rộng vòng đời của mọi bản phát hành ổn định
    Debian lên tối thiểu 5 năm. Thêm nữa,
    <a href="https://wiki.debian.org/LTS/Extended">LTS Kéo dài</a> dành cho kinh doanh
    khởi xướng hỗ trợ tập hợp gói phần mềm kéo dài hơn 5 năm.
  </dd>
</dl>

<dl>
  <dt><strong>Ảnh cho Điện toán đám mây</strong></dt>
  <dd>
    Ảnh cho điện toán đám mây chính thức cũng sẵn có cho phần lớn các nền tảng đám mây chính. Chúng tôi cũng
    cung cấp các công cụ và cấu hình do vậy bạn có thể biên dịch để tạo ra các ảnh dành cho điện toán đám mây
    theo tùy chỉnh của mình. Bạn cũng có thể sử dụng Debian trong máy ảo trên máy tính để bàn
    cũng như trong một thùng chứa (container).
  </dd>
</dl>
