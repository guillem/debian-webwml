msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2011-01-10 22:46+0100\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "delegálási levél"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "kinevezési levél"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegált"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegált"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"he_him\"/>he/him"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"she_her\"/>she/her"

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"male\"/>delegált"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"they_them\"/>they/them"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "jelenleg"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "tag"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "menedzser"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Stabil kiadásmenedzser"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "varázsló"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "elnök"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "asszisztens"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "titkár"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "képviselő"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "szerepkör"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"A következő listában <q>jelenlegi</q> jelöléssel átmeneti pozíciók\n"
"szerepelnek (adott lejárati dátummal megválasztottak vagy kijelöltek)"

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Tisztviselők"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:104
msgid "Distribution"
msgstr "Disztribúció"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:195
msgid "Communication and Outreach"
msgstr "Kommunikáció és elérhetőség"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:198
msgid "Data Protection team"
msgstr "Adatvédelmi csapat"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:203
msgid "Publicity team"
msgstr "Nyilvánosság csapat"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:270
msgid "Membership in other organizations"
msgstr "Tagság más szervezetekben"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:293
msgid "Support and Infrastructure"
msgstr "Felhasználói támogatás és infrastruktúra"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Vezető"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Szakmai bizottság"

#: ../../english/intro/organization.data:99
msgid "Secretary"
msgstr "Titkár"

#: ../../english/intro/organization.data:107
msgid "Development Projects"
msgstr "Fejlesztési projektek"

#: ../../english/intro/organization.data:108
msgid "FTP Archives"
msgstr "FTP-archívumok"

#: ../../english/intro/organization.data:110
msgid "FTP Masters"
msgstr "FTP Masters"

#: ../../english/intro/organization.data:116
msgid "FTP Assistants"
msgstr "FTP asszisztensek"

#: ../../english/intro/organization.data:122
msgid "FTP Wizards"
msgstr "FTP varázslók"

#: ../../english/intro/organization.data:125
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:127
msgid "Backports Team"
msgstr "Backport csapat"

#: ../../english/intro/organization.data:131
msgid "Release Management"
msgstr "Kiadásmenedzsment"

#: ../../english/intro/organization.data:133
msgid "Release Team"
msgstr "Kiadás Csapat"

#: ../../english/intro/organization.data:143
msgid "Quality Assurance"
msgstr "Minőségbiztosítás"

#: ../../english/intro/organization.data:144
msgid "Installation System Team"
msgstr "A telepítési rendszer karbantartói"

#: ../../english/intro/organization.data:145
msgid "Debian Live Team"
msgstr "Debian Live csapat"

#: ../../english/intro/organization.data:146
msgid "CD/DVD/USB Images"
msgstr "CD/DVD/USB képek"

#: ../../english/intro/organization.data:148
msgid "Production"
msgstr "Gyártás"

#: ../../english/intro/organization.data:156
msgid "Testing"
msgstr "Tesztelés"

#: ../../english/intro/organization.data:158
msgid "Cloud Team"
msgstr "Cloud csapat"

#: ../../english/intro/organization.data:160
#, fuzzy
msgid "Autobuilding infrastructure"
msgstr "Autobilding infrastruktúra"

#: ../../english/intro/organization.data:162
msgid "Wanna-build team"
msgstr "Wanna-build csapat"

#: ../../english/intro/organization.data:169
#, fuzzy
msgid "Buildd administration"
msgstr "Buildd adminisztráció"

#: ../../english/intro/organization.data:186
msgid "Documentation"
msgstr "Dokumentáció"

#: ../../english/intro/organization.data:190
msgid "Work-Needing and Prospective Packages list"
msgstr "További munkát igénylő és jövőbeli csomagok"

#: ../../english/intro/organization.data:206
msgid "Press Contact"
msgstr "Sajtókapcsolatok"

#: ../../english/intro/organization.data:208
msgid "Web Pages"
msgstr "Weboldalak"

#: ../../english/intro/organization.data:216
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:221
msgid "Outreach"
msgstr "Elérhetőség"

#: ../../english/intro/organization.data:226
msgid "Debian Women Project"
msgstr "Debian Women Project"

#: ../../english/intro/organization.data:234
msgid "Community"
msgstr "Közösség"

#: ../../english/intro/organization.data:241
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""
"A Közösség csapat minden tagjának küldendő levél esetén a következő GPG "
"kulcsot használd <a href=\"community-team-pubkey.txt "
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."

#: ../../english/intro/organization.data:243
msgid "Events"
msgstr "Események"

#: ../../english/intro/organization.data:250
msgid "DebConf Committee"
msgstr "DebConf Bizottság"

#: ../../english/intro/organization.data:257
msgid "Partner Program"
msgstr "Partner program"

#: ../../english/intro/organization.data:261
msgid "Hardware Donations Coordination"
msgstr "Hardveradomány-koordinátorok"

#: ../../english/intro/organization.data:276
msgid "GNOME Foundation"
msgstr "GNOE Alapítvány"

#: ../../english/intro/organization.data:278
msgid "Linux Professional Institute"
msgstr "Linux Professional Intézet"

#: ../../english/intro/organization.data:279
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:281
msgid "Linux Standards Base"
msgstr "Linux Standards Base"

#: ../../english/intro/organization.data:282
msgid "Free Standards Group"
msgstr "Free Standards Group"

#: ../../english/intro/organization.data:283
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS: Szervezet\n"
"      a Struktúrált Információs Szabványok Haladásáért"

#: ../../english/intro/organization.data:286
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""
"OVAL: Nyílt Sebezhetőség\n"
"      Értékelő Nyelv"

#: ../../english/intro/organization.data:289
msgid "Open Source Initiative"
msgstr "Nyílt Forráskód Kezdeményezés"

#: ../../english/intro/organization.data:296
msgid "Bug Tracking System"
msgstr "Hibakövető rendszer"

#: ../../english/intro/organization.data:301
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Levelezőlisták Adminisztrációja és Levelezőlisták Archívuma"

#: ../../english/intro/organization.data:309
msgid "New Members Front Desk"
msgstr "Új tagok recepciója"

#: ../../english/intro/organization.data:318
msgid "Debian Account Managers"
msgstr "Debian accountok kezelése"

#: ../../english/intro/organization.data:324
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Ha privát üzenetet küldesz minden DAM-nak, akkor a "
"57731224A9762EA155AB2A530CA8D15BB24D96F2 kulcsot használd."

#: ../../english/intro/organization.data:325
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Kulcscsomó-karbantartók (PGP és GPG)"

#: ../../english/intro/organization.data:329
msgid "Security Team"
msgstr "Biztonsági csoport"

#: ../../english/intro/organization.data:341
msgid "Policy"
msgstr "Szabályzat"

#: ../../english/intro/organization.data:344
msgid "System Administration"
msgstr "Rendszeradminisztráció"

#: ../../english/intro/organization.data:345
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Ezen a címen jelentheted be a Debian gépeivel kapcsolatos problémákat, "
"beleértve a jelszóproblémákat, vagy ha egy csomagot szeretnél telepíttetni."

#: ../../english/intro/organization.data:355
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Ha hardware-problémáid akadtak a Debian gépeivel, látogasd meg a <a href="
"\"https://db.debian.org/machines.cgi\">Debian-gépek</a> oldalt, ahol "
"megtalálod az egyes gépek adminisztrátorát."

#: ../../english/intro/organization.data:356
msgid "LDAP Developer Directory Administrator"
msgstr "LDAP fejlesztői adatbázis adminisztrátora"

#: ../../english/intro/organization.data:357
msgid "Mirrors"
msgstr "Tükrözések"

#: ../../english/intro/organization.data:360
msgid "DNS Maintainer"
msgstr "DNS-karbantartó"

#: ../../english/intro/organization.data:361
msgid "Package Tracking System"
msgstr "Csomagkövető rendszer"

#: ../../english/intro/organization.data:363
msgid "Treasurer"
msgstr "Kincstárnok"

#: ../../english/intro/organization.data:368
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Márkajel</a> használati "
"kérdések"

#: ../../english/intro/organization.data:371
msgid "Salsa administrators"
msgstr "Salsa adminisztrátorok"

#~ msgid "APT Team"
#~ msgstr "APT-team"

#~ msgid "Accountant"
#~ msgstr "Könyvelő"

#~ msgid "Alioth administrators"
#~ msgstr "Alioth-felügyelet"

#~ msgid "CD Vendors Page"
#~ msgstr "CD-terjesztők oldala"

#~ msgid "Consultants Page"
#~ msgstr "Konzultánsok oldala"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Testre szabott Debian disztribúciók"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian GNU/Linux a vállalkozási informatikában"

#, fuzzy
#~| msgid "New Maintainers Front Desk"
#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Új karbantartók felvétele"

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Debian multimédia"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Debian az oktatásban"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian gyerekeknek 1-től 99 éves korig"

#~ msgid "Debian for education"
#~ msgstr "Debian az oktatásban"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian az orvosi gyakorlatban és kutatásban"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian a nonprofit cégek számára"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian a fogyatékkal élők számára"

#, fuzzy
#~| msgid "Debian for medical practice and research"
#~ msgid "Debian for science and related research"
#~ msgstr "Debian az orvosi gyakorlatban és kutatásban"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian az ügyvédi irodákban"

#~ msgid "Delegates"
#~ msgstr "Képviselők"

#~ msgid "Embedded systems"
#~ msgstr "Beágyazott rendszerek"

#~ msgid "Firewalls"
#~ msgstr "Tűzfalak"

#~ msgid "Handhelds"
#~ msgstr "Tenyérgépek"

#~ msgid "Individual Packages"
#~ msgstr "Egyedi csomagok"

#~ msgid "Installation"
#~ msgstr "Telepítés"

#~ msgid "Installation System for ``stable''"
#~ msgstr "A \"stable\" telepítőrendszere"

#~ msgid "Internal Projects"
#~ msgstr "Belső projektek"

#~ msgid "Key Signing Coordination"
#~ msgstr "Kulcsaláírás-koordinátorok"

#~ msgid "Laptops"
#~ msgstr "Laptopok"

#, fuzzy
#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "A telepítési rendszer karbantartói"

#~ msgid "Mailing List Archives"
#~ msgstr "Levelezőlista-archívumok"

#~ msgid "Mailing list"
#~ msgstr "Levelezőlista"

#, fuzzy
#~| msgid "Security Testing Team"
#~ msgid "Marketing Team"
#~ msgstr "Biztonsági tesztelő csoport"

#~ msgid "Ports"
#~ msgstr "Portolások"

#~ msgid "Publicity"
#~ msgstr "Sajtó és nyilvánosság"

#~ msgid "Release Assistants"
#~ msgstr "Kiadás asszisztensek"

#~ msgid "Release Manager for ``stable''"
#~ msgstr "A ''stable'' kiadásmenedzsere"

#~ msgid "Release Notes"
#~ msgstr "Verzióinformációk"

#~ msgid "SchoolForge"
#~ msgstr "SchoolForge"

#~ msgid "Security Audit Project"
#~ msgstr "Biztonsági ellenőrzési projekt"

#~ msgid "Special Configurations"
#~ msgstr "Különleges konfigurációk"

#, fuzzy
#~| msgid "Security Team"
#~ msgid "Testing Security Team"
#~ msgstr "Biztonsági csoport"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Az univerzális asztali operációs rendszer"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Ez még nem hivatalos belő Debian projekt, de bejelentette az igényét az "
#~ "integrációra."

#~ msgid "User support"
#~ msgstr "Felhasználói támogatás"

#~ msgid "Vendors"
#~ msgstr "Terjesztők"
